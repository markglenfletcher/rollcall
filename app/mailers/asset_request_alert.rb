Rollcall::App.mailer :asset_request_alert do
  email :requests_due do |user, asset_requests|
    from 'admin@rollcall.com'
    to user.email
    subject "Rollcall - #{user.company.name} - Asset Requests Due Alert"
    locals company: user.company, asset_requests: asset_requests, user: user
    content_type 'text/html'  
    render  'alerts/asset_requests/requests_due'
  end

  email :request_created do |user, asset_request|
    from 'admin@rollcall.com'
    to user.email
    subject "Rollcall - #{user.company.name} - Asset Request Created Alert"
    locals company: user.company, asset_request: asset_request, user: user
    content_type 'text/html'  
    render  'alerts/asset_requests/request_created'
  end
end
