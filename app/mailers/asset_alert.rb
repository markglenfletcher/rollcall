Rollcall::App.mailer :asset_alert do
  email :expiring_warranties do |user, assets|
    from 'admin@rollcall.com'
    to user.email
    subject "Rollcall - #{user.company.name} - Warranty Expiration Alert"
    locals company: user.company, assets: assets, user: user
    content_type 'text/html'  
    render  'alerts/assets/expiring_warranties'
  end

  email :inspection_due do |user, assets|
    from 'admin@rollcall.com'
    to user.email
    subject "Rollcall - #{user.company.name} - Asset Inspection Alert"
    locals company: user.company, assets: assets, user: user
    content_type 'text/html'  
    render  'alerts/assets/inspection_due'
  end
end
