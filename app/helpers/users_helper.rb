# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module UsersHelper
      def find_user(id)
        current_company.users.includes(:identities,
                      :role,
                      :asset_requests,
                      :manager_asset_requests, 
                      :created_asset_requests,
                      :comments,
                      :assets).find_by_id(id)
      end

      def check_and_set_user(id)
        @user = find_user(id)
        unless @user
          redirect_with_flash_message url_for(:users, :index),
            :flash_type => :error, :flash_message => 'That user does not exist!'
        end
      end

      def load_resources_for_user_form
        @roles ||= current_company.roles.all
      end
    end

    helpers UsersHelper
  end
end
