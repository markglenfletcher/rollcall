# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module CompaniesHelper
      def send_to_overview_if_company_exists
        if @company
          redirect_with_flash_message url_for(:companies, :overview),
            :flash_type => :error, :flash_mesage => 'You already have a company, why do you want to create another? :P'
        end
      end

      def send_to_new_if_company_doesnt_exist
        unless @company
          redirect_with_flash_message url_for(:companies, :new),
              :flash_type => :error, :flash_message => "You don't currently have a company, please create one first"
        end
      end
    end

    helpers CompaniesHelper
  end
end
