# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module SessionsHelper
      def current_user_email
        current_user.email || nil
      end

      def find_user_and_login
        auth = request.env['omniauth.auth']
        current_user = User.find_by_provider_and_uid(auth['provider'], auth['uid']) || User.create_with_omniauth!(auth)
        session[:user_id] = current_user.id
      end
    end

    helpers SessionsHelper
  end
end
