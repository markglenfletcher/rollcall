# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module CommentsHelper
      def find_comment(id)
        Comment.includes(:comments).find_by_id(id)
      end

      def check_and_set_comment
        @comment = find_comment(params[:id])
        unless comment_can_be_edited_or_destroyed?(@comment)
          redirect_error 'You cannot modify this comment'
        end
      end

      def comment_owner_editing?(comment)
        comment.user == current_user
      end

      def comment_can_be_edited_or_destroyed?(comment)
        comment && comment_owner_editing?(comment)
      end

      def redirect_success(message)
        redirect_to_root message
      end

      def redirect_error(message)
        redirect_to_root :error, message
      end
    end

    helpers CommentsHelper
  end
end
