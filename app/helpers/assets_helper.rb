# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module AssetsHelper
      # controller helpers
      def check_and_set_asset
        @asset = find_asset(params[:id])
        unless @asset
          redirect_with_flash_message url_for(:assets, :index),
            :flash_type => :error, :flash_message => 'That asset does not exist!'  
        end
      end

      def load_resources_for_asset_form
        @users ||= current_company.users.includes(:identities).all
        @categories ||= current_company.categories.all
      end

      def find_asset(id)
        current_company.assets.includes(:categories,
                            :asset_profile,
                            :comments,
                            asset_status_changes: [user: [:identities]],
                            user: [:identities]).find_by_id(params[:id])
      end

      def date_text(date)
        date ? date.to_s : 'No Date Specified'
      end

      def health_status_btn(health_status)
        status_btn(health_status,'health_status', 
          status_el(Status::Health, health_status))
      end

      def assignment_status_btn(assignment_status)
        status_btn(assignment_status,'assignment_status', 
          status_el(Status::Assignment, assignment_status))
      end

      def warranty_status_btn(warranty_status)
        status_btn(warranty_status,'warranty_status', 
          status_el(Status::Warranty, warranty_status))
      end

      def status_el(mapping, warranty_status)
        mapping[warranty_status].status_class
      end

      def status_btn(status, type, css_class)
        link_to status,
          url_for(:assets, :statuses, type: type, status: status),
          class: "btn #{css_class}"
      end

      def status_change_text(status_change)
        status_change.class.name.titleize
      end

      def status_changed_to_btn(status_change)
        status_change_btn(status_change, status_change.changed_to_status)
      end

      def status_changed_from_btn(status_change)
        status_change_btn(status_change, status_change.changed_from_status)
      end

      def status_change_btn(status_change, status)
        case status_change
        when AssignmentStatusChange
          assignment_status_btn(status)
        when HealthStatusChange
          health_status_btn(status)
        when WarrantyStatusChange
          warranty_status_btn(status)
        when UserStatusChange
          user_btn(status)
        end
      end
    end

    helpers AssetsHelper
  end
end
