# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module AlertSettingsHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers AlertSettingsHelper
  end
end
