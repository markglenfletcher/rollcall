# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module RolesHelper
      def find_role(id)
        current_company.roles.includes(:assets, :comments, users: [:identities]).find_by_id(params[:id])
      end

      def check_and_set_role
        @role = find_role(params[:id])
        unless @role
          redirect_with_flash_message url_for(:roles, :index),
            :flash_type => :error, :flash_message => 'That role does not exist!'
        end
      end

      def load_resources_for_role_form
        @staff_profiles ||= current_company.staff_profiles.all
      end
    end

    helpers RolesHelper
  end
end
