# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module ApplicationHelper
      # Used for actions that accept ordering parameters
      def default_order_attribute
        'id'
      end

      def default_order_direction
        'asc'
      end

      def git_sha
        `git rev-parse --short HEAD`.chomp
      end

      # html helpers
      def filter_btn(filter, resource, options={})
        active_class = 'active' if @order == filter.to_s
        direction_param,direction_class = 'desc', ''
        if active_class
          direction_param = (@dir == 'desc' ? :asc : :desc)
          direction_class = case direction_param
            when :desc
              'fa fa-arrow-up'
            else
              'fa fa-arrow-down'
            end
        end
        text = options[:text] || filter.to_s.titleize
        content_tag(:td, class: "filter-btn #{active_class}") do 
          "".tap do |html|
            html << link_to(text, url_for(resource, :index, order: filter, dir: direction_param), class: 'btn btn-default')
            html << tag(:i, class: direction_class)
          end.html_safe
        end
      end

      def errors_for_form_group(resource, field, html_class)
        html_class.tap do |hc|
          hc << ' has-error' if resource.errors.messages[field]
        end
      end

      def id_for_resource(resource)
        "#{class_for_resource(resource)}-#{resource.id}"
      end

      def class_for_resource(resource)
        resource.class.name.underscore.dasherize
      end

      def user_btn(user, alt='No User Assigned')
        user ? link_to(user.full_name, url_for(:users, :show, id: user.id), class: 'btn btn-primary') : alt
      end

      def role_btn(role, alt='No role')
        role ? link_to(role.name, url_for(:roles, :show, id: role.id), class: 'btn btn-primary') : alt
      end

      # session helpers
      def current_user
        @current_user ||= User.includes(:company).find_by_id session[:user_id]
      end

      def current_company
        @current_company ||= (current_user ? current_user.company : nil)
      end

      def logged_in?
        !current_user.nil?
      end

      def redirect_non_authenticated_user
        unless logged_in?
          redirect_with_flash_message url_for(:sessions, :new), 
            :flash_type => :error, :flash_message => 'You must log in to perform that action'
        end
      end

      # Rendering and redirection helpers
      def render_template(template)
        render_template_with_flash_message(template, nil)
      end

      def render_template_with_flash_message(template, options={})
        populate_flash_message(options) if options
        render template
      end

      def redirect_with_flash_message(redirect_path, options={})
        populate_flash_message(options)
        redirect redirect_path
      end

      def redirect_to_root(flash_type=:success, message)
        redirect_with_flash_message '/',
          :flash_type => flash_type, :flash_message => message
      end

      def populate_flash_message(options={})
        flash[options.fetch(:flash_type, :notice)] = options.fetch(:flash_message, '')
      end
    end

    helpers ApplicationHelper
  end
end
