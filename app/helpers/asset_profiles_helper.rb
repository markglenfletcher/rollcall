# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module AssetProfilesHelper
      def find_asset_profile(id)
        current_company.asset_profiles.find_by_id(id)
      end

      def check_and_set_asset_profile
        @asset_profile = find_asset_profile(params[:id])
        unless @asset_profile
          redirect_with_flash_message url_for(:asset_profiles, :index),
            :flash_type => :error, :flash_message => 'That asset_profile does not exist!'
        end
      end
    end

    helpers AssetProfilesHelper
  end
end
