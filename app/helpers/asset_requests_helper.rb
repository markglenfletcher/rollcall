# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module AssetRequestsHelper
      def find_asset_request(id)
        current_company.asset_requests.find_by_id(id)
      end

      def check_and_set_asset_request
        @asset_request = find_asset_request(params[:id])
        unless @asset_request
          redirect_with_flash_message url_for(:asset_requests, :index), 
            :flash_type => :error, :flash_message => 'That Asset Request does not exist'
        end
      end

      def load_resources_for_asset_request_form
        @asset_profiles ||= current_company.asset_profiles.all
        @roles ||= current_company.roles.all
        @users ||= current_company.users.includes(:identities).all
      end
    end

    helpers AssetRequestsHelper
  end
end
