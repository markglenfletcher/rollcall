# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module CategoryHelper
      def find_category(id)
        current_company.categories.includes(:assets, :comments).find_by_id(params[:id])
      end

      def check_and_set_category
        @category = find_category(params[:id])
        unless @category
          redirect_with_flash_message url_for(:categories, :index),
            :flash_type => :error, :flash_message => 'That category does not exist!'
        end
      end
    end

    helpers CategoryHelper
  end
end
