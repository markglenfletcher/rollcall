# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module HomeHelper
      def go_to_app_root
        if current_company
          redirect_to url_for(:companies, :overview)
        else
          redirect_to url_for(:companies, :new)
        end
      end
    end

    helpers HomeHelper
  end
end
