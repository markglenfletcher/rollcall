# Helper methods defined here can be accessed in any controller or view in the application

module Rollcall
  class App
    module SignUpsHelper
      def check_and_set_signup
        @signup = SignUp.find_by_signup_code(params[:code])
        unless @signup
          redirect_to_root :error, "The code you are trying to use is invalid"
        end
      end

      def send_invitation_email(user)
        signup = SignUp.create(company: current_company, inviter: current_user, user: user)
        if signup
          deliver(
            :sign_up,
            :invitation_email,
            signup,
            absolute_url(:sign_ups, :accept_invitation, code: signup.signup_code)
          )
        end
      end
    end

    helpers SignUpsHelper
  end
end
