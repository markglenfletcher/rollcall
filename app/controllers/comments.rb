Rollcall::App.controllers :comments do
  before except: [:create] do
    check_and_set_comment
  end

  post :create, params: [:text, :user_id, :commentable_type, :commentable_id] do
    comment = Comment.new(params)
    if comment.save
      redirect_success 'Your comment has been created'
    else
      redirect_error 'Your comment could not be posted'
    end
  end

  put :update, params: [:id, :text, :user_id, :commentable_type, :commentable_id] do
    if @comment.update(params)
      redirect_success 'Your comment was edited'
    else
      redirect_error 'Your comment could not be updated'
    end
  end

  delete :destroy, params: [:id] do
    if @comment.destroy
      redirect_success 'Your comment was deleted'
    else
      redirect_error 'Your comment could not be deleted'
    end
  end
end
