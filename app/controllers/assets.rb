Rollcall::App.controllers :assets do
  HIDDEN_PARAMS = [
    :creator_id,
    :user_id,
    :asset_profile_id,
    :company_id
  ]

  FORM_PARAMS = [
    :name,
    :category_ids, 
    :assignment_status, 
    :health_status,
    :warranty_status,
    :warranty_type,
    :warranty_expiration_date,
    :purchase_date,
    :inspection_date,
    :serial_number,
    :description
  ]

  CHART_ACTIONS = [
    :chart_asset_health,
    :chart_asset_assignment,
    :chart_asset_warranty
  ]

  before do
    @assignment_statuses = Status::Assignment.names
    @health_statuses = Status::Health.names
    @warranty_statuses = Status::Warranty.names
  end

  before except: [:index, :new, :create, :statuses] + CHART_ACTIONS do
    check_and_set_asset
  end

  get :index, provides: [:html, :js], params: [:order, :dir, :page] do
    @title = "Viewing all Assets"
    @order = params[:order] ||= default_order_attribute
    @dir = params[:dir] ||= default_order_direction
    @page = params[:page] ||= 1
    @assets = current_company.assets.includes(user: [:identities])
                .order("#{@order} #{@dir}")
                .page(@page)
    case content_type
    when :js
      render_template 'js/index'
    else
      render_template 'index'
    end
  end

  get :show, with: :id do
    render_template 'show'
  end

  get :new, provides: [:html, :js], params: [:category_ids] do
    load_resources_for_asset_form
    @asset = Asset.new(params.merge(creator: current_user, company: current_company))
    case content_type
    when :js
      render_template 'js/new'
    else
      render_template 'new'
    end
  end

  post :create, params: HIDDEN_PARAMS + FORM_PARAMS do
    @asset = Asset.new(params)
    if @asset.save
      redirect_with_flash_message url_for(:assets, :index),
        :flash_type => :success, :flash_message => "Asset: #{@asset.name} has been created."
    else
      load_resources_for_asset_form
      render_template_with_flash_message 'new',
        :flash_type => :error, :flash_message => "Asset could not be created."
    end
  end

  get :edit, with: :id do
    load_resources_for_asset_form
    render_template 'edit'
  end

  put :update, params: [:id] + HIDDEN_PARAMS + FORM_PARAMS do
    @asset.modifying_user = current_user
    if @asset.update(params)
      redirect_with_flash_message url_for(:assets, :index),
        :flash_type => :success, :flash_message => "Asset: #{@asset.name} has been edited."
    else
      load_resources_for_asset_form
      render_template_with_flash_message 'edit',
        :flash_type => :error, :flash_message => "Asset could not be edited."
    end
  end

  delete :destroy, params: [:id] do
    if @asset.destroy
      redirect_with_flash_message url_for(:assets, :index),
        :flash_type => :success, :flash_message => 'Asset has been deleted.'
    else
      redirect_with_flash_message url_for(:assets, :index),
        :flash_type => :error, :flash_message => 'Asset could not be deleted.'
    end
  end

  post :clone, params: [:id] do
    cloned_asset = @asset.clone_asset
    if cloned_asset.persisted?
      redirect_with_flash_message url_for(:assets, :show, id: cloned_asset.id),
        :flash_type => :success, :flash_message => "Successfully cloned Asset: #{@asset.name}"
    else
      redirect_with_flash_message url_for(:assets, :index),
        :flash_type => :error, :flash_message => "Unable to clone the asset"
    end
  end

  get :statuses, params: [:type, :status, :page] do
    @type = params[:type]
    @status = params[:status]
    @title = "Viewing all Assets with #{@type} of #{@status}"
    @page = params[:page] ||= 1
    @assets = current_company.assets.includes(user:[:identities]).by_status(@type, @status).page(@page)
    render_template 'index'
  end

  get :chart_asset_health do
    current_company.assets.group(:health_status).count.to_json
  end

  get :chart_asset_assignment do
    current_company.assets.group(:assignment_status).count.to_json
  end

  get :chart_asset_warranty do
    current_company.assets.group(:warranty_status).count.to_json
  end
end
