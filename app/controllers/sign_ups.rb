Rollcall::App.controllers :sign_ups do
  before :create do 
    check_and_set_user(params[:user_id])
  end
  before :accept_invitation do
    check_and_set_signup
  end

  post :create, params: [:user_id] do
    if send_invitation_email(@user)
      redirect_with_flash_message url_for(:users, :show, id: @user.id),
        :flash_type => :success, :flash_message => 'An invitation has been sent'
    else
      redirect_with_flash_message url_for(:users, :show, id: @user.id),
        :flash_type => :error, :flash_message => 'Invitation could not be created'
    end
  end

  get :accept_invitation, params: [:code] do
    @invitee = @signup.user
    @signup.destroy
    render_template 'accept_invitation'
  end
end
