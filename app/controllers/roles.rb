Rollcall::App.controllers :roles do
  before except: [:index, :new, :create, :chart] do
    check_and_set_role
  end

  get :index do
    @roles = current_company.roles.includes(:assets, :users).all
    render_template 'index'
  end

  get :show, with: :id do
    render_template 'show'
  end

  get :new do
    load_resources_for_role_form
    @role = Role.new(company: current_company)
    render_template 'new'
  end

  get :edit, with: :id do
    load_resources_for_role_form
    render_template 'edit'
  end

  post :create, params: [:name, :staff_profile_id, :company_id] do
    @role = Role.new(params)
    if @role.save
      redirect_with_flash_message url_for(:roles, :index),
        :flash_type => :success, :flash_message => "Role: #{@role.name} has been created."
    else
      load_resources_for_role_form
      render_template_with_flash_message 'new',
        :flash_type => :error, :flash_message => "Role could not be created."
    end
  end

  put :update, params: [:id, :name, :staff_profile_id] do
    if @role.update(params)
      redirect_with_flash_message url_for(:roles, :index),
        :flash_type => :success, :flash_message => "Role: #{@role.name} has been edited."
    else
      load_resources_for_role_form
      render_template_with_flash_message 'new',
        :flash_type => :error, :flash_message => "Role could not be edited."
    end
  end

  delete :destroy, params: [:id] do
    if @role.destroy
      redirect_with_flash_message url_for(:roles, :index),
        :flash_type => :success, :flash_message => 'Role has been deleted.'
    else
      redirect_with_flash_message url_for(:roles, :index),
        :flash_type => :error, :flash_message => 'Role could not be deleted.'
    end
  end

  get :chart do
    current_company.roles.map { |r| [r.name, r.users.count] }.to_json
  end
end
