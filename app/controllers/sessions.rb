Rollcall::App.controllers :sessions do
  get :new do
    render 'new'
  end

  get :login, map: '/auth/google_oauth2' do
  end

  get :auth, map: '/auth/google_oauth2/callback' do
    find_user_and_login
    redirect_to_root "#{current_user.identities.first.email} Logged in successfully"
  end

  delete :destroy do
    session[:user_id] = nil
    redirect_to_root 'User has been logged out'
  end
end
