Rollcall::App.controllers :home do  
  get :index, :map => '/' do
    if logged_in?
      go_to_app_root
    else
      render_template 'index'
    end
  end

  get :about, :map => '/about' do
    render_template 'about'
  end
end
