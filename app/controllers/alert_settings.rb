Rollcall::App.controllers :alert_settings do
  before do
    @company = current_company
    send_to_new_if_company_doesnt_exist
    @settings = current_company.alert_settings
    @users = current_company.users
  end

  get :edit, map: '/companies/settings/alert_settings/edit' do
    render 'companies/settings/alert_settings/edit'
  end

  put :update, params: [:user_ids], map: '/companies/settings/alert_settings/update' do
    if @settings.update(params)
      redirect_with_flash_message '/companies/settings',
        flash_type: :success, flash_message: 'Alert settings have been saved.'
    else
      render 'companies/settings/alert_settings/edit'
    end
  end
end
