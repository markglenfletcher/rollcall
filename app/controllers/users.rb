Rollcall::App.controllers :users do
  before except: [:index, :new, :create, :my_profile] do
    check_and_set_user(params[:id])
  end

  get :index, params: [:order, :dir, :page] do
    @order = params[:order] ||= default_order_attribute
    @dir = params[:dir] ||= default_order_direction
    @page = params[:page] ||= 1
    @users = current_company.users.includes(:identities,:role,:assets)
      .order("#{@order} #{@dir}")
      .page(@page)
    render_template 'index'
  end

  get :show, with: :id do
    render_template 'show'
  end

  get :my_profile, map: '/me' do
    @user = find_user(current_user.id)
    render_template 'show'
  end

  get :new, provides: [:html, :js], params: [:role_id] do
    @user = User.new(params)
    load_resources_for_user_form
    case content_type
    when :js
      render_template 'js/new'
    when :html
      render_template 'new'
    end
  end

  get :edit, with: :id do
    load_resources_for_user_form
    render_template 'edit'
  end

  post :create, provides: [:html, :js], params: [:email, :role_id, :invite] do
    send_invitation = params.delete('invite')
    @user = User.new(params.merge(company: current_user.company))
    save_status = @user.save
    case content_type
    when :js
      @users = User.includes(:identities).all
      render_template 'js/create'
    when :html
      if save_status
        notification = "User: #{@user.email} has been created."
        notification << ' An invitation has been sent' if send_invitation && send_invitation_email(@user)
        redirect_with_flash_message url_for(:users, :index),
          :flash_type => :success, :flash_message => notification
      else
        load_resources_for_user_form
        render_template_with_flash_message 'new',
          :flash_type => :error, :flash_message => "User could not be created."
      end
    end
  end

  put :update, params: [:id, :email, :role_id] do
    if @user.update(params)
      redirect_with_flash_message url_for(:users, :index), 
        :flash_type => :success, :flash_message => "User: #{@user.email} has been edited."
    else
      load_resources_for_user_form
      render_template_with_flash_message 'edit',
        :flash_type => :error, :flash_message => "User could not be edited."
    end
  end

  delete :destroy, params: [:id] do
    if @user.destroy
      redirect_with_flash_message url_for(:users, :index), 
        :flash_type => :success, :flash_message => 'User has been deleted.'
    else
      redirect_with_flash_message url_for(:users, :index), 
        :flash_type => :error, :flash_message =>  'User could not be deleted.'
    end
  end
end
