Rollcall::App.controllers :asset_requests do
  before except: [:index, :create, :new] do
    check_and_set_asset_request
  end

  get :index do
    @asset_requests = current_company.asset_requests.all
    render_template 'index'
  end

  get :show, with: :id do
    render_template 'show'
  end

  get :new do
    load_resources_for_asset_request_form
    @asset_request = AssetRequest.new(requester: current_user, company: current_company)
    render_template 'new'
  end

  post :create, params: [:company_id, :due_date, :requester_id, :user_id, :asset_profile_ids, :manager_id] do
    @asset_request = AssetRequest.new(params)
    if @asset_request.save
      redirect_with_flash_message url_for(:asset_requests, :index),
        :flash_type => :success, :flash_message => "Asset Request for #{@asset_request.user.full_name} has been created."
    else
      load_resources_for_asset_request_form
      render_template_with_flash_message 'new',
        :flash_type => :error, :flash_message => "Asset Request could not be created."
    end
  end

  get :edit, with: :id do
    load_resources_for_asset_request_form
    render_template 'edit'
  end

  put :update, params: [:id, :due_date, :requester_id, :user_id, :asset_profile_ids, :manager_id] do
    if @asset_request.update(params)
      redirect_with_flash_message url_for(:asset_requests, :index),
        :flash_type => :success, :flash_message => "Asset Request for #{@asset_request.user.full_name} has been edited."
    else
      load_resources_for_asset_request_form
      render_template_with_flash_message 'edit',
        :flash_type => :error, :flash_message =>"Asset Request could not be edited."
    end
  end

  delete :destroy, params: [:id] do
    if @asset_request.destroy
      redirect_with_flash_message url_for(:asset_requests, :index),
        :flash_type => :success, :flash_message => 'Asset Request has been deleted.'
    else
      redirect_with_flash_message url_for(:asset_requests, :index),
        :flash_type => :error, :flash_message =>'Asset Request could not be deleted.'
    end
  end
end
