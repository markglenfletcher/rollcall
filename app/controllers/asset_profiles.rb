Rollcall::App.controllers :asset_profiles do
  before except: [:index, :new, :create] do
    check_and_set_asset_profile
  end

  get :index do
    @asset_profiles = current_company.asset_profiles.all
    render_template 'index'
  end

  get :show, with: :id do
    render_template 'show'
  end

  get :new, provides: [:html, :js] do
    @asset_profile = AssetProfile.new(company: current_company)
    case content_type
    when :js
      render_template 'js/new'
    when :html
      render_template 'new'
    end
  end

  get :edit, with: :id do
    render_template 'edit'
  end

  post :create, provides: [:html, :js], params: [:name, :company_id] do
    @asset_profile = AssetProfile.new(params)
    save_status = @asset_profile.save
    case content_type
    when :js
      @asset_profiles = current_company.asset_profiles.all
      render_template 'js/create'
    when :html
      if save_status
        redirect_with_flash_message url_for(:asset_profiles, :index),
          :flash_type => :success, :flash_message => "Asset Profile: #{@asset_profile.name} has been created."
      else
        render_template_with_flash_message 'new',
          :flash_type => :error, :flash_message => "Asset Profile could not be created."
      end
    end
  end

  put :update, params: [:id, :name] do
    if @asset_profile.update(params)
      redirect_with_flash_message url_for(:asset_profiles, :index),
        :flash_type => :success, :flash_message => "Asset Profile: #{@asset_profile.name} has been edited."
    else
      render_template_with_flash_message 'edit',
        :flash_type => :error, :flash_message => "Asset Profile could not be edited."
    end
  end

  delete :destroy, params: [:id] do
    if @asset_profile.destroy
      redirect_with_flash_message url_for(:asset_profiles, :index),
        :flash_type => :success, :flash_message => 'Asset Profile has been deleted.'
    else
      redirect_with_flash_message url_for(:asset_profiles, :index),
        :flash_type => :error, :flash_message => 'Asset Profile could not be deleted.'
    end
  end

  get :clone, with: :id do
    @assignment_statuses = Status::Assignment.names
    @health_statuses = Status::Health.names
    @warranty_statuses = Status::Warranty.names
    @categories = current_company.categories.all
    @users = current_company.users.includes(:identities).all
    @asset = Asset.from_asset_profile(@asset_profile, current_user)
    render_template 'assets/new'
  end
end
