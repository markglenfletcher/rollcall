Rollcall::App.controllers :categories do  
  before except: [:index, :new, :create, :chart] do
    check_and_set_category
  end

  get :index, params: [:order, :dir, :page] do
    @order = params[:order] ||= default_order_attribute
    @dir = params[:dir] ||= default_order_direction
    @page = params[:page] ||= 1
    @categories = current_company.categories.includes(:assets)
      .order("#{@order} #{@dir}")
      .page(@page)
    render_template 'index'
  end

  get :show, with: :id do
    render_template 'show'
  end

  get :new do
    @category = Category.new(user: current_user, company: current_company)
    render_template 'new'
  end

  post :create, params: [:name, :user_id, :company_id] do
    @category = Category.new(params)
    if @category.save
      redirect_with_flash_message url_for(:categories, :index),
        :flash_type => :success, :flash_message => "Category: #{@category.name} has been created."
    else
      render_template_with_flash_message 'new',
        :flash_type => :error, :flash_message => 'Category could not be created.'
    end
  end

  get :edit, with: :id do
    render_template 'edit'
  end

  put :update, params: [:id, :name, :user_id] do
    if @category.update(params)
      redirect_with_flash_message url_for(:categories, :index),
        :flash_type => :success, :flash_message => "Category: #{@category.name} has been edited."
    else
      render_template_with_flash_message 'new',
        :flash_type => :error, :flash_message => 'Category could not be edited.'
    end
  end

  delete :destroy do
    @category.destroy
    redirect_with_flash_message url_for(:categories, :index),
      :flash_type => :success, :flash_message => 'Category has been deleted.'
  end

  get :chart do
    current_company.categories.map do |cat| 
      [cat.name, cat.assets.count]
    end.to_json
  end
end
