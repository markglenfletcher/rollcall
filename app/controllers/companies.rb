Rollcall::App.controllers :companies do
	before do
		@company = current_company
	end

  before :create, :new do
    send_to_overview_if_company_exists
    @users = [current_user]
  end

  before :edit, :update, :overview, :settings do
    send_to_new_if_company_doesnt_exist
    @users = @company.users
  end

  get :overview do
    render_template 'overview'
  end

  get :settings do
    render_template 'settings'
  end

  get :new do
    @company = Company.new(owner: current_user)
  	render_template 'new'
  end

  post :create, params: [:name, :owner_id] do
    @company = Company.new(params) do |c|
      c.users << current_user
    end

    if @company.save
      redirect_with_flash_message url_for(:companies, :overview),
        :flash_type => :success, :flash_message => "Your company #{@company.name} has been created."
    else
      render_template 'new'
    end
  end

  get :edit do
    render_template 'edit'
  end

  put :update, params: [:name, :owner_id] do
    if @company.update(params)
      redirect_with_flash_message url_for(:companies, :overview),
        :flash_type => :success, :flash_message => "Your company #{@company.name} has been edited."
    else
      render_template 'edit'
    end
  end
end
