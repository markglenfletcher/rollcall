RollCall is an Asset Management application. This project aims to aid IT Managers to log their inventory of Software and Hardware assets and facilitate the acquisition and transfer of these items across their company and staff.

### DB ###
MySQL DB

* U: root

* P: <empty>

### Dependencies ###
```bundle install --path vendor/bundle```

### Setup ###
```bundle exec rake ar:create RACK_ENV=development && bundle exec rake ar:create RACK_ENV=test && bundle exec rake ar:migrate && bundle exec rake ar:migrate RACK_ENV=test && bundle exec rake db:seed && bundle exec rspec```

### Server ###
```bundle exec padrino s```

### Get yourself into the app ###
Login via Google

### Get Some Seed Data ###
```bundle exec padrino c```

Add your newly created user as to one of the pre-populated companies:

```c = Company.last; u = User.last; u.company = c; u.save```
