class AlertSettings < ActiveRecord::Base
  belongs_to :company
  has_many :users

  validates_presence_of :company

  validate :users_belong_to_company

  private

  def users_belong_to_company
    if users.present?
      errors.add(:users, 'must belong to the same company') if users.map(&:company_id).uniq.compact != [self.company_id]
    end
  end
end
