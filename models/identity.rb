class Identity < ActiveRecord::Base
  belongs_to :user

  validates_presence_of :provider, :uid

  def self.create_with_omniauth!(auth)
    identity = new(email: auth['info']['email'], first_name: auth['info']['first_name'],
      last_name: auth['info']['last_name'], image_url: auth['info']['image'],
      provider: auth['provider'], uid: auth['uid'])

    if identity.save
      identity
    else
      nil
    end
  end
end
