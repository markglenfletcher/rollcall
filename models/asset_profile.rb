class AssetProfile < ActiveRecord::Base
	include CompanyCompare
	
  belongs_to :company

  has_many :assets
  has_many :categories, through: :assets
  has_many :asset_request_items

  validates_presence_of :name, :company
end
