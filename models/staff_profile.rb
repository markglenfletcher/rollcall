class StaffProfile < ActiveRecord::Base
	include CompanyCompare
	
  belongs_to :company

  has_many :roles
  has_many :asset_profiles, through: :roles
  has_many :users, through: :roles

  validates_presence_of :name, :company
end
