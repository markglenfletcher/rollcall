class AssetStatusChange < ActiveRecord::Base
  belongs_to :user
  belongs_to :asset

  validates_presence_of :asset,
                        :user,
                        :changed_from,
                        :changed_to,
                        :changed_date

  validate :changed_date_is_not_future_dated
  validate :changed_to_and_from_are_not_identical

  def changed_to_status
    changed_to
  end

  def changed_from_status
    changed_from
  end

  private

  def changed_date_is_not_future_dated
    errors.add(:changed_date, 'cannot be future dated.') if changed_date && changed_date > Date.today
  end

  def changed_to_and_from_are_not_identical
    errors.add(:changed_from, 'cannot be identical to changed to.') if changed_to && changed_from && (changed_from == changed_to)
  end
end

class AssignmentStatusChange < AssetStatusChange
  def self.status_class
    Status::Assignment
  end
end

class HealthStatusChange < AssetStatusChange
  def self.status_class
    Status::Health
  end
end

class WarrantyStatusChange < AssetStatusChange
  def self.status_class
    Status::Warranty
  end
end

class UserStatusChange < AssetStatusChange
  def changed_from_status
    User.find_by_id(changed_from)
  end

  def changed_to_status
    User.find_by_id(changed_to)
  end
end
