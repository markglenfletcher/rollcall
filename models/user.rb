class User < ActiveRecord::Base
  include CompanyCompare
  self.per_page = 10

  belongs_to :company
  belongs_to :role
  belongs_to :alert_settings

  has_one :sign_up, :dependent => :destroy
  has_many :invitations, class_name: 'SignUp', foreign_key: 'inviter_id'

  has_many :identities, :dependent => :destroy
  has_many :assets
  has_many :asset_requests
  has_many :manager_asset_requests, class_name: 'AssetRequest', foreign_key:"manager_id"
  has_many :created_asset_requests, class_name: 'AssetRequest', foreign_key:"requester_id"

  has_many :comments, as: :commentable

  validates :email, presence: true, uniqueness: true
  validate :validate_company_scoped_associations

  delegate :first_name, :last_name, :image_url, to: :primary_identity

  def primary_identity
    @primary_identity ||= identities.first
  end

  def full_name
    if primary_identity
      "#{first_name} #{last_name}"
    else
      email
    end
  end

  def role_name
    role ? role.name : 'No role'
  end

  def self.create_with_omniauth!(auth)
    User.find_or_create_by(email: auth['info']['email']).tap do |u|
      u.identities << Identity.create_with_omniauth!(auth)
    end
  end

  def self.find_by_provider_and_uid(provider, uid)
    Identity.find_by_provider_and_uid(provider, uid).try(:user)
  end

  private

  def validate_company_scoped_associations
    verify_role_belongs_to_company
    verify_manager_assets_belong_to_same_company
    verify_created_assets_belong_to_same_company
  end

  def verify_role_belongs_to_company
    if role && !role.belongs_to_company?(company)
      errors.add(:role, 'must belong to the same company')
    end
  end

  def verify_manager_assets_belong_to_same_company
    if !manager_asset_requests.empty? && !asset_requests_belong_to_company?(manager_asset_requests)
      errors.add(:manager_asset_requests, 'must belong to the same company')
    end
  end

  def verify_created_assets_belong_to_same_company
    if !created_asset_requests.empty? && !asset_requests_belong_to_company?(created_asset_requests)
      errors.add(:created_asset_requests, 'must belong to the same company')
    end
  end

  def asset_requests_belong_to_company?(asset_requests)
    asset_requests.select do |ar| 
      ar.belongs_to_company?(company) 
    end.present?
  end
end
