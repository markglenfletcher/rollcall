class Company < ActiveRecord::Base
  validates_presence_of :name

  belongs_to :owner, class_name: 'User'
  has_many :users
  has_many :categories
  has_many :roles
  has_many :assets
  has_many :asset_profiles
  has_many :staff_profiles
  has_many :asset_requests
  has_many :sign_ups, dependent: :destroy
  has_one :alert_settings, dependent: :destroy

  before_validation :create_alert_settings

  def alertees
    alert_settings.users
  end

  private

  def create_alert_settings
    self.alert_settings ||= AlertSettings.create(company: self)
  end
end
