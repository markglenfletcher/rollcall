require 'securerandom'

class SignUp < ActiveRecord::Base
	belongs_to :company
	belongs_to :inviter, class_name: 'User'
	belongs_to :user

	validates_presence_of :company,
		:inviter,
		:user,
		:signup_code

	before_validation :assign_signup_code

	private

	def assign_signup_code
		self.signup_code = SignupCodeGeneration.generate_signup_code
	end

	module SignupCodeGeneration
		def self.generate_signup_code
			SecureRandom.urlsafe_base64(15)
		end
	end
end
