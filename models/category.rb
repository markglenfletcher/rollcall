class Category < ActiveRecord::Base
  include CompanyCompare
  self.per_page = 10
  
  belongs_to :user
  belongs_to :company

  has_and_belongs_to_many :assets

  has_many :comments, as: :commentable

  validates_presence_of :name, :user, :company

  validate :validate_company_scoped_associations

  private

  def validate_company_scoped_associations
    errors.add(:user, 'must belong to your company') if user && !user.belongs_to_company?(company)
    errors.add(:assets, 'all must belong to your company') if !assets.empty? && assets.select { |a| a.belongs_to_company?(company) }.empty?
  end
end
