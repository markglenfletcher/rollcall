class Asset < ActiveRecord::Base
  include ActiveModel::Dirty
  include CompanyCompare

  attr_accessor :modifying_user

  WARRANTY_EXPIRATION_LIMIT = 1.weeks.from_now
  INSPECTION_LIMIT = 1.weeks.from_now

  self.per_page = 10

  belongs_to :company
  belongs_to :user
  belongs_to :creator, class_name: 'User', foreign_key: :creator_id

  belongs_to :asset_profile

  has_and_belongs_to_many :categories
  has_many :comments, as: :commentable

  has_many :asset_status_changes

  has_many :asset_request_items
  has_many :asset_requests, through: :asset_request_items

  validates_presence_of :company,
                        :name, 
                        :creator,
                        :categories, 
                        :assignment_status,
                        :health_status,
                        :warranty_status

  validates :health_status, inclusion: { 
    in: Status::Health.names, 
    message: 'must be a valid health status'
  }
  validates :assignment_status, inclusion: { 
    in: Status::Assignment.names,
    message: 'must be a valid assignment status'
  }
  validates :warranty_status, inclusion: { 
    in: Status::Warranty.names,
    message: 'must be a valid warranty status'
  }

  validate :validate_assignment,
           :validate_warranty,
           :validate_dates,
           :validate_company_scoped_associations

  before_validation :adjust_assignment_status, 
                    :adjust_warranty_status
  before_save :create_asset_profile,
              :create_status_changes

  scope :by_status, -> (type,status) do
    [
      'health_status',
      'assignment_status',
      'warranty_status'
    ].include?(type) ? where(type => status) : []
  end

  scope :warranty_expiring, -> (company) do
    company.assets.where("warranty_expiration_date < ?", WARRANTY_EXPIRATION_LIMIT)
  end

  scope :inspection_due, -> (company) do
    company.assets.where("inspection_date < ?", INSPECTION_LIMIT)
  end

  def self.from_asset_profile(asset_profile, user)
    asset_profile.assets.build(name: asset_profile.name, creator: user, company: user.company)
  end

  def under_warranty?
    if warranty_expiration_date
      warranty_expiration_date >= Date.today
    else
      false
    end
  end

  def clone_asset
    company.assets.create(
      asset_profile: asset_profile,
      name: name,
      creator: creator,
      categories: categories,
      warranty_status: warranty_status,
      warranty_expiration_date: warranty_expiration_date,
      purchase_date: purchase_date,
      inspection_date: inspection_date,
      serial_number: serial_number,
      description: description,
      image_url: image_url,
      health_status: health_status,
      assignment_status: Status::Assignment::Unassigned.status_name,
      user: nil
    )
  end

  private

  # If no assignment status has been set, make it reflect that of the user assigned
  def adjust_assignment_status
    self.assignment_status ||= detect_assignment_status
  end

  def detect_assignment_status
    if user
      Status::Assignment::Assigned.status_name
    else
      Status::Assignment::Unassigned.status_name
    end
  end

  def adjust_warranty_status
    self.warranty_status ||= detect_warranty_status
  end

  def detect_warranty_status
    return Status::Warranty::NoWarranty.status_name unless warranty_expiration_date 
    if under_warranty?
      Status::Warranty::UnderWarranty.status_name
    else
      Status::Warranty::OutOfWarranty.status_name
    end
  end

  # Ensure that associated models are part of the same company!
  def validate_company_scoped_associations
    errors.add(:user, 'assignee must belong to your company') if user && !user.belongs_to_company?(company)
    errors.add(:creator, 'creator must belong to your company') if creator && !creator.belongs_to_company?(company)
    errors.add(:categories, 'categories must all belong to your company') if !categories.empty? && categories.select { |c| c.belongs_to_company?(company) }.empty?
    errors.add(:asset_profile, 'asset_profile must belong to the same company') if asset_profile && !asset_profile.belongs_to_company?(company)
  end

  def validate_dates
    if inspection_date && new_record? && inspection_date <= Date.today
      errors.add(:inspection_date, 'inspection date for a new asset cannot be in the past') 
    end
  end

  def validate_assignment
    if status_klass = Status::Assignment[assignment_status]
      assignment_errors = status_klass.new.validate(self).first
      errors.add(:assignment_status, assignment_errors) if assignment_errors
    end
  end

  def validate_warranty
    if status_klass = Status::Warranty[warranty_status]
      warranty_errors = status_klass.new.validate(self).first
      errors.add(:warranty_status, warranty_errors) if warranty_errors
    end
  end

  def create_asset_profile
    self.asset_profile ||= AssetProfile.create(name: name, company: company)
  end

  def create_status_changes
    add_status_change(AssignmentStatusChange, assignment_status_was, assignment_status) if assignment_status_has_changed?
    add_status_change(HealthStatusChange, health_status_was, health_status) if health_status_has_changed?
    add_status_change(WarrantyStatusChange, warranty_status_was, warranty_status) if warranty_status_has_changed?
    add_status_change(UserStatusChange, user_id_was, user_id) if user_has_changed?
  end

  def add_status_change(type, from, to)
    asset_status_changes << type.new(changed_from: from, changed_to: to, changed_date: Date.today, user: self.modifying_user)
  end

  def user_has_changed?
    user_id_changed? && !new_record?
  end

  def assignment_status_has_changed?
    assignment_status_changed? && !new_record?
  end

  def health_status_has_changed?
    health_status_changed? && !new_record?
  end

  def warranty_status_has_changed?
    warranty_status_changed? && !new_record?
  end
end
