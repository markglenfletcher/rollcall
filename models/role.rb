class Role < ActiveRecord::Base
  include CompanyCompare

  belongs_to :company
  belongs_to :staff_profile

  has_many :users
  has_many :assets, through: :users
  has_many :asset_profiles, through: :users

  has_many :comments, as: :commentable

  validates_presence_of :name, :company

  validate :validate_company_scoped_associations

  before_save :create_staff_profile

  private

  def validate_company_scoped_associations
    errors.add(:staff_profile, 'staff_profile must belong to your company') if staff_profile && !staff_profile.belongs_to_company?(company)
    errors.add(:users, 'users must all belong to your company') if !users.empty? && !users.select { |u| u.belongs_to_company?(company) }.empty?
  end

  def create_staff_profile
    self.staff_profile ||= new_staff_profile
  end

  def new_staff_profile
    StaffProfile.create(name: name, company: company)
  end
end
