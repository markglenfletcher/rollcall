class AssetRequestItem < ActiveRecord::Base
	belongs_to :asset_request
	belongs_to :asset_profile
	belongs_to :asset

	validates_presence_of :asset_request, :asset_profile
	validate :ensure_profile_asset_identity

	def complete?
		asset.present?
	end

	private 

	def ensure_profile_asset_identity
		errors.add(:asset, 'must have the same asset profile') if asset && asset.asset_profile != asset_profile
	end
end
