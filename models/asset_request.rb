class AssetRequest < ActiveRecord::Base
  include CompanyCompare

  DUE_LIMIT = 1.weeks.from_now

  belongs_to :company
  belongs_to :user
  belongs_to :requester, class_name: 'User', foreign_key: :requester_id
  belongs_to :manager, class_name: 'User', foreign_key: :manager_id

  has_many :asset_request_items
  has_many :asset_profiles, through: :asset_request_items
  has_many :assets, through: :asset_request_items

  alias_attribute :assignee, :user

  validates_presence_of :requester, :due_date, :company, :asset_request_items

  accepts_nested_attributes_for :user, :asset_profiles

  validate :date_cant_be_before_tomorrow,
    :validate_company_scoped_associations

  after_create :send_alert

  scope :due, -> (company) do
    company.asset_requests.where("due_date < ?", DUE_LIMIT)
  end

  def complete?
    asset_request_items.all?(&:complete?)
  end

  def before_tomorrow?
    self.due_date ||= Date.today
    due_date < Date.tomorrow
  end

  private

  def send_alert
    Alerters::AssetRequests::RequestCreatedAlerter.new(company,self).alert
  end

  def date_cant_be_before_tomorrow
    errors.add(:due_date, 'Due date cannot be before tomorrow') if before_tomorrow?
  end

  def validate_company_scoped_associations
    errors.add(:user, 'assignee must belong to your company') if user && !user.belongs_to_company?(company)
    errors.add(:requester, 'requester must belong to your company') if requester && !requester.belongs_to_company?(company)
    errors.add(:manager, 'manager must belong to your company') if manager && !manager.belongs_to_company?(company)
    errors.add(:asset_profiles, 'asset_profiles must all belong to your company') if !asset_profiles.empty? && asset_profiles.select { |ap| ap.belongs_to_company?(company) }.empty?
  end
end
