$(document).ready(function() {
// Asset form datepicker code
  $('input.datepicker').datepicker({
    format: 'dd/mm/yyyy'
  });

// Asset index modal code
  $('.container').on("click", '#new-asset-btn', function(){
    $('#new-asset-modal').load('/assets/new.js');
    $('#new-asset-modal').modal('show',{
      backdrop: 'static',
      keyboard: true
    });
  });

//User index clickable row
  $('.user-row').hover(function() {
    $(this).toggleClass('active');
  });

  $('#user-table').on('click','.user-row', function() {
    var location = $(this).attr('data-location');
    document.location.href = location;
  });

//Role index clickable row
  $('.role-row').hover(function() {
    $(this).toggleClass('active');
  });

  $('#role-table').on('click','.role-row', function() {
    var location = $(this).attr('data-location');
    document.location.href = location;
  });

// Asset Request form code
  $('#request-form').on("click", "#create_user_btn", function() {
    $("#new-user" ).load("/users/new.js");
    $('#create_user_btn').addClass('hidden');
  });

  $('#request-form').on("click", "#submit_user_btn", function() {
    $.post(
      '/users/create.js',
      $('#user-fields :input').serialize(),
      function(data) {
        $('#user-sect').html(data);
      },
      'html'
    );
  });

  $('#request-form').on("click", "#create_asset_profile_btn", function() {
    $("#new-asset" ).load("/asset_profiles/new.js");
    $('#create_asset_profile_btn').addClass('hidden');
  });

  $('#request-form').on("click", "#submit_asset_profile_btn", function() {
    $.post(
      '/asset_profiles/create.js',
      $('#asset-fields :input').serialize(),
      function(data) {
        $('#assets-sect').html(data);
      },
      'html'
    );
  });
});