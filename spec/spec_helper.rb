require 'rubygems'

require 'simplecov'
require 'simplecov-csv'
SimpleCov.formatters = 
  [ 
    SimpleCov::Formatter::CSVFormatter,
    SimpleCov::Formatter::HTMLFormatter
  ]
SimpleCov.coverage_dir(ENV["COVERAGE_REPORTS"])
SimpleCov.start do
  add_filter '/spec/'
  add_filter '/config/'
  add_group "Models", "models"
  add_group "Controllers", "app/controllers"
  add_group "Helpers", "app/helpers"
  add_group "Mailers", "app/mailers"
end

RACK_ENV = 'test' unless defined?(RACK_ENV)
require File.expand_path(File.dirname(__FILE__) + "/../config/boot")
Dir[File.expand_path(File.dirname(__FILE__) + "/../app/helpers/**/*.rb")].each(&method(:require))

require 'capybara/rspec'

RSpec.configure do |conf|
  conf.include Rack::Test::Methods

  conf.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  conf.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end

# You can use this method to custom specify a Rack app
# you want rack-test to invoke:
#
#   app Rollcall::App
#   app Rollcall::App.tap { |a| }
#   app(Rollcall::App) do
#     set :foo, :bar
#   end
#
def app(app = nil, &blk)
  @app ||= block_given? ? app.instance_eval(&blk) : app
  @app ||= Padrino.application
end

Capybara.app = app

def login_user(user)
  set_oauth_for user
  get '/auth/google_oauth2/callback'
end

def capybara_login_user(user)
  set_oauth_for user
  visit '/auth/google_oauth2/callback'
end

def set_oauth_for(user)
  id = user.identities.first

  OmniAuth.config.test_mode = true
  OmniAuth.config.add_mock(:google_oauth2, {
    'provider' => id.provider,
    'uid' => id.uid,
    'info' => {
      'email' => id.email,
      'first_name' => id.first_name,
      'last_name' => id.last_name,
      'image' => id.image_url
    }
  })
end

def assert_redirects_to_companies_new(response)
  assert_redirects_to_path response, '/companies/new'
end

def assert_redirects_to_companies_overview(response)
  assert_redirects_to_path response, '/companies/overview'
end

def assert_redirects_to_path(response, path)
  expect(response).to be_redirect
  expect(response.location).to include(path)
end
