require 'spec_helper'

RSpec.describe Status::Warranty do
  context '::keys' do
    it 'should have the correct keys' do
      keys = Status::Warranty.keys
      expect(keys).to include('under_warranty')
      expect(keys).to include('no_warranty')
      expect(keys).to include('out_of_warranty')
    end
  end

  context '::names' do
    it 'should have the correct names' do
      keys = Status::Warranty.names
      expect(keys).to include('Under Warranty')
      expect(keys).to include('No Warranty')
      expect(keys).to include('Out Of Warranty')
    end
  end
end

RSpec.describe Status::Warranty::UnderWarranty do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Warranty::UnderWarranty.status_key).to eq('under_warranty')
      expect(Status::Warranty::UnderWarranty.new.status_key).to eq('under_warranty')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Warranty::UnderWarranty.status_name).to eq('Under Warranty')
      expect(Status::Warranty::UnderWarranty.new.status_name).to eq('Under Warranty')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Warranty::UnderWarranty.status_class).to eq('btn-success')
      expect(Status::Warranty::UnderWarranty.new.status_class).to eq('btn-success')
    end
  end
  
  context 'validations' do
    context 'invalid without warranty_expiration_date' do
      let(:asset) { double('asset', warranty_expiration_date: nil) }
      it 'should have an error message' do
        expect(Status::Warranty::UnderWarranty.new.validate(asset)).to include('cannot be under warranty if no warranty expiration date has been set!')
      end
    end

    context 'invalid if warranty_expiration_date is in the past' do
      let(:asset) { double('asset', warranty_expiration_date: Date.yesterday) }
      it 'should have an error message' do
        expect(Status::Warranty::UnderWarranty.new.validate(asset)).to include('cannot be under warranty if warranty expiration date has passed!')
      end
    end

    context 'valid with warranty_expiration_date in future' do
      let(:asset) { double('asset', warranty_expiration_date: Date.tomorrow) }
      it 'should have no error message' do
        expect(Status::Warranty::UnderWarranty.new.validate(asset)).to be_empty
      end
    end
  end
end

RSpec.describe Status::Warranty::NoWarranty do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Warranty::NoWarranty.status_key).to eq('no_warranty')
      expect(Status::Warranty::NoWarranty.new.status_key).to eq('no_warranty')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Warranty::NoWarranty.status_name).to eq('No Warranty')
      expect(Status::Warranty::NoWarranty.new.status_name).to eq('No Warranty')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Warranty::NoWarranty.status_class).to eq('btn-danger')
      expect(Status::Warranty::NoWarranty.new.status_class).to eq('btn-danger')
    end
  end
  
  context 'validations' do
    context 'invalid with warranty_expiration_date' do
      let(:asset) { double('asset', warranty_expiration_date: Date.tomorrow) }
      it 'should have an error message' do
        expect(Status::Warranty::NoWarranty.new.validate(asset)).to include('cannot have no warranty if warranty expiration date has been set!')
      end
    end

    context 'valid without warranty_expiration_date' do
      let(:asset) { double('asset', warranty_expiration_date: nil) }
      it 'should have no error message' do
        expect(Status::Warranty::NoWarranty.new.validate(asset)).to be_empty
      end
    end
  end
end

RSpec.describe Status::Warranty::OutOfWarranty do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Warranty::OutOfWarranty.status_key).to eq('out_of_warranty')
      expect(Status::Warranty::OutOfWarranty.new.status_key).to eq('out_of_warranty')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Warranty::OutOfWarranty.status_name).to eq('Out Of Warranty')
      expect(Status::Warranty::OutOfWarranty.new.status_name).to eq('Out Of Warranty')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Warranty::OutOfWarranty.status_class).to eq('btn-primary')
      expect(Status::Warranty::OutOfWarranty.new.status_class).to eq('btn-primary')
    end
  end
  
  context 'validations' do
    # No valdations 
  end
end
