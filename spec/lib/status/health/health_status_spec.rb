require 'spec_helper'

RSpec.describe Status::Health do
  context '::keys' do
    it 'should have the correct keys' do
      keys = Status::Health.keys
      expect(keys).to include('healthy')
      expect(keys).to include('due_an_upgrade')
      expect(keys).to include('out_for_repair')
      expect(keys).to include('needs_repair')
      expect(keys).to include('dead')
    end
  end

  context '::names' do
    it 'should have the correct names' do
      keys = Status::Health.names
      expect(keys).to include('Healthy')
      expect(keys).to include('Due An Upgrade')
      expect(keys).to include('Out For Repair')
      expect(keys).to include('Needs Repair')
      expect(keys).to include('Dead')
    end
  end
end

RSpec.describe Status::Health::Healthy do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Health::Healthy.status_key).to eq('healthy')
      expect(Status::Health::Healthy.new.status_key).to eq('healthy')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Health::Healthy.status_name).to eq('Healthy')
      expect(Status::Health::Healthy.new.status_name).to eq('Healthy')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Health::Healthy.status_class).to eq('btn-success')
      expect(Status::Health::Healthy.new.status_class).to eq('btn-success')
    end
  end
  
  context 'validations' do
    # No Validations Currently
  end
end

RSpec.describe Status::Health::DueAnUpgrade do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Health::DueAnUpgrade.status_key).to eq('due_an_upgrade')
      expect(Status::Health::DueAnUpgrade.new.status_key).to eq('due_an_upgrade')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Health::DueAnUpgrade.status_name).to eq('Due An Upgrade')
      expect(Status::Health::DueAnUpgrade.new.status_name).to eq('Due An Upgrade')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Health::DueAnUpgrade.status_class).to eq('btn-info')
      expect(Status::Health::DueAnUpgrade.new.status_class).to eq('btn-info')
    end
  end
  
  context 'validations' do
    # No validations currently
  end
end

RSpec.describe Status::Health::OutForRepair do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Health::OutForRepair.status_key).to eq('out_for_repair')
      expect(Status::Health::OutForRepair.new.status_key).to eq('out_for_repair')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Health::OutForRepair.status_name).to eq('Out For Repair')
      expect(Status::Health::OutForRepair.new.status_name).to eq('Out For Repair')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Health::OutForRepair.status_class).to eq('btn-default')
      expect(Status::Health::OutForRepair.new.status_class).to eq('btn-default')
    end
  end
  
  context 'validations' do
    # No validations currently
  end
end

RSpec.describe Status::Health::NeedsRepair do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Health::NeedsRepair.status_key).to eq('needs_repair')
      expect(Status::Health::NeedsRepair.new.status_key).to eq('needs_repair')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Health::NeedsRepair.status_name).to eq('Needs Repair')
      expect(Status::Health::NeedsRepair.new.status_name).to eq('Needs Repair')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Health::NeedsRepair.status_class).to eq('btn-primary')
      expect(Status::Health::NeedsRepair.new.status_class).to eq('btn-primary')
    end
  end

  context 'validations' do
    # No validations currently
  end
end

RSpec.describe Status::Health::Dead do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Health::Dead.status_key).to eq('dead')
      expect(Status::Health::Dead.new.status_key).to eq('dead')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Health::Dead.status_name).to eq('Dead')
      expect(Status::Health::Dead.new.status_name).to eq('Dead')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Health::Dead.status_class).to eq('btn-danger')
      expect(Status::Health::Dead.new.status_class).to eq('btn-danger')
    end
  end

  context 'validations' do
    # No validations currently
  end
end
