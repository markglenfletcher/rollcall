require 'spec_helper'

RSpec.describe Status::AssetStatus do
  context '::status_name' do
    it 'should return a readable name' do
      expect(Status::AssetStatus.status_name).to eq('Asset Status')
    end
  end

  context '#status_name' do
    it 'should return a readable name' do
      expect(Status::AssetStatus.new.status_name).to eq('Asset Status')
    end
  end

  context '::status_key' do
    it 'should return a readable key' do
      expect(Status::AssetStatus.status_key).to eq('asset_status')
    end
  end

  context '#status_key' do
    it 'should return a readable key' do
      expect(Status::AssetStatus.new.status_key).to eq('asset_status')
    end
  end

  context '::status_class' do
    it 'should return a css class' do
      expect(Status::AssetStatus.status_class).to eq('btn-primary')
    end
  end

  context '#status_class' do
    it 'should return a css class' do
      expect(Status::AssetStatus.new.status_class).to eq('btn-primary')
    end
  end

  context '#validate' do
    let(:asset) { Object.new }

    it 'should return an empty array' do
      expect(Status::AssetStatus.new.validate(asset)).to eq([])
    end
  end
end
