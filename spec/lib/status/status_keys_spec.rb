require 'spec_helper'

RSpec.describe Status::StatusKeys do
  module TestStatusModule
    extend Status::StatusKeys
    class TestStatusOne < Status::AssetStatus; end
    class TestStatusTwo < Status::AssetStatus; end
  end

  context '::[]' do
    it 'should return the correct constant given a key' do
      expect(TestStatusModule['test_status_one']).to eq(TestStatusModule::TestStatusOne)
      expect(TestStatusModule[:test_status_two]).to eq(TestStatusModule::TestStatusTwo)
    end

    it 'should return the correct constant given a name' do
      expect(TestStatusModule['Test Status One']).to eq(TestStatusModule::TestStatusOne)
      expect(TestStatusModule['Test Status Two']).to eq(TestStatusModule::TestStatusTwo)
    end
  end

  context '::pairs' do
    it 'should return the correct list of kvps' do
      expect(TestStatusModule.pairs).to eq([['Test Status One','test_status_one'], ['Test Status Two','test_status_two']])
    end
  end

  context '::keys' do
    it 'should return the correct list of keys' do
      expect(TestStatusModule.keys).to eq(['test_status_one', 'test_status_two'])
    end
  end

  context '::names' do
    it 'should return the correct list of names' do
      expect(TestStatusModule.names).to eq(['Test Status One', 'Test Status Two'])
    end
  end
end
