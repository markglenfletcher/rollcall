require 'spec_helper'

RSpec.describe Status::Assignment do
  context '::keys' do
    it 'should have the correct keys' do
      keys = Status::Assignment.keys
      expect(keys).to include('assigned')
      expect(keys).to include('assignment_in_progress')
      expect(keys).to include('on_order')
      expect(keys).to include('reserved')
      expect(keys).to include('unassigned')
    end
  end

  context '::names' do
    it 'should have the correct names' do
      keys = Status::Assignment.names
      expect(keys).to include('Assigned')
      expect(keys).to include('Assignment In Progress')
      expect(keys).to include('On Order')
      expect(keys).to include('Reserved')
      expect(keys).to include('Unassigned')
    end
  end
end

RSpec.describe Status::Assignment::Assigned do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Assignment::Assigned.status_key).to eq('assigned')
      expect(Status::Assignment::Assigned.new.status_key).to eq('assigned')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Assignment::Assigned.status_name).to eq('Assigned')
      expect(Status::Assignment::Assigned.new.status_name).to eq('Assigned')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Assignment::Assigned.status_class).to eq('btn-danger')
      expect(Status::Assignment::Assigned.new.status_class).to eq('btn-danger')
    end
  end

  context 'validations' do
    context 'invalid without user' do
      let(:asset) { double('asset', user: nil) }
      it 'should have an error message' do
        expect(Status::Assignment::Assigned.new.validate(asset)).to_not be_empty
        expect(Status::Assignment::Assigned.new.validate(asset)).to include('cannot be set to assigned without a user')
      end
    end

    context 'valid with user' do
      let(:asset) { double('asset', user: Object.new) }
      it 'should have no error message' do
        expect(Status::Assignment::Assigned.new.validate(asset)).to be_empty
      end
    end
  end
end

RSpec.describe Status::Assignment::AssignmentInProgress do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Assignment::AssignmentInProgress.status_key).to eq('assignment_in_progress')
      expect(Status::Assignment::AssignmentInProgress.new.status_key).to eq('assignment_in_progress')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Assignment::AssignmentInProgress.status_name).to eq('Assignment In Progress')
      expect(Status::Assignment::AssignmentInProgress.new.status_name).to eq('Assignment In Progress')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Assignment::AssignmentInProgress.status_class).to eq('btn-info')
      expect(Status::Assignment::AssignmentInProgress.new.status_class).to eq('btn-info')
    end
  end
  
  context 'validations' do
    # No validations currently
  end
end

RSpec.describe Status::Assignment::Reserved do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Assignment::Reserved.status_key).to eq('reserved')
      expect(Status::Assignment::Reserved.new.status_key).to eq('reserved')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Assignment::Reserved.status_name).to eq('Reserved')
      expect(Status::Assignment::Reserved.new.status_name).to eq('Reserved')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Assignment::Reserved.status_class).to eq('btn-default')
      expect(Status::Assignment::Reserved.new.status_class).to eq('btn-default')
    end
  end
  
  context 'validations' do
    context 'invalid without user' do
      let(:asset) { double('asset', user: nil) }
      it 'should have an error message' do
        expect(Status::Assignment::Reserved.new.validate(asset)).to_not be_empty
        expect(Status::Assignment::Reserved.new.validate(asset)).to include('cannot be set to reserved without a user')
      end
    end

    context 'valid with user' do
      let(:asset) { double('asset', user: Object.new) }
      it 'should have no error message' do
        expect(Status::Assignment::Reserved.new.validate(asset)).to be_empty
      end
    end
  end
end

RSpec.describe Status::Assignment::Unassigned do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Assignment::Unassigned.status_key).to eq('unassigned')
      expect(Status::Assignment::Unassigned.new.status_key).to eq('unassigned')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Assignment::Unassigned.status_name).to eq('Unassigned')
      expect(Status::Assignment::Unassigned.new.status_name).to eq('Unassigned')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Assignment::Unassigned.status_class).to eq('btn-success')
      expect(Status::Assignment::Unassigned.new.status_class).to eq('btn-success')
    end
  end
  
  context 'validations' do
    context 'valid without user' do
      let(:asset) { double('asset', user: nil) }
      it 'should have no error message' do
        expect(Status::Assignment::Unassigned.new.validate(asset)).to be_empty
      end
    end

    context 'invalid with user' do
      let(:asset) { double('asset', user: Object.new) }
      it 'should have an error message' do
        expect(Status::Assignment::Unassigned.new.validate(asset)).to_not be_empty
        expect(Status::Assignment::Unassigned.new.validate(asset)).to include('cannot be set to unassigned with a user')
      end
    end
  end
end

RSpec.describe Status::Assignment::OnOrder do
  context 'status_key' do
    it 'should have the correct key' do
      expect(Status::Assignment::OnOrder.status_key).to eq('on_order')
      expect(Status::Assignment::OnOrder.new.status_key).to eq('on_order')
    end
  end

  context 'status_name' do
    it 'should have the correct key' do
      expect(Status::Assignment::OnOrder.status_name).to eq('On Order')
      expect(Status::Assignment::OnOrder.new.status_name).to eq('On Order')
    end
  end

  context 'status_class' do
    it 'should have the correct class' do
      expect(Status::Assignment::OnOrder.status_class).to eq('btn-primary')
      expect(Status::Assignment::OnOrder.new.status_class).to eq('btn-primary')
    end
  end
  
  context 'validations' do
    # No validations currently
  end
end
