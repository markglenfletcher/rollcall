require 'spec_helper'

RSpec.describe Alerters::Assets::ExpiringWarrantiesAlerter do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) { company.users.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') } }
  let(:category) { company.categories.create(name:'Category', user: user) }
  let(:asset) do
    company.assets.create(
      name: 'Hardware Asset',
      user: user,
      creator: user,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      categories: [
        category
      ])
  end

  before do
    company.alert_settings.users << user
    company.save
  end

  context 'with expiring assets' do
    before { asset }

    it 'should call deliver on app once per user' do
      expect(Rollcall::App).to receive(:deliver).with(:asset_alert, :expiring_warranties, user, [asset]).once
      Alerters::Assets::ExpiringWarrantiesAlerter.new(company).alert
    end

    context '::alert' do
      it 'should call deliver for all eligible companies' do
        expect(Rollcall::App).to receive(:deliver).with(:asset_alert, :expiring_warranties, user, [asset]).once
        Alerters::Assets::ExpiringWarrantiesAlerter.alert
      end
    end
  end

  context 'with no expiring assets' do
    it 'should not call deliver' do
      expect(Rollcall::App).to_not receive(:deliver)
      Alerters::Assets::ExpiringWarrantiesAlerter.new(company).alert
    end

    context '::alert' do
      it 'should not call deliver' do
        expect(Rollcall::App).to_not receive(:deliver)
        Alerters::Assets::ExpiringWarrantiesAlerter.alert
      end
    end
  end
end
