require 'spec_helper'

RSpec.describe Alerters::AssetRequests::RequestsDueAlerter do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) { company.users.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') } }
  let(:asset_profile_macbook) { company.asset_profiles.create(name: 'Macbook') }
  let(:asset_request) do
    company.asset_requests.create(
      due_date: 1.day.from_now.to_date.to_s,
      manager: user,
      requester: user,
      user: user,
      asset_profiles: [asset_profile_macbook]
    )
  end

  before do
    company.alert_settings.users << user
    company.save
  end

  context 'with due asset_requests' do
    before { asset_request }

    it 'should call deliver on app once per user' do
      expect(Rollcall::App).to receive(:deliver).with(:asset_request_alert, :requests_due, user, [asset_request]).once
      Alerters::AssetRequests::RequestsDueAlerter.new(company).alert
    end

    context '::alert' do
      it 'should call deliver for all eligible companies' do
        expect(Rollcall::App).to receive(:deliver).with(:asset_request_alert, :requests_due, user, [asset_request]).once
        Alerters::AssetRequests::RequestsDueAlerter.alert
      end
    end
  end

  context 'with no asset_requests due' do
    it 'should not call deliver' do
      expect(Rollcall::App).to_not receive(:deliver)
      Alerters::AssetRequests::RequestsDueAlerter.new(company).alert
    end

    context '::alert' do
      it 'should not call deliver' do
        expect(Rollcall::App).to_not receive(:deliver)
        Alerters::AssetRequests::RequestsDueAlerter.alert
      end
    end
  end
end
