require 'spec_helper'

RSpec.describe "AssetsController" do
  context 'when logged in' do
    let(:company) { Company.create(name: 'Design Agency') }
    let(:user) do
      company.users.create(email: 'a@b.com') do |u|
        u.identities.build(provider: 'provider', uid: 'uid',
          first_name: 'first', last_name: 'last',
          email: 'a@b.com', image_url: 'http://img.com/i.png')
      end
    end
    let(:category) { company.categories.create(name: 'Hardware', user: user) }
    let(:attr) do
      {
        name: 'Macbook',
        creator_id: user.id,
        user_id: user.id,
        assignment_status: Status::Assignment::Assigned.status_name,
        health_status: Status::Health::Healthy.status_name,
        warranty_status: Status::Warranty::UnderWarranty.status_name,
        warranty_expiration_date: Date.tomorrow.to_date.to_s,
        category_ids: [
          category.id
        ],
        company_id: company.id
      }
    end
    let(:session) do
      {  
        'rack.session' => {
          csrf: 'a',
          user_id: user.id
        }
      }
    end
    let(:asset) { Asset.create(attr) }

    before do
      login_user user
    end

    context 'INDEX' do
      it "responds success" do
        get "/assets", session do |res|
          expect(res).to be_ok
        end
      end
    end

    context "SHOW" do
      it 'responds success' do
        get "/assets/show/#{asset.id}", session do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to assets index if non existent' do
        get "/assets/show/999999", session do |res|
          assert_redirects_to_assets_index(res)
        end
      end
    end

    context "NEW" do
      context 'html request' do
        it "responds success" do
          get "/assets/new", session do |res|
            expect(res).to be_ok
          end
        end
      end

      context 'js request' do
        it "responds success" do
          get "/assets/new.js", session do |res|
            expect(res).to be_ok
          end
        end
      end

      context 'with a category id' do
        it 'responds success' do
          get "/assets/new", { category_ids: category.id }, session do |res|
            expect(res).to be_ok
          end
        end
      end
    end

    context "EDIT" do
      it "responds success" do
        get "/assets/edit/#{asset.id}", session do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to assets index if non existent' do
        get "/assets/edit/999999", session do |res|
          assert_redirects_to_assets_index(res)
        end
      end
    end

    context "CREATE" do
      it 'creates a new asset with valid params' do
        expect do
          post '/assets/create', attr.merge(authenticity_token: 'a'), session do |res|
            assert_redirects_to_assets_index(res)
          end
        end.to change(Asset, :count).by(1)
      end

      it 'doesnt create a new asset with invalid params' do
        expect do
          post '/assets/create', {authenticity_token: 'a', name: ''}, session do |res|
            expect(res).to be_ok
          end
        end.to change(Asset, :count).by(0)
      end
    end

    context "UPDATE" do
      it 'changes the current asset with valid params' do
        new_name = 'New Asset'
        put '/assets/update', {authenticity_token: 'a', id: asset.id, name: new_name}, session do |res|
          assert_redirects_to_assets_index(res)
        end
        expect(asset.reload.name).to eql(new_name)
      end

      it 'doesnt change the current asset with invalid params' do
        put '/assets/update', {authenticity_token: 'a', id: asset.id, name: ''}, session do |res|
          expect(res).to be_ok
        end
        expect(asset.reload.name).to eql(attr[:name])
      end

      it 'should redirect to assets index if asset doesnt exist' do
        put '/assets/update', {authenticity_token: 'a', id: '99999', name: 'New Name'}, session do |res|
          assert_redirects_to_assets_index(res)
        end
      end
    end

    context "DESTROY" do
      it 'should delete the asset' do
        id = asset.id
        expect do
          delete '/assets/destroy', {authenticity_token: 'a', id: asset.id}, session do |res|
            assert_redirects_to_assets_index(res)
          end
        end.to change(Asset,:count).by(-1)
        expect(Asset.find_by_id(id)).to be_nil
      end

      it 'should redirect to assets index if asset doesnt exist' do
        delete '/assets/destroy', {authenticity_token: 'a', id: '99999'}, session do |res|
          assert_redirects_to_assets_index(res)
        end
      end
    end

    context 'STATUSES' do
      it 'should respond success' do
        get '/assets/statuses', {authenticity_token: 'a', type:'health_status', status:'Healthy'}, session do |res|
          expect(res).to be_ok
        end
      end
    end

    context 'CLONE' do
      it 'should create a new asset' do
        asset.touch
        expect do 
          post '/assets/clone', { authenticity_token: 'a', id: asset.id }, session do |res|
            expect(res).to be_redirect
            expect(res.location).to include('/assets/show/')
          end
        end.to change(Asset, :count).by(1)
      end

      it 'should redirect to assets index if asset to be cloned does not exist' do
        expect do 
          post '/assets/clone', { authenticity_token: 'a', id: '999999' }, session do |res|
            assert_redirects_to_assets_index(res)
          end
        end.to change(Asset, :count).by(0)
      end
    end

    context 'CHARTS' do
      before do
        asset.touch
      end

      context 'CHARTS_ASSET_HEALTH' do
        it 'contains the correct json' do
          expected = '{"Healthy":1}'
          get '/assets/chart_asset_health' do |res|
            expect(res).to be_ok
            expect(res.body).to eq(expected)
          end
        end
      end

      context 'CHARTS_ASSET_ASSIGNMENT' do
        it 'contains the correct json' do
          expected = '{"Assigned":1}'
          get '/assets/chart_asset_assignment' do |res|
            expect(res).to be_ok
            expect(res.body).to eq(expected)
          end
        end
      end

      context 'CHARTS_ASSET_WARRANTY' do
        it 'contains the correct json' do
          expected = '{"Under Warranty":1}'
          get '/assets/chart_asset_warranty' do |res|
            expect(res).to be_ok 
            expect(res.body).to eq(expected)
          end
        end
      end
    end
  end

  context 'when not logged in' do
    it 'should redirect to root path' do
      get '/assets/new' do |res|
        expect(res).to be_redirect
      end
    end
  end

  def assert_redirects_to_assets_index(response)
    expect(response).to be_redirect
    expect(response.location).to match(/\/assets\z/)
  end
end
