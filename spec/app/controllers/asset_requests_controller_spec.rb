require 'spec_helper'

RSpec.describe "AssetRequestsController" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    company.users.create(email: 'a@b.com') do |u| 
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end 
  let(:asset_profile_macbook) { company.asset_profiles.create(name: 'Macbook') }
  let(:attr) do
    {
      due_date: 1.year.from_now.to_date.to_s,
      requester_id: user.id,
      user_id: user.id,
      asset_profile_ids: [ asset_profile_macbook.id ],
      manager_id: user.id,
      company_id: company.id
    }
  end
  let(:session) do
   {
      'rack.session' => {
        csrf: 'a',
        user_id: user.id
      }
    }
  end
  let(:asset_request) { AssetRequest.create(attr) }

  context 'when logged in' do
    before do
      login_user user
    end

    context 'INDEX' do
      it 'responds success' do
        get '/asset_requests' do |res|
          expect(res).to be_ok
        end
      end
    end

    context 'SHOW' do
      it 'responds success' do
        get "/asset_requests/show/#{asset_request.id}", session do |res|
          expect(res).to be_ok
        end
      end

      it 'redirect if non existent' do
        get "/asset_requests/show/999999" do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context 'NEW' do
      it 'should respond success' do
        get "/asset_requests/new" do |res|
          expect(res).to be_ok
        end
      end
    end

    context 'CREATE' do
      it 'should create a new asset request with valid params' do
        expect do
          post '/asset_requests/create', attr.merge(authenticity_token: 'a'), session do |res|
            assert_redirected_to_index(res)
          end
        end.to change(AssetRequest, :count).by(1)
      end

      it 'should not create a new asset request with invalid params' do
        expect do
          post '/asset_requests/create', {authenticity_token: 'a', company_id: 999999}, session do |res|
            expect(res).to be_ok
          end
        end.to change(AssetRequest, :count).by(0)
      end
    end

    context 'EDIT' do
      it 'should respond success' do
        get "/asset_requests/edit/#{asset_request.id}" do |res|
          expect(res).to be_ok
        end
      end

      it 'should redirect if non existent' do
        get "/asset_requests/edit/999999" do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context 'UPDATE' do
      it 'should change the resource with valid params' do
        new_due_date = 2.years.from_now.to_date.to_s
        put '/asset_requests/update', attr.merge(id: asset_request.id, authenticity_token: 'a', due_date: new_due_date), session do |res|
          assert_redirected_to_index(res)
          expect(asset_request.reload.due_date.to_s).to eql(new_due_date)
        end
      end

      it 'should not change the resource with invalid params' do
        put '/asset_requests/update', attr.merge(id: asset_request.id, authenticity_token: 'a', due_date: ''), session do |res|
          expect(res).to be_ok
          expect(asset_request.reload.due_date.to_s).to eql(attr[:due_date])
        end
      end

      it 'should redirect to asset requests index if non existent' do
        put '/asset_requests/update', attr.merge(id: 999999, authenticity_token: 'a', due_date: ''), session do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context "DESTROY" do
      it 'should delete the asset_request' do
        id = asset_request.id
        expect do
          delete '/asset_requests/destroy', {authenticity_token: 'a', id: asset_request.id}, session do |res|
            assert_redirected_to_index(res)
          end
        end.to change(AssetRequest, :count).by(-1)
        expect(AssetRequest.find_by_id(id)).to be_nil
      end

      it 'should redirect to asset requests index if category doesnt exist' do
        delete '/asset_requests/destroy', {authenticity_token: 'a', id: '99999'}, session do |res|
          assert_redirected_to_index(res)
        end
      end
    end
  end

  def assert_redirected_to_index(response)
    expect(response).to be_redirect
    expect(response.location).to match(/\/asset_requests\z/)
  end
end
