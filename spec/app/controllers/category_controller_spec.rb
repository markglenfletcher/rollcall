require 'spec_helper'

RSpec.describe "CategoryController" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u| 
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:attr) do
    {
      name: 'Hardware',
      user_id: user.id,
      company_id: company.id
    }
  end
  let(:session) do
    {
      'rack.session' => {
        csrf: 'a',
        user_id: user.id
      }
    }
  end
  let(:category) { Category.create(attr) }

  before { login_user user }

  context 'INDEX' do
    it 'responds success' do
      get '/categories' do |res|
        expect(res).to be_ok
      end
    end
  end

  context 'SHOW' do
    it 'responds success' do
      get "/categories/show/#{category.id}" do |res|
        expect(res).to be_ok
      end
    end

    it 'redirect to categories index if non existent' do
      get "/categories/show/999999" do |res|
        assert_redirected_to_index(res)
      end
    end
  end

  context 'NEW' do
    it 'should respond success' do
      get "/categories/new" do |res|
        expect(res).to be_ok
      end
    end
  end

  context 'CREATE' do
    it 'should create a new category with valid params' do
      expect do
        post '/categories/create', attr.merge(authenticity_token: 'a'), session do |res|
          assert_redirected_to_index(res)
        end
      end.to change(Category, :count).by(1)
    end

    it 'should not create a new category with invalid params' do
      expect do
        post '/categories/create', attr.merge(authenticity_token: 'a', name: ''), session do |res|
          expect(res).to be_ok
        end
      end.to change(Category, :count).by(0)
    end
  end

  context 'EDIT' do
    it 'should respond success' do
      get "/categories/edit/#{category.id}" do |res|
        expect(res).to be_ok
      end
    end

    it 'should redirect to categories index if non existent' do
      get "/categories/edit/999999" do |res|
        assert_redirected_to_index(res)
      end
    end
  end

  context 'UPDATE' do
    let(:new_name) { 'New Category' }

    it 'should change the resource with valid params' do
        put '/categories/update', attr.merge(id: category.id, authenticity_token: 'a', name: new_name), session do |res|
          assert_redirected_to_index(res)
          expect(category.reload.name).to eq(new_name)
        end
    end

    it 'should not change the resource with invalid params' do
      expect do
        put '/categories/update', attr.merge(id: category.id, authenticity_token: 'a', name: ''), session do |res|
          expect(res).to be_ok
        end
      end.to_not change(category.reload, :name)
    end

    it 'should redirect to categories index if non existent' do
      put '/categories/update', attr.merge(id: 999999, authenticity_token: 'a', name: new_name), session do |res|
        assert_redirected_to_index(res)
      end        
    end
  end

  context "DESTROY" do
    it 'should delete the category' do
      id = category.id
      expect do
        delete '/categories/destroy', {authenticity_token: 'a', id: category.id}, session do |res|
          assert_redirected_to_index(res)
        end
      end.to change(Category, :count).by(-1)
      expect(Category.find_by_id(id)).to be_nil
    end

    it 'should redirect to categories index if category doesnt exist' do
      delete '/categories/destroy', {authenticity_token: 'a', id: '99999'}, session do |res|
        assert_redirected_to_index(res)
      end
    end
  end

  context "CHART" do
    let(:asset) do 
      company.assets.create(
        name: 'Hardware Asset',
        creator: user,
        categories: [category],
        assignment_status: Status::Assignment::Unassigned.status_name,
        health_status: Status::Health::Healthy.status_name,
        warranty_status: Status::Warranty::NoWarranty.status_name
      )
    end

    before { asset.touch }

    it 'contains the correct json' do
      expected = "[[\"#{category.name}\",1]]"
      get '/categories/chart' do |res|
        expect(res).to be_ok
        expect(res.body).to eq(expected)
      end
    end
  end

  def assert_redirected_to_index(response)
    expect(response).to be_redirect
    expect(response.location).to match(/\/categories\z/)
  end
end
