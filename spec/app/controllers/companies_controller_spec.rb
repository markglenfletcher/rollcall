require 'spec_helper'

RSpec.describe "CompaniesController" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user_with_company) do
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:user_no_company) do 
    User.create(email: 'c@d.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'c@d.com', image_url: 'http://img.com/i.png')
    end
  end

  context 'for a user with no company' do
    let(:session) do
     {
        'rack.session' => {
          csrf: 'a',
          user_id: user_no_company.id
        }
      }
    end
    let(:attr) do
      {
        name: 'Acme',
        owner_id: user_no_company.id
      }
    end
    before { login_user user_no_company }

    context 'OVERVIEW' do
      it 'redirects to companies new' do
        get '/companies/overview' do |res|
          assert_redirects_to_companies_new(res)
        end
      end
    end

    context 'NEW' do
      it 'responds success' do
        get '/companies/new' do |res|
          expect(res).to be_ok
        end
      end
    end

    context 'CREATE' do
      it 'creates a new company with valid params' do
        expect do
          post '/companies/create', attr.merge(authenticity_token: 'a'), session do |res|
            assert_redirects_to_companies_overview(res)
          end
        end.to change(Company, :count).by(1)
      end

      it 'doesnt create a new company with invalid params' do
        expect do
          post '/companies/create', {authenticity_token: 'a', name: ''}, session do |res|
            expect(res).to be_ok
          end
        end.to change(Company, :count).by(0)
      end
    end

    context 'EDIT' do
      it 'redirects to companies new' do
        get '/companies/edit' do |res|
          assert_redirects_to_companies_new(res)
        end
      end
    end

    context 'UPDATE' do
      it 'redirect to companies new' do
        put '/companies/update', attr.merge(authenticity_token: 'a'), session do |res|
          assert_redirects_to_companies_new(res)
        end
      end
    end

    context 'SETTINGS' do
      it 'should redirect to companies new' do
        get '/companies/settings' do |res|
          assert_redirects_to_companies_new(res)
        end
      end
    end
  end

  context 'for a user with a company' do
    before { login_user user_with_company }

    let(:session) do
      {
        'rack.session' => {
          csrf: 'a',
          user_id: user_with_company.id
        }
      }
    end
    let(:attr) do
      {
        name: 'Acme',
        owner_id: user_with_company.id
      }
    end

    context 'OVERVIEW' do
      it 'responds success' do
        get '/companies/overview' do |res|
          expect(res).to be_ok
        end
      end
    end

    context 'NEW' do
      it 'redirects to the company overview' do
        get '/companies/new' do |res|
          assert_redirects_to_companies_overview(res)
        end
      end
    end

    context 'CREATE' do
      it 'doesnt create company and redirects to the company overview' do
        expect do
          post '/companies/create', attr.merge(authenticity_token: 'a'), session do |res|
            assert_redirects_to_companies_overview(res)
          end
        end.to change(Company, :count).by(0)
      end
    end

    context 'EDIT' do
      it 'responds success' do
        get "/companies/edit" do |res|
          expect(last_response).to be_ok
        end
      end
    end

    context 'UPDATE' do
      it 'updates the company with valid params' do
        new_name = 'New Name'
        put '/companies/update', attr.merge(authenticity_token: 'a', name: new_name), session do |res|
          assert_redirects_to_companies_overview(res)
        end
        expect(company.reload.name).to eq(new_name)
      end

      it 'doesnt update the company with invalid params' do
        old_name = company.name
        put '/companies/update', attr.merge(authenticity_token: 'a', name: ''), session do |res|
          expect(res).to be_ok
        end
        expect(company.reload.name).to eq(old_name)
      end
    end

    context 'SETTINGS' do
      it 'responds success' do
        get '/companies/settings' do |res|
          expect(res).to be_ok
        end
      end
    end
  end

  context 'when not logged in' do
    context 'NEW' do
      it 'should redirect to root path' do
        get '/companies/new' do |res|
          expect(res).to be_redirect
          expect(res.location).to_not include('/companies/new')
        end
      end
    end

    context 'SETTINGS' do
      it 'should redirect to root path' do
        get '/companies/settings' do |res|
          expect(res).to be_redirect
          expect(res.location).to_not include('/companies/settings')
        end
      end
    end
  end
end
