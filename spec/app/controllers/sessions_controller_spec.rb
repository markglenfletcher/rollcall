require 'spec_helper'

RSpec.describe "/sessions", type: :controller do
  context 'NEW' do
    it 'should respond success' do
      get '/sessions/new' do |res|
        expect(res).to be_ok
      end
    end
  end

  context 'AUTH' do
    before do
      OmniAuth.config.test_mode = true
      OmniAuth.config.add_mock(:google_oauth2, {
        'provider' => 'provider',
        'uid' => 'uid',
        'info' => {
          'email' => 'a@b.com',
          'first_name' => 'yuri',
          'last_name' => 'gellar',
          'image' => 'http://image.com/me.jpg'
        }
      })
    end

    it 'should create a new user' do
      expect do
        get '/auth/google_oauth2/callback'
      end.to change(User,:count).by(1)
    end
  end

  context 'DESTROY' do
    let(:user) do
      User.create do |u|
        u.identities.build(provider: 'provider', uid: 'uid',
          first_name: 'first', last_name: 'last',
          email: 'a@b.com', image_url: 'http://img.com/i.png')
      end
    end

    before { login_user user }

    it 'should log out the user' do
      delete '/sessions/destroy', {authenticity_token: 'a'}, 'rack.session' => {user_id: '123', csrf: 'a'}
      get '/assets/new' do |res|
        expect(res).to be_redirect
      end
    end
  end
end
  
