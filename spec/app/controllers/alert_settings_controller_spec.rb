require 'spec_helper'

RSpec.describe "alert_settings" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    company.users.create(email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:no_company_user) do
    User.create(email: 'a@c.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'new', last_name: 'user',
        email: 'a@c.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:alert_user1) { company.users.create(email: 'alert.user1@rollcall.com') }
  let(:alert_user2) { company.users.create(email: 'alert.user2@rollcall.com') } 

  let(:attr) do
    {
      user_ids: [
        alert_user1.id,
        alert_user2.id
      ]
    }
  end

  context 'with a company that exists' do
    before do
      login_user user
      @session = {
        'rack.session' => {
          csrf: 'a',
          user_id: user.id
        }
      }
    end

    context 'EDIT' do
      it 'should respond success' do
        get '/companies/settings/alert_settings/edit' do |res|
          expect(res).to be_ok
          expect(res.body).to include('Alert Settings')
        end
      end
    end

    context 'UPDATE' do 
      it 'should edit the alert settings with valid params' do
        expect do
          put '/companies/settings/alert_settings/update', attr.merge(authenticity_token: 'a'), @session do |res|
            expect(res).to be_redirect
            expect(res.location).to include('/companies/settings')
          end
        end.to change(company.alert_settings.reload.users, :count)
      end

      it 'should rerender edit with invalid params' do
        old_users = company.alert_settings.users
        put '/companies/settings/alert_settings/update', attr.merge(authenticity_token: 'a', user_ids: [no_company_user.id]), @session do |res|
          expect(res).to be_ok
        end
        expect(company.alert_settings.reload.users).to eq(old_users)
      end
    end
  end

  # context 'with a company that doesnt exist' do
  #   before do 
  #     login_user no_company_user
  #     @session = {
  #       'rack.session' => {
  #         csrf: 'a',
  #         user_id: no_company_user.id
  #       }
  #     }
  #   end

  #   context 'EDIT' do
  #     it 'should redirect to companies new' do
  #       get '/companies/settings/alert_settings/edit' do |res|
  #         assert_redirects_to_companies_new(res)
  #       end
  #     end
  #   end

  #   context 'UPDATE' do
  #     it 'should redirect to companies new' do
  #       put '/companies/settings/alert_settings/update', attr.merge(authenticity_token: 'a'), @session do |res|
  #         assert_redirects_to_companies_new(res)
  #       end
  #     end
  #   end
  # end
end
