require 'spec_helper'

RSpec.describe "CommentsController" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do 
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:role) { Role.create(company: company, name: 'My Role') }

  let(:attr) do
    {
      text: 'My Comment',
      user_id: user.id,
      commentable_id: role.id,
      commentable_type: role.class.name
    }
  end

  let(:session) do 
    {
      'rack.session' => {
        csrf: 'a',
        user_id: user.id
      }
    }
  end
  let(:comment) { Comment.create(attr) }
  let(:anothers_comment) do
    role.comments.create(
      text: 'Not my comment',
      user: User.create(email: 'pablo@escobar.com')
    )
  end

  before do
    login_user user
    comment.touch
    anothers_comment.touch
  end

  context "CREATE" do
    it 'should create a new comment' do
      expect do
        post '/comments/create', attr.merge(authenticity_token: 'a'), session do |res|
          expect(res).to be_redirect
        end
      end.to change(role.reload.comments, :count).by(1)
    end

    it 'should not create the comment with invalid params' do
      expect do
        post '/comments/create', attr.merge(authenticity_token: 'a', text: ''), session do |res|
          expect(res).to be_redirect
        end
      end.to change(role.reload.comments, :count).by(0)
    end
  end

  context "UPDATE" do
    it 'should update the comment' do
      new_text = 'My New Comment'
      put '/comments/update', attr.merge(authenticity_token: 'a', id: comment.id, text: new_text), session do |res|
        expect(res).to be_redirect
      end
      expect(comment.reload.text).to eql(new_text)
    end

    it 'should not update the comment with invalid params' do
      put '/comments/update', attr.merge(authenticity_token: 'a', id: comment.id, text: ''), session do |res|
        expect(res).to be_redirect
      end
      expect(comment.reload.text).to eql attr[:text]
    end

    context 'someone else comment' do
      it 'should not be updated' do
        put '/comments/update', { authenticity_token: 'a', id: anothers_comment.id, text: 'My comment now!' }, session do |res|
          expect(res).to be_redirect
        end  
        expect(anothers_comment.reload.text).to eql 'Not my comment'
      end
    end
  end

  context "DESTROY" do
    it 'should delete the comment' do
      expect do
        delete '/comments/destroy', { authenticity_token: 'a', id: comment.id }, session do |res|
          expect(res).to be_redirect
        end
      end.to change(role.reload.comments, :count).by(-1)
    end

    context 'someone else comment' do
      it 'should not be deleted' do
        expect do
          delete '/comments/destroy', { authenticity_token: 'a', id: anothers_comment.id }, session  do |res|
            expect(res).to be_redirect
          end
        end.to change(role.reload.comments, :count).by(0)
      end
    end
  end
end
