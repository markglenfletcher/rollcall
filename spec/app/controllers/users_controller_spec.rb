require 'spec_helper'

RSpec.describe "UsersController" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:role) { company.roles.create(name: 'New Role') }
  let(:loggedin_user) do
    User.create(company: company, email: 'c@d.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'c@d.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:attr) do
    {
      email: 'a@b.com',
      role_id: role.id,
      company_id: company.id
    }
  end
  let(:session) do
   {
      'rack.session' => {
        csrf: 'a',
        user_id: loggedin_user.id
      }
    }
  end
  let(:user) { User.create(attr) }

  context 'when logged in' do
    before { login_user loggedin_user }

    context 'INDEX' do
      it "responds success" do
        get "/users" do |res|
          expect(res).to be_ok
        end
      end
    end

    context "SHOW" do
      it 'responds success' do
        get "/users/show/#{user.id}" do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to users index if non existent' do
        get "/users/show/999999" do |res|
          assert_redirects_to_users_index(res)
        end
      end
    end

    context "NEW" do
      context 'html request' do
        it "responds success" do
          get "/users/new" do |res|
            expect(res).to be_ok
          end
        end
      end

      context 'js request' do
        it "responds success" do
          get "/users/new.js" do |res|
            expect(res).to be_ok
          end
        end
      end
    end

    context "EDIT" do
      it "responds success" do
        get "/users/edit/#{user.id}" do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to users index if non existent' do
        get "/users/edit/999999" do |res|
          assert_redirects_to_users_index(res)
        end 
      end
    end

    context "CREATE" do
      context 'html request' do
        it 'creates a new user with valid params' do
          expect do
            post '/users/create', attr.merge(authenticity_token: 'a'), session do |res|
              assert_redirects_to_users_index(res)
            end
          end.to change(User, :count).by(1)
        end

        it 'doesnt create a new user with invalid params' do
          expect do
            post '/users/create', {authenticity_token: 'a', email: ''},  session do |res|
              expect(res).to be_ok
            end
          end.to change(User, :count).by(0)
        end

        it 'creates a new sign up if invite true' do
          expect do
            post '/users/create', attr.merge(authenticity_token: 'a', invite: '1'), session do |res|
              assert_redirects_to_users_index(res)
            end
          end.to change(SignUp, :count).by(1)
        end

        it 'creates no new sign up if invite false' do
          expect do
            post '/users/create', attr.merge(authenticity_token: 'a'), session do |res|
              assert_redirects_to_users_index(res)
            end
          end.to change(SignUp, :count).by(0)
        end
      end

      context 'js request' do
        it 'creates a new user with valid params' do
          expect do
            post '/users/create.js', attr.merge(authenticity_token: 'a'), session do |res|
              expect(res).to be_ok
            end
          end.to change(User, :count).by(1)
        end

        it 'doesnt create a new user with invalid params' do
          expect do
            post '/users/create.js', attr.merge(authenticity_token: 'a', email: ''), session do |res|
              expect(res).to be_ok
            end
          end.to change(User, :count).by(0)
        end
      end
    end

    context "UPDATE" do
      it 'changes the current user with valid params' do
        new_email = 'new_email@example.com'
        put '/users/update', {authenticity_token: 'a', id: user.id, email: new_email}, session do |res|
          assert_redirects_to_users_index(res)
        end
        expect(user.reload.email).to eql(new_email)
      end

      it 'doesnt change the current user with invalid params' do
        put '/users/update', {authenticity_token: 'a', id: user.id, email: ''}, session do |res|
          expect(res).to be_ok
        end
        expect(user.reload.email).to eql(attr[:email])
      end

      it 'should redirect to users index if user doesnt exist' do
        put '/users/update', {authenticity_token: 'a', id: '99999', email: 'valid_email@rollcall.com'}, session do |res|
          assert_redirects_to_users_index(res)
        end
      end
    end

    context "DESTROY" do
      it 'should delete the user' do
        id = user.id
        delete '/users/destroy', {authenticity_token: 'a', id: id}, session do |res|
          assert_redirects_to_users_index(res)
        end
        expect(User.find_by_id(id)).to be_nil
      end

      it 'should redirect to users index if user doesnt exist' do
        delete '/users/destroy', {authenticity_token: 'a', id: '99999'}, session do |res|
          assert_redirects_to_users_index(res)
        end
      end
    end

    context "ME" do
      it 'should respond success' do
        get '/me' do |res|
          expect(res).to be_ok
        end
      end
    end
  end

  context 'when not logged in' do
    it 'should redirect to root path' do
      get '/users/new' do |res|
        expect(res).to be_redirect
        expect(res.location).to include('/sessions/new')
      end
    end
  end
end

def assert_redirects_to_users_index(response)
  expect(response).to be_redirect
  expect(response.location).to include('/users')
end
