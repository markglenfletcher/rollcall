require 'spec_helper'

RSpec.describe "HomeController" do
  context 'INDEX' do
    it "responds success" do
      get "/" do |res|
        expect(res).to be_ok
      end
    end
  end

  context 'ABOUT' do
    it 'responds success' do
      get "/about" do |res|
        expect(last_response).to be_ok
      end
    end
  end
end
