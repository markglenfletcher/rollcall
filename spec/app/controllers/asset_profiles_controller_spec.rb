require 'spec_helper'

RSpec.describe "AssetProfilesController" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email:'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:session) do
    {
      'rack.session' => {
        csrf: 'a',
        user_id: user.id
      }
    }
  end
  let(:attr) do
    {
      name: 'Macbook',
      company_id: company.id
    }
  end
  let(:asset_profile) { AssetProfile.create(attr) }

  context 'when logged in' do
    before do
      login_user user
    end

    context 'INDEX' do
      it "responds success" do
        get("/asset_profiles", session) do |res| 
          expect(res).to be_ok
        end
      end
    end

    context "SHOW" do
      it 'responds success' do
        get("/asset_profiles/show/#{asset_profile.id}", session) do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to asset_profile index if non existent' do
        get("/asset_profiles/show/999999", session) do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context "NEW" do
      context' html request' do
        it "responds success" do
          get "/asset_profiles/new", session do |res|
            expect(res).to be_ok
          end
        end
      end

      context 'js request' do
        it 'responds success' do
          get "/asset_profiles/new.js", session do |res|
            expect(res).to be_ok
          end
        end
      end
    end

    context "EDIT" do
      it "responds success" do
        get "/asset_profiles/edit/#{asset_profile.id}", session do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to asset_profile index if non existent' do
        get "/asset_profiles/edit/999999", session do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context "CREATE" do
      it 'creates a new asset_profile with valid params' do
        expect do
          post '/asset_profiles/create', attr.merge(authenticity_token: 'a'), session do |res|
            assert_redirected_to_index(res)
          end
        end.to change(AssetProfile, :count).by(1)
      end

      it 'doesnt create a new asset_profile with invalid params' do
        expect do
          post '/asset_profiles/create', {authenticity_token: 'a', name: ''}, session do |res|
            expect(res).to be_ok
          end
        end.to change(AssetProfile, :count).by(0)
      end

      context 'js request' do
        it 'creates a new asset_profile with valid params' do
          expect do
            post '/asset_profiles/create.js', attr.merge(authenticity_token: 'a'), session
          end.to change(AssetProfile, :count).by(1)
        end

        it 'doesnt create a new asset_profile with invalid params' do
          expect do
            post '/asset_profiles/create.js', {authenticity_token: 'a', name: ''}, session
          end.to change(AssetProfile, :count).by(0)
        end
      end
    end

    context "UPDATE" do
      let(:new_name) { 'New Name' }
      let(:invalid_name) { '' }

      it 'changes the current asset_profile with valid params' do
        put '/asset_profiles/update', {authenticity_token: 'a', id: asset_profile.id, name: new_name}, session do |res|
          assert_redirected_to_index(res)
          expect(asset_profile.reload.name).to eql(new_name)
        end
      end

      it 'doesnt change the current asset_profile with invalid params' do
        original_name = asset_profile.name
        put '/asset_profiles/update', {authenticity_token: 'a', id: asset_profile.id, name: invalid_name}, session do |res|
          expect(res).to be_ok
          expect(asset_profile.reload.name).to eql(original_name)
        end
      end

      it 'should redirect to asset_profile index if asset_profile doesnt exist' do
        put '/asset_profiles/update', {authenticity_token: 'a', id: '99999', name: new_name}, session do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context "DESTROY" do
      it 'should delete the asset_profile' do
        id = asset_profile.id
        expect {
          delete '/asset_profiles/destroy', {authenticity_token: 'a', id: asset_profile.id}, session do |res|
            assert_redirected_to_index(res)
          end
        }.to change(AssetProfile, :count).by(-1)
        expect(AssetProfile.find_by_id(id)).to be_nil
      end

      it 'should redirect to asset_profile index if asset_profile doesnt exist' do
        delete '/asset_profiles/destroy', {authenticity_token: 'a', id: '99999'}, session do |res|
          assert_redirected_to_index(res)
        end
      end
    end

    context 'CLONE' do
      it 'should respond success' do
        get "/asset_profiles/clone/#{asset_profile.id}" do |res|
          expect(res).to be_ok
        end
      end

      it 'should redirect to asset_profile index if non existent' do
        get "/asset_profiles/clone/999999" do |res|
          assert_redirected_to_index(res)
        end
      end
    end
  end

  context 'when not logged in' do
    it 'should redirect to root path' do
      get '/asset_profiles/new' do |res|
        expect(res).to be_redirect
      end
    end
  end

  def assert_redirected_to_index(response)
    expect(response).to be_redirect
    expect(response.location).to match(/\/asset_profiles\z/)
  end
end
