require 'spec_helper'

RSpec.describe "RolesController" do
  context 'when logged in' do
    let(:company) { Company.create(name: 'Design Agency') }
    let(:user) do
      User.create(company: company, email: 'a@b.com') do |u|
        u.identities.build(provider: 'provider', uid: 'uid',
          first_name: 'first', last_name: 'last',
          email: 'a@b.com', image_url: 'http://img.com/i.png')
      end
    end
    let(:staff_profile) { StaffProfile.create(name: 'Engineering Staff Profile') }
    let(:attr) do
      {
        name: 'Engineer',
        staff_profile_id: staff_profile.id,
        company_id: company.id
      }
    end
    let(:session) do 
      {
        'rack.session' => {
          csrf: 'a',
          user_id: user.id
        }
      }
    end
    let(:role) { Role.create(attr) }
    before { login_user user }

    context 'INDEX' do
      it "responds success" do
        get "/roles" do |res|
          expect(res).to be_ok
        end 
      end
    end

    context "SHOW" do
      it 'responds success' do
        get "/roles/show/#{role.id}" do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to roles index if non existent' do
        get "/roles/show/999999" do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/roles")
        end
      end
    end

    context "NEW" do
      it "responds success" do
        get "/roles/new" do |res|
          expect(res).to be_ok
        end
      end
    end

    context "EDIT" do
      it "responds success" do
        get "/roles/edit/#{role.id}" do |res|
          expect(res).to be_ok
        end
      end

      it 'redirects to roles index if non existent' do
        get "/roles/edit/999999" do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/roles")
        end
      end
    end

    context "CREATE" do
      it 'creates a new role with valid params' do
        expect do
          post '/roles/create', attr.merge(authenticity_token: 'a'), session do |res|
            expect(res).to be_redirect
            expect(res.location).to include("/roles")
          end
        end.to change(Role, :count).by(1)
      end

      it 'doesnt create a new role with invalid params' do
        expect do
          post '/roles/create', {authenticity_token: 'a', name: ''}, session do |res|
            expect(res).to be_ok
          end
        end.to change(Role, :count).by(0)
      end
    end

    context "UPDATE" do
      it 'changes the current role with valid params' do
        new_name = 'New Name'
        put '/roles/update', {authenticity_token: 'a', id: role.id, name: new_name}, session do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/roles")
        end
        expect(role.reload.name).to eql(new_name)
      end

      it 'doesnt change the current role with invalid params' do
        put '/roles/update', {authenticity_token: 'a', id: role.id, name: ''}, session do |res|
          expect(res).to be_ok
        end
        expect(role.reload.name).to eql(attr[:name])
      end

      it 'should redirect to roles index if role doesnt exist' do
        put '/roles/update', {authenticity_token: 'a', id: '99999', name: 'New name'}, session do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/roles")
        end
      end
    end

    context "DESTROY" do
      it 'should delete the role' do
        id = role.id
        delete '/roles/destroy', {authenticity_token: 'a', id: role.id}, session do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/roles")
        end
        expect(Role.find_by_id(id)).to be_nil
      end

      it 'should redirect to roles index if role doesnt exist' do
        delete '/roles/destroy', {authenticity_token: 'a', id: '99999'}, session do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/roles")
        end
      end
    end

    context 'CHARTS' do 
      before do
        user.role = role
        user.save
      end

      it 'contains the correct json' do
        expected = '[["Engineer",1]]'
        get '/roles/chart' do |res|
          expect(res).to be_ok
          expect(res.body).to eq(expected)
        end
      end
    end
  end

  context 'when not logged in' do
    it 'should redirect to root path' do
      get '/roles/new' do |res|
        expect(res).to be_redirect
        expect(res.location).to include('/sessions/new')
      end
    end
  end
end
