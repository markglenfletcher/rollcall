require 'spec_helper'

RSpec.describe "/sign_ups" do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:new_user) do
    User.create(company: company, email: 'new_user@rollcall.com')
  end
  let(:attr) do
    {
      user_id: new_user.id
    }
  end
  let(:session) do
    {
      'rack.session' => {
        csrf: 'a',
        user_id: user.id
      }
    }
  end

  context 'CREATE' do
    before { login_user user }

    it 'should create a signup' do
      expect do
       	post '/sign_ups/create', attr.merge(authenticity_token: 'a'), session do |res|
          expect(res).to be_redirect
          expect(res.location).to include("/users/show/#{new_user.id}")
        end
      end.to change(SignUp,:count).by(1)
    end

    it 'should redirect to users index if user does not exist' do
    	post '/sign_ups/create', attr.merge(authenticity_token: 'a', user_id: 99999), session do |res|
        expect(res).to be_redirect
        expect(res.location).to include('/users')
      end
    end
  end

	context 'ACCEPT_INVITATION' do
    let(:signup) do
      company.sign_ups.create(inviter: user, user: new_user)
		end

		it 'responds success' do
			get "/sign_ups/accept_invitation?code=#{signup.signup_code}" do |res|
        expect(last_response).to be_ok
      end
		end

		it 'should redirect to root path if code is not valid' do
			get "/sign_ups/accept_invitation?code=gumpf" do |res|
        expect(last_response).to be_redirect
        expect(last_response.location).to eql('http://example.org/') 
      end
		end

		it 'should delete the signup when code has been used' do
      signup.touch
			expect do
				get "/sign_ups/accept_invitation?code=#{signup.signup_code}" do |res|
          expect(res).to be_ok
        end
			end.to change(SignUp, :count).by(-1)
		end
	end
end
