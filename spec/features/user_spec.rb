require 'spec_helper'

RSpec.describe 'UserSpec' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:non_tech_staff_profile) { company.staff_profiles.create(name: 'Non technical Staff Profile') }
  let(:support_role) { company.roles.create(name:'Support', staff_profile: non_tech_staff_profile) }
  let(:user) do
    company.users.create(email: 'a@b.com', role: support_role) do |u|
      u.identities.build(provider: 'provider', uid: 'uid1',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:other_user) do
    company.users.create(email: 'c@d.com',role: support_role) do |u|
      u.identities.build(provider: 'provider', uid: 'uid2',
        first_name: 'first-name', last_name: 'last-name',
        email: 'c@d.com', image_url: 'http://img.com/u.png')
    end
  end
  let(:category) { company.categories.create(name: 'Hardware', user: user) }
  let(:asset) do
    company.assets.create(
      name: 'Macbook',
      creator: user,
      user: user,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      categories: [
        category
      ]
    )
  end

  before do 
    capybara_login_user user
    other_user.touch
  end

  feature 'User views own profile' do
    scenario 'from home page' do
      visit '/'

      click_link user.email

      expect(page.has_content?(user.first_name)).to eql true
      expect(page.has_content?(user.last_name)).to eql true
      expect(page.has_content?(user.email)).to eql true
    end
  end

  feature 'Users views user list' do
    before do
      12.times do |i|
        company.users.create(email: "user-#{i}@rollcall.com")
      end
    end

    scenario 'from the index page' do
      visit '/users'
      within '#user-table' do
        # assert pagination is working
        expect(page).to have_selector('tr.user.user-row', count: 10)
      end
    end
  end

  feature 'User views another profile' do
    scenario 'from users index' do
      visit '/users'

      expect(page.has_content?('You are currently viewing all Users')).to eql true

      within('#users') do
        within("#user-#{other_user.id}") do
          click_link 'Show'
        end
      end

      expect(page.has_content?(other_user.full_name)).to eql true
    end
  end

  feature 'Should list their assets only' do
    scenario 'in the user profile' do
      visit "/users/show/#{user.id}"

      within('#assets') do
        user.assets.each do |asset|
          expect(page.has_content?(asset.name)).to eql true
        end
        expect(page).to have_selector('.asset', count: user.assets.count)
      end
    end
  end
end
