require 'spec_helper'

RSpec.describe 'CreateAssetRequest' do
  before do
    @company = Company.create(name: 'Design Agency')
    @author = User.create(email: 'author@rollcall.com', company: @company) { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'author@rollcall.com', image_url: 'http://img.com/i.png') } 
    @assignee = User.create(email: 'assignee@rollcall.com', company: @company)
    @due_date = Date.tomorrow
    @asset_profile1 = AssetProfile.create(name: 'Macbook', company: @company)
    @asset_profile2 = AssetProfile.create(name: 'Secondary Display', company: @company)

    capybara_login_user @author
  end

  feature 'When asset profiles and users exist' do    
    scenario 'author can create an asset request' do
      visit '/asset_requests/new'

      within '#request-form' do
        select @assignee.email, from: 'request_user_id'
        select @asset_profile1.name, from: 'request_asset_ids'
        select @asset_profile2.name, from: 'request_asset_ids'
        fill_in :request_due_date, with: @due_date.to_s
        click_button 'Submit Request'
      end

      expect(page).to have_content("Asset Request for #{@assignee.email} has been created.")
    end

    scenario 'it should create asset request items' do
      visit '/asset_requests/new'

      within '#request-form' do
        select @assignee.email, from: 'request_user_id'
        select @asset_profile1.name, from: 'request_asset_ids'
        select @asset_profile2.name, from: 'request_asset_ids'
        fill_in :request_due_date, with: @due_date.to_s
        expect do
          click_button 'Submit Request'
        end.to change(AssetRequestItem, :count).by 2
      end
    end
  end

  feature 'when an asset request exists' do
    before do
      @asset_request = AssetRequest.create(company: @company, requester: @author, manager: @author, user: @assignee, due_date: @due_date, asset_profiles: [@asset_profile1])
      @new_assignee = User.create(email: 'new_assignee@rollcall.com', company: @company)
    end

    scenario 'we should be able to view it' do
      visit '/asset_requests'

      within '#asset-requests' do
        within "#asset-request-#{@asset_request.id}" do
          click_link 'Show'
        end
      end

      expect(page).to have_content('Assignee')
      expect(page).to have_content(@asset_request.user.full_name)
      expect(page).to have_content('Manager')
      expect(page).to have_content('Requester')
      expect(page).to have_content(@asset_request.requester.full_name)
      expect(page).to have_content('Status')
      expect(page).to have_content('Incomplete')

      @asset_request.asset_request_items.each do |ari|
        expect(page).to have_content(ari.asset_profile.name)
      end
    end

    scenario 'we should be able to edit it' do
      visit "/asset_requests/edit/#{@asset_request.id}"

      within '#request-form' do
        select @new_assignee.email, from: 'request_user_id'
        click_button 'Edit Request'
      end

      expect(page).to have_content("Asset Request for #{@new_assignee.email} has been edited.")
    end

    scenario 'it is listed in the asset requests index' do
      visit "/asset_requests"

      within '#asset-requests' do
        expect(page).to have_selector('.asset-request', count: 1)
      end
    end
  end

  feature 'when no asset requests exist' do
    scenario 'it should tell the user' do
      visit '/asset_requests'

      expect(page).to have_content('There are no Requests available to show, click here to create a new one')
      expect(page).to have_link('New Asset Request')
    end
  end
end
