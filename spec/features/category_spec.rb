require 'spec_helper'

RSpec.describe 'CategorySpec' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    company.users.create(email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:attr) do
    {
      name: 'Hardware',
      user_id: user.id,
      company_id: company.id
    }
  end

  context 'when logged in' do
    before do
      capybara_login_user user
    end

    feature 'User creates a new category' do
      scenario 'using valid params' do 
        visit '/categories/new'

        expect(page.has_content?('Creating a new Category')).to eql(true)

        within('#category_form') do
          fill_in 'category_name', with: 'Hardware'
          click_button 'Create Category'
        end

        expect(page.has_content?('Category: Hardware has been created.')).to eql true
      end
    end

    feature 'Users views category list' do
      before do
        12.times do |i|
          company.categories.create(attr)
        end
      end

      scenario 'from the index page' do
        visit '/categories'
        within '#category-table' do
          # assert pagination is working
          expect(page).to have_selector('tr.category', count: 10)
        end
      end
    end

    feature 'User edits a category' do
      scenario 'when categories exist' do
        category = Category.create(attr)
        
        visit '/categories'
        expect(page.has_content?('Viewing all Categories')).to eql true

        within('#categories') do
          expect(page).to have_selector('.category', count: Category.count)
          within("#category-#{category.id}") do
            expect(page.has_content?(category.name)).to eql true
            expect(page).to have_link('Show')
            click_link 'Show'
          end
        end

        expect(page).to have_selector('h2', category.name)
        click_link 'Edit'

        expect(page).to have_selector('h1', "Editing #{category.name}")

        within('#category_form') do
          fill_in 'category_name', with: 'Software'
          click_button 'Save'
        end

        expect(page.has_content?("Category: #{category.reload.name} has been edited.")).to eql true
      end

      scenario 'when categories dont exist' do
        visit '/categories'
        expect(page.has_content?("There are no Categories available to show, click here to create a new one")).to eql true
      end
    end

    feature 'User deletes a category' do
      scenario 'when categories exist' do
        category = Category.create(attr)
        visit "/categories/show/#{category.id}"
        click_button 'Delete'
        expect(page.has_content?("Category has been deleted.")).to eql true
      end
    end
  end

  context 'when not logged in' do
    feature ' ' do
      scenario 'user redirected to root path' do
        visit '/categories'
        expect(page.has_content?('You must log in to perform that action')).to eql(true)
      end
    end
  end
end
