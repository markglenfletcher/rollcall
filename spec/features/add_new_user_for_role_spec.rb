require 'spec_helper'

RSpec.describe 'AddNewUserForRoleSpec' do
  before do
    @company = Company.create(name: 'Design Agency')
    @role = Role.create(company: @company, name: 'New Role')
    @user = User.create(company: @company, email: 'admin@rollcall.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'admin@rollcall.com', image_url: 'http://img.com/i.png') }
    @attr = {
      email: 'newbie@rollcall.com'
    }
  end

  context 'when logged in' do
    before { capybara_login_user @user }

    feature 'User creates a user for a role' do
      scenario 'with valid params' do
        visit "/roles/show/#{@role.id}"

        within('.role') do
          click_link 'Users'
          click_link 'New User'
        end

        within '#user-form' do
          fill_in 'user_email', with: @attr[:email]
          click_button 'Create User'
        end

        expect(page).to have_content("User: #{@attr[:email]} has been created.")
      end
    end
  end
end