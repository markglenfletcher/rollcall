require 'spec_helper'

RSpec.describe 'AssetProfilesSpec' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:role) { company.roles.create(name: 'New Role') }
  let(:user) do
    company.users.create(email: 'admin@rollcall.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'admin@rollcall.com', image_url: 'http://img.com/i.png')
    end
  end
  before { capybara_login_user user }

  context 'when logged in' do
    feature 'User views list of asset profiles' do
      before do
        12.times do |i|
          company.asset_profiles.create(name: "Asset Profile #{i}")
        end
      end

      scenario 'from the index page' do
        visit "/asset_profiles"

        within('#asset-profiles') do
          expect(page).to(have_selector('.asset-profile', count: 12))
        end
      end
    end

    feature 'User views an individual asset profile' do
      let(:asset_profile) { company.asset_profiles.create(name: 'Asset Profile 1') }
      let(:category) { company.categories.create(name: 'Category1', user: user) }
      let(:asset) do
        company.assets.create(
            name: 'Macbook',
            creator: user,
            user: user,
            assignment_status: Status::Assignment::Assigned.status_name,
            health_status: Status::Health::Healthy.status_name,
            warranty_status: Status::Warranty::UnderWarranty.status_name,
            warranty_expiration_date: Date.tomorrow.to_date.to_s,
            categories: [
              category
            ],
            asset_profile: asset_profile
          )
      end
      before do
        asset
      end

      scenario 'from the index page' do
        visit '/asset_profiles'

        within '#asset-profiles' do
          within "#asset-profile-#{asset_profile.id}" do
            click_link 'Show'
          end
        end

        expect(page).to have_content(asset_profile.name)

        within '#assets' do
          expect(page).to have_selector('article.asset', count: 1)
        end
      end
    end
  end
end
