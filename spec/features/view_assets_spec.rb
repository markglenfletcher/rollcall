require 'spec_helper'

RSpec.describe 'ViewAssetsSpec' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:category) { company.categories.create(name: 'Hardware', user: user) }
  let(:attr) do
    {
      company: company,
      name: 'Macbook',
      creator_id: user.id,
      user_id: user.id,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      category_ids: [
        category.id
      ]
    }
  end
  before { capybara_login_user user }

  feature 'User creates a new asset' do
    before { category.touch }

    scenario 'using valid params' do
      visit '/assets/new'

      expect(page.has_content?('Create new Asset')).to eql true
      
      within('#asset_form') do
        fill_in 'asset_name', with: 'Chromecast'
        select 'Hardware', from: 'asset_categories'
        select 'Unassigned', from: 'asset_assignment_status'
        select 'Healthy', from: 'asset_health_status'
        select 'Under Warranty', from: 'asset_warranty_status'
        fill_in 'asset_warranty_type', with: 'AppleCare 3 Year'
        fill_in 'asset_warranty_expiration_date', with: Date.today.to_date.to_s
        fill_in 'asset_purchase_date', with: '2011/01/01'
        fill_in 'asset_inspection_date', with: Date.tomorrow.to_date.to_s
        fill_in 'asset_serial_number', with: '12345678'
        fill_in 'asset_description', with: 'Gumpf'
        click_button 'Create Asset'
      end
      expect(page.has_content?('Asset: Chromecast has been created.')).to eql true
    end
  end

  feature 'User navigates to edit individual asset' do
    let(:asset) { company.assets.create(attr) }

    scenario 'when assets exist' do
      asset.touch

      visit '/assets'

      expect(page.has_content?('Viewing all Assets')).to eql true

      within('#assets') do
        expect(page).to have_selector('.asset', count: Asset.count)
        within("#asset-#{asset.id}") do
          expect(page.has_content?(asset.name)).to eql true
          expect(page).to have_link('View')
          click_link 'View'
        end
      end

      expect(page).to have_selector('h3', asset.name)
      expect(page).to have_link('Edit This Asset')

      click_link 'Edit This Asset'

      expect(page).to have_selector('h1', "Editing #{asset.name}")

      within('#asset_form') do
        fill_in 'asset_name', with: 'New Name'
        click_button 'Save'
      end

      expect(page.has_content?("Asset: #{asset.reload.name} has been edited.")).to eql true
    end

    scenario 'when assets dont exist' do
      visit '/assets'
      expect(page.has_content?("There are no Assets available to show, click here to create a new one")).to eql true
    end
  end

  feature 'User deletes an asset' do
    let(:asset) { company.assets.create(attr) }
    scenario 'when assets exist' do
      visit "/assets/show/#{asset.id}"
      click_button 'Delete'
      expect(page.has_content?("Asset has been deleted.")).to eql true
    end
  end

  feature 'User creates new asset' do
    scenario 'from a category' do
      visit "/categories/show/#{category.id}"
      click_link 'Create Asset for this Category'
      expect(page.has_content?("Create new Asset")).to eq(true)
      expect(find(:css, 'select#asset_categories').value ).to include(category.id.to_param)
    end
  end

  feature 'User clones an existing asset' do
    let(:original_asset) { company.assets.create(attr) }
    before do
      original_asset.touch
    end

    scenario 'to create a new asset' do
      visit "/assets/show/#{original_asset.id}"
      click_button 'Clone This Asset'
      expect(page.has_content?("Successfully cloned Asset: #{original_asset.name}")).to eq(true)
    end
  end
end
