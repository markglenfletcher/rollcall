require 'spec_helper'

feature 'User can login' do
  scenario 'via oauth' do
    Capybara.default_host = 'http://localhost:9292'

    OmniAuth.config.test_mode = true
    OmniAuth.config.add_mock(:google_oauth2, {
        'provider' => 'provider',
        'uid' => 'uid',
        'info' => {
          'email' => 'a@b.com',
          'first_name' => 'yuri',
          'last_name' => 'gellar',
          'image' => 'http://image.com/me.jpg'
        }
      })

    visit '/sessions/new'

    click_link 'Login with Google'
    
    expect(page.has_content?('a@b.com Logged in successfully!'))
  end
end

feature 'User can logout' do
  before do
    company = Company.create(name: 'Design Agency')
    user = company.users.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
    capybara_login_user user
  end

  scenario 'git out' do
    visit '/'

    click_button 'Log out'

    expect(page).to have_content('logged out')
  end
end
