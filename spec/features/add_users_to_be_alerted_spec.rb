require 'spec_helper'

RSpec.describe 'AddUsersAlerted' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:alert_user1) { company.users.create(email: 'alert.user1@rollcall.com') }
  let(:alert_user2) { company.users.create(email: 'alert.user2@rollcall.com') } 
  before do
    company.touch
    alert_user1.touch
    alert_user2.touch
  end

  feature 'User adds alert users' do
    context 'with an existing company' do
      before { capybara_login_user user }

      it 'should be successful' do
        visit '/'

        click_link 'Settings'

        click_link 'Alert Settings'

        expect(page.has_content?('Alert Settings')).to eq(true)

        within('#alert-settings-form') do
          select alert_user1.email, from: 'alert_settings_users'
          select alert_user2.email, from: 'alert_settings_users'
        end
        click_button 'Save'

        expect(page.has_content?('Alert settings have been saved.')).to eq(true)
      end
    end
  end
end
