require 'spec_helper'

RSpec.describe 'CompanyScopeSpec' do
  before do
    @company1 = Company.create(name: 'Design Agency')
    @company2 = Company.create(name: 'Marketing Agency')

    @user1 = @company1.users.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
    @user2 = @company2.users.create(email: 'c@d.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'c@d.com', image_url: 'http://img.com/i.png') }

    @category1 = @company1.categories.create(name: 'Hardware', user: @user1)
    @category2 = @company2.categories.create(name: 'Hardware', user: @user2)

    @role1 = @company1.roles.create(name: 'IT Staff')
    @role2 = @company2.roles.create(name: 'IT Staff')

    @asset1 = @company1.assets.create(name: 'Macbook',
      creator_id: @user1.id,
      user_id: @user1.id,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      category_ids: [
        @category1.id
      ])
    @asset2 = @company2.assets.create(name: 'Macbook',
      creator_id: @user2.id,
      user_id: @user2.id,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      category_ids: [
        @category2.id
      ])
  end

  feature 'Index routes are scoped to company' do
    before do
      capybara_login_user @user1
    end

    scenario 'for assets' do
      visit '/assets'

      within '#asset-table' do
        expect(page).to have_selector('.asset', count: 1)
      end
    end

    scenario 'for categories' do
      visit '/categories'

      within '#category-table' do
        expect(page).to have_selector('.category', count: 1)
      end
    end

    scenario 'for roles' do
      visit '/roles'

      within '#role-table' do
        expect(page).to have_selector('.role', count: 1)
      end
    end

    scenario 'for users' do
      visit '/users'

      within '#user-table' do
        expect(page).to have_selector('.user', count: 1)
      end
    end
  end

  feature 'Show and edit routes are scoped to company' do
    before do
      capybara_login_user @user1
    end

    scenario 'for assets' do
      visit "/assets/show/#{@asset2.id}"
      expect(page).to have_content('That asset does not exist!')
      visit "/assets/edit/#{@asset2.id}"
      expect(page).to have_content('That asset does not exist!')
    end

    scenario 'for categories' do
      visit "/categories/show/#{@category2.id}"
      expect(page).to have_content('That category does not exist!')
      visit "/categories/edit/#{@category2.id}"
      expect(page).to have_content('That category does not exist!')
    end

    scenario 'for roles' do
      visit "/roles/show/#{@role2.id}"
      expect(page).to have_content('That role does not exist!')
      visit "/roles/edit/#{@role2.id}"
      expect(page).to have_content('That role does not exist!')
    end

    scenario 'for users' do
      visit "/users/show/#{@user2.id}"
      expect(page).to have_content('That user does not exist!')
      visit "/users/edit/#{@user2.id}"
      expect(page).to have_content('That user does not exist!')
    end
  end
end
