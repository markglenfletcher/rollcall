require 'spec_helper'

describe 'RoleStaffProfileSpec' do
  before do
    @company = Company.create(name: 'Design Agency')
    user = User.create(company: @company, email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
    capybara_login_user user
  end

  feature 'creating a new role' do
    before do
      @staff_profile = StaffProfile.create(company: @company, name: 'Marketing Staff Profile')
    end

    scenario 'when a staff profile isnt specified a new one is created' do
      StaffProfile.destroy_all

      visit '/roles'

      click_link 'New Role'

      expect(page.has_content?('Create a new Role')).to eql true

      expect {
        within '#role_form' do
          fill_in 'name', with: 'Marketing Executive'
          click_button 'Create Role'
        end
      }.to change(StaffProfile, :count).by(1)
    end

    scenario 'when a staff profile is specified a new one isnt created' do
      visit '/roles/new'

      expect {
        within '#role_form' do
          fill_in 'role_name', with: 'Marketing Executive'
          select 'Marketing Staff Profile', from: 'role_staff_profile'
          click_button 'Create Role'
        end
      }.to change(StaffProfile, :count).by(0)
    end
  end
end