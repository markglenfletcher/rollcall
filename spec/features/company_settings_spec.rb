require 'spec_helper'

RSpec.describe 'CompanySettingsSpec' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:no_company_user) do
    User.create(email: 'a@c.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@c.com', image_url: 'http://img.com/i.png')
    end
  end

  feature 'User views company settings page' do
    context 'with an existing company' do
      before { capybara_login_user user }

      it 'should be successful' do
        visit '/companies/settings'
        expect(page.has_content?('Company Settings')).to eq(true)
      end
    end

    context 'without an existing company' do
      before { capybara_login_user no_company_user }

      it 'should redirect to new company page' do
        visit '/companies/settings'
        expect(page.has_content?('Create new Company')).to eq(true)
      end
    end
  end
end
