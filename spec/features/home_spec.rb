require 'spec_helper'

RSpec.describe 'HomeSpec' do
  feature 'can navigate to the root of app' do
    context 'when logged in with a company' do
      before do
        @company = Company.create(name: 'Design Agency')
        @user = @company.users.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
        capybara_login_user @user
      end

      it 'should show the company overview' do
        visit '/'
        expect(page.has_content?('Company Overview')).to eql(true)
      end

      it 'should have some navigation' do
        visit '/'
        within '#nav' do
          expect(page.has_link?('About')).to eql true
          expect(page.has_link?('Assets')).to eql true
          expect(page.has_link?('Roles')).to eql true
          expect(page.has_link?('Categories')).to eql true
          expect(page.has_link?('Users')).to eql true
          expect(page.has_link?('Requests')).to eql true
          expect(page.has_link?('Settings')).to eql true
        end
      end
    end

    context 'when logged in with no company' do
      before do
        @user = User.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
        capybara_login_user @user
      end

      it 'should show the company creation page' do
        visit '/'
        expect(page).to have_content('Create new Company')
      end
    end

    context 'when not logged in' do
      it 'should show the home page' do
        visit '/'
        expect(page.has_content?('RollCall')).to eql(true)
      end
    end
  end

  feature 'can navigate to the about page' do
    it 'should show the about page' do
      visit '/about'
      expect(page.has_content?('About RollCall')).to eql(true)
    end
  end
end
