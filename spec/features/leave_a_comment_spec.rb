require 'spec_helper'

describe 'LeaveCommentSpec' do
  before do
    @company = Company.create(name: 'Design Agency')
    @user = User.create(company: @company, email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
    @category = Category.create(company: @company, name: 'Hardware', user: @user)
    @asset = @user.assets.create(
      company: @company,
      name: 'Macbook',
      creator_id: @user.id,
      user_id: @user.id,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      category_ids: [
        @category.id
      ]
    )
    capybara_login_user @user
  end

  feature 'User can leave a comment' do
    scenario 'on an asset' do
      visit "/assets/show/#{@asset.id}"

      within('.comments') do
        expect {
          fill_in 'comment_text', with: 'My Comment'
          click_button 'Post Comment'
        }.to change(@asset.reload.comments, :count).by(1)
      end

      expect(page.has_content?('Your comment has been created'))
    end

    scenario 'on a comment' do
      comment = @asset.comments.create(text: 'My Comment', user: @user)

      visit "/assets/show/#{@asset.id}"

      within("#comment-#{comment.id}") do
        within('.comment-form') do
          expect {
            fill_in 'comment_text', with: 'My Comment'
            click_button 'Submit Reply'
          }.to change(comment.reload.comments, :count).by(1)
        end
      end

      expect(page.has_content?('Your comment has been created'))
    end
  end
end
