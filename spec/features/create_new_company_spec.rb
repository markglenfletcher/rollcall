require 'spec_helper'

RSpec.describe 'CreateNewCompany' do
  feature 'User with no company' do
    before do
      user = User.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
      capybara_login_user user
    end

    scenario 'creates a new company' do
      visit '/'

      expect(page).to have_content('Create new Company')

      within '#company-form' do
        fill_in :company_name, with: 'Acme'
        click_button 'Create Company'
      end

      expect(page).to have_content('Company Overview')
      expect(page).to have_content('Acme')
    end
  end

  feature 'User with a company' do
    before do
      @company = Company.create(name: 'Design Agency')
      user_with_company = @company.users.create(email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
      @new_owner = @company.users.create(email: 'new_owner@rollcall.com')
      capybara_login_user user_with_company
    end

    scenario 'wants to edit the owner' do
      visit "/companies/edit"

      expect(page).to have_content('Edit Your Company')

      within '#company-form' do
        fill_in :company_name, with: 'Acme'
        select @new_owner.email, from: :company_owner
        click_button 'Edit Company'
      end

      expect(page).to have_content('Company Overview')
      expect(page).to have_content('Acme')
    end
  end
end
