require 'spec_helper'

RSpec.describe 'InviteUserSpec' do
  before do
    @company = Company.create(name: 'Design Agency')
    user = User.create(company: @company, email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
    capybara_login_user user

    @attr = {
      email: 'new_user@rollcall.com'
    }
  end

  feature 'User create a new user' do
    scenario 'and sends an invitation' do
      visit '/users/new'

      within '#user-form' do
        fill_in 'user_email', with: @attr[:email]
        check 'user_invite'
        click_button 'Create User'
      end

      expect(page).to have_content('invitation')
    end
  end
end
