require 'spec_helper'

RSpec.describe 'CreateAssetFromProfile' do
  before do
    AssetProfile.destroy_all
    @company = Company.create(name: 'Design Agency')
    user = User.create(company: @company, email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') }
    Category.create!(company: @company, name: 'Hardware', user: user)
    capybara_login_user user
  end

  feature 'User can create new asset' do
    scenario 'from new asset profile' do
      visit '/asset_profiles'

      expect(page.has_content?('You are currently viewing all Asset Profiles')).to eql(true)

      click_link ('New Asset Profile')

      expect(page.has_content?('Create a new Asset Profile')).to eql(true)

      within('#asset_profile_form') do
        fill_in 'asset_profile_name', with: 'Macbook Air 13 inch'
        click_button 'Create Asset Profile'
      end

      expect(page.has_content?('Asset Profile: Macbook Air 13 inch has been created.')).to eql(true)

      within('#asset-profiles') do
        click_link 'Show'
      end

      expect(page.has_content?('Macbook Air 13 inch')).to eql(true)

      click_link 'Create Asset from this profile'

      expect(page.has_content?('Create new Asset')).to eql(true)

      expect {
        within('#asset_form') do
          fill_in 'asset_name', with: 'Macbook Air 11 inch'
          select 'Hardware', from: 'asset_categories'
          select 'Unassigned', from: 'asset_assignment_status'
          select 'Healthy', from: 'asset_health_status'
          select 'Under Warranty', from: 'asset_warranty_status'
          fill_in 'asset_warranty_type', with: 'AppleCare 3 Year'
          fill_in 'asset_warranty_expiration_date', with: Date.today.to_date.to_s
          fill_in 'asset_purchase_date', with: '2011/01/01'
          fill_in 'asset_inspection_date', with: Date.tomorrow.to_date.to_s
          fill_in 'asset_serial_number', with: '12345678'
          fill_in 'asset_description', with: 'Gumpf'
          click_button 'Create Asset'
        end
      }.to change(AssetProfile, :count).by(0)

      expect(page.has_content?('Asset: Macbook Air 11 inch has been created.')).to eql true
    end
  end
end
