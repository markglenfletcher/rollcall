require 'spec_helper'

RSpec.describe 'StatusChangeLogging' do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:new_user) do
    User.create(company: company, email: 'a@c.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'Keith', last_name: 'Richards',
        email: 'a@c.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:category) { company.categories.create(name: 'Hardware', user: user) }
  let(:asset) do
    company.assets.create(
      company: company,
      name: 'Macbook',
      creator: user,
      user: user,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::OutForRepair.status_name,
      warranty_status: Status::Warranty::NoWarranty.status_name,
      categories: [
        category
      ]
    )
  end
  before { capybara_login_user user }

  feature 'User edits an asset' do
    before do 
      asset.touch 
      new_user.touch
    end

    scenario 'using valid params' do
      visit "/assets/edit/#{asset.id}"
      
      within('#asset_form') do
        fill_in 'asset_name', with: 'Chromecast'
        select 'Hardware', from: 'asset_categories'
        select 'Assignment In Progress', from: 'asset_assignment_status'
        select 'Healthy', from: 'asset_health_status'
        select 'Under Warranty', from: 'asset_warranty_status'
        select 'Keith Richards', from: 'asset_user'
        fill_in 'asset_warranty_type', with: 'AppleCare 3 Year'
        fill_in 'asset_warranty_expiration_date', with: Date.today.to_date.to_s
        fill_in 'asset_purchase_date', with: '2011/01/01'
        fill_in 'asset_inspection_date', with: Date.tomorrow.to_date.to_s
        fill_in 'asset_serial_number', with: '12345678'
        fill_in 'asset_description', with: 'Gumpf'
        click_button 'Save'
      end

      visit "/assets/show/#{asset.id}"

      within('#status-changes') do
        expect(page.has_content?('Warranty Status Change')).to eq(true)
        expect(page.has_content?('Assignment Status Change')).to eq(true)
        expect(page.has_content?('Health Status Change')).to eq(true)
        expect(page.has_content?('User Status Change')).to eq(true)
      end
    end
  end
end
