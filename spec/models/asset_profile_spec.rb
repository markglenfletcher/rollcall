require 'spec_helper'

RSpec.describe AssetProfile do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:asset_profile) { AssetProfile.new(name: 'Macbook Pro', company: company) }

  it 'should respond to asset_request_items' do
    expect(asset_profile).to respond_to :asset_request_items
  end

  it 'should respond to assets' do
    expect(asset_profile).to respond_to(:assets)
  end

  it 'should respond to categories' do
    expect(asset_profile).to respond_to(:categories)
  end

  it 'should be invalid without a name' do
    asset_profile.name = ''
    expect(asset_profile).to_not be_valid
  end

  it 'should be invalid without a name' do
    asset_profile.company = nil
    expect(asset_profile).to_not be_valid
  end

  context 'company scoping' do
    let (:other_company) { Company.create(name: 'EvilWorks') }
    let (:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }
    let (:other_company_category) { other_company.categories.create(name: 'Other Company Category', user: other_company_user)}
    let (:other_company_asset_profile) {
      other_company.asset_profiles.create(
        name: 'Macbook Pro 15er'
      )
    }
    let (:other_company_asset) { 
      other_company.assets.create(
        name: 'Hardware Asset',
        user: other_company_user,
        creator: other_company_user,
        assignment_status: Status::Assignment::Assigned.status_name,
        health_status: Status::Health::Healthy.status_name,
        warranty_status: Status::Warranty::UnderWarranty.status_name,
        warranty_expiration_date: Date.tomorrow,
        categories: [
          other_company_category
        ]
      )
    }

    it 'should be invalid if assets not in the same company' do
      asset_profile.assets << other_company_asset
      expect(asset_profile).to_not be_valid
    end
  end
end
