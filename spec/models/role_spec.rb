require 'spec_helper'

RSpec.describe Role do
  let(:role) { Role.new(name: 'IT Staff', company: Company.create(name: 'Design Agency')) }
  let(:company) { role.company }

  it 'should respond to assets' do
    expect(role).to respond_to(:assets)
  end

  it 'should respond to asset profiles' do
    expect(role).to respond_to(:asset_profiles)
  end

  it 'should respond to comments' do
    expect(role).to respond_to(:comments)
  end

  it 'should be invalid without a name' do
    role.name = ''
    expect(role).to_not be_valid
  end

  it 'should be invalid without a company' do
    role.company = nil
    expect(role).to_not be_valid
  end

  it 'should create a new staff profile with the same name when one isnt specified' do
    role.staff_profile = nil
    expect{
      role.save
    }.to change(StaffProfile, :count).by (1)
    expect(role.staff_profile.name).to eql(role.name)
  end

  context 'company scoping' do
    let (:other_company) { Company.create(name: 'EvilWorks') }
    let (:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }
    let (:other_company_staff_profile) { other_company.staff_profiles.create(name: 'Evil Staff') }

    it 'should be invalid if user not in the same company' do
      role.users << other_company_user
      expect(role).to_not be_valid
    end

    it 'should be invalid if staff profile is not in the same company' do
      role.staff_profile = other_company_staff_profile
      expect(role).to_not be_valid
    end
  end
end
