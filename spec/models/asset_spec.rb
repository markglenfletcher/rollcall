require 'spec_helper'

RSpec.describe Asset do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) { User.create(company: company, email: 'a@b.com') { |u| u.identities.build(provider: 'provider', uid: 'uid', first_name: 'first', last_name: 'last', email: 'a@b.com', image_url: 'http://img.com/i.png') } }
  let(:category) { Category.create(company: company, name:'Category', user: user) }
  let(:attr) {
    {
      name: 'Hardware Asset',
      user_id: user.id,
      creator_id: user.id,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      categories: [
        category
      ],
      company: company
    }
  }

  let(:asset) { Asset.new(attr) }

  it 'should respond to certain attributes' do
    attributes = [
      :warranty_type, 
      :warranty_expiration_date,
      :purchase_date,
      :inspection_date,
      :serial_number,
      :description,
      :image_url,
      :company
    ]
    attributes.each do |response_attribute|
      expect(asset).to respond_to(response_attribute)
    end
  end

  it 'should respond to asset_request_items' do
    expect(asset).to respond_to :asset_request_items
  end

  it 'should respond to asset_requests' do
    expect(asset).to respond_to :asset_requests
  end

  it 'should be invalid without a company' do
    asset.company = nil
    expect(asset).to_not be_valid
  end

  it 'should be invalid with no name' do
    asset.name = nil
    expect(asset).to_not be_valid
  end

  it 'should be invalid with no creator' do
    asset.creator = nil
    expect(asset).to_not be_valid
  end

  it 'should be invalid with no categories' do
    asset.categories = []
    expect(asset).to_not be_valid
  end

  context 'invalid dates' do
    context 'when setting an inspection date' do
      it 'should be invalid to create an asset with past inspection date' do
        asset.inspection_date =  Date.yesterday
        expect(asset).to be_invalid
        expect(asset.errors.messages[:inspection_date]).to include('inspection date for a new asset cannot be in the past')
      end

      it 'should be ok to edit an asset with past inspection date' do
        asset.inspection_date =  Date.yesterday
        asset.save validate: false
        expect(asset.reload).to be_valid
      end
    end
  end

  it 'should be invalid with no health status' do
    asset.health_status = nil
    expect(asset).to_not be_valid
  end

  it 'should be invalid if health_status not in statuses' do
    asset.health_status = 'not there'
    expect(asset).to_not be_valid
    expect(asset.errors.messages[:health_status]).to include('must be a valid health status')
  end

  it 'should be invalid if assignment_status not in statuses' do
    asset.assignment_status = 'not there'
    expect(asset).to_not be_valid
    expect(asset.errors.messages[:assignment_status]).to include('must be a valid assignment status')
  end

  it 'should be invalid if warranty_status not in statuses' do
    asset.warranty_status = 'not there'
    expect(asset).to_not be_valid
    expect(asset.errors.messages[:warranty_status]).to include('must be a valid warranty status')
  end

  it 'should be invalid if set to assigned with no user' do
    asset.assignment_status = Status::Assignment::Assigned.status_name
    asset.user = nil
    expect(asset).to_not be_valid
    expect(asset.errors.messages[:assignment_status]).to include('cannot be set to assigned without a user')
  end

  it 'should be invalid if set to unassigned with a user' do
    asset.assignment_status = Status::Assignment::Unassigned.status_name
    asset.user = user
    expect(asset).to_not be_valid
    expect(asset.errors.messages[:assignment_status]).to include('cannot be set to unassigned with a user')
  end

  it 'should be invalid if set to reserved with no user' do
    asset.assignment_status = Status::Assignment::Reserved.status_name
    asset.user = nil
    expect(asset).to_not be_valid
    expect(asset.errors.messages[:assignment_status]).to include('cannot be set to reserved without a user')
  end

  context 'company scoping' do
    let (:other_company) { Company.create(name: 'EvilWorks') }
    let (:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }
    let (:other_company_category) { other_company.categories.create(name: 'Other Company Category', user: other_company_user)}
    let (:other_company_asset_profile) { other_company.asset_profiles.create(name: 'Evil Asset')}

    it 'should be invalid if assigned to a user not in the same company' do
      asset.user = other_company_user
      expect(asset).to_not be_valid
    end

    it 'should be invalid if creator is a user not in the same company' do
      asset.creator = other_company_user
      expect(asset).to_not be_valid
    end

    it 'should be invalid if category not in the same company' do
      asset.categories = [other_company_category]
      expect(asset).to_not be_valid
    end

    it 'should be invalid if asset profile not in the same company' do
      asset.asset_profile = other_company_asset_profile
      expect(asset).to_not be_valid
    end
  end

  context 'by default' do
    context 'with a user' do
      before do
        asset.assignment_status = nil
      end

      it 'should have an assignment status of assigned' do
        expect {
          asset.save
        }.to change(asset, :assignment_status).from(nil).to(Status::Assignment::Assigned.status_name)
      end
    end

    context 'without a user' do
      before do
        asset.assignment_status = nil
        asset.user = nil
      end

      it 'should have an assignment status of unassigned' do
        expect {
          asset.save
        }.to change(asset, :assignment_status).from(nil).to(Status::Assignment::Unassigned.status_name)
      end
    end

    context 'with a warranty_expiration_date in the future' do
      before do
        asset.warranty_expiration_date = Date.tomorrow
        asset.warranty_status = nil
      end

      it 'should have a warranty_status of under_warranty' do
        expect {
          asset.save
        }.to change(asset, :warranty_status).from(nil).to(Status::Warranty::UnderWarranty.status_name)
      end
    end

    context 'with a warranty_expiration_date in the past' do
      before do
        asset.warranty_expiration_date = Date.yesterday
        asset.warranty_status = nil
      end

      it 'should have a warranty_status of out_of_warranty' do
        expect {
          asset.save
        }.to change(asset, :warranty_status).from(nil).to(Status::Warranty::OutOfWarranty.status_name)
      end
    end

    context 'with no warranty_expiration_date' do
      before do
        asset.warranty_expiration_date = nil
        asset.warranty_status = nil
      end

      it 'should have a warranty_status of no_warranty' do
        expect {
          asset.save
        }.to change(asset, :warranty_status).from(nil).to(Status::Warranty::NoWarranty.status_name)
      end
    end
  end

  context 'warranty_expiration_date is in the past' do
    before do
      asset.warranty_expiration_date = Date.yesterday
    end

    it 'should be invalid if warranty_status is under warranty' do
      asset.warranty_status = Status::Warranty::UnderWarranty.status_name
      expect(asset).to_not be_valid
      expect(asset.errors.messages[:warranty_status]).to include('cannot be under warranty if warranty expiration date has passed!')
    end

    it 'should be invalid if warranty_status is no warranty' do
      asset.warranty_status = Status::Warranty::NoWarranty.status_name
      expect(asset).to_not be_valid
      expect(asset.errors.messages[:warranty_status]).to include('cannot have no warranty if warranty expiration date has been set!')
    end
  end

  context 'no warranty_expiration_date present' do
    before do 
      asset.warranty_expiration_date = nil
    end

    it 'should be invalid if warranty_status is under warranty' do
      asset.warranty_status = Status::Warranty::UnderWarranty.status_name
      expect(asset).to_not be_valid
      expect(asset.errors.messages[:warranty_status]).to include('cannot be under warranty if no warranty expiration date has been set!')
    end
  end

  it 'should create a new asset profile with the same name when one isnt specified' do
    asset.asset_profile_id = nil
    expect {
      asset.save
    }.to change(AssetProfile, :count).by (1)
    expect(asset.asset_profile.name).to eql(asset.name)
  end

  it 'should respond to comments' do
    expect(asset).to respond_to(:comments)
  end

  context '#under_warranty?' do
    context 'when no warranty date' do
      it 'shouldnt blow up' do
        asset.warranty_expiration_date = nil
        expect(asset.under_warranty?).to eq(false)
      end
    end

    context 'when under warranty' do
      before do
        asset.warranty_expiration_date = Date.tomorrow
      end

      it 'should return true' do
        expect(asset.under_warranty?).to eq(true)
      end
    end

    context 'when out of warranty' do
      before do
        asset.warranty_expiration_date = Date.yesterday
      end

      it 'should return false' do
        expect(asset.under_warranty?).to eq(false)
      end
    end
  end

  context '#clone_asset' do
    before { asset.save }
    let(:asset_clone) { asset.clone_asset }

    it 'should save the clone' do
      expect(asset_clone).to be_persisted
    end

    it 'should maintain the company' do
      expect(asset_clone.company).to eq(asset.company)
    end

    it 'should maintain the asset profile' do
      expect(asset_clone.asset_profile).to eq(asset.asset_profile)
    end

    it 'should maintain the name' do
      expect(asset_clone.name).to eq(asset.name)
    end

    it 'should maintain the creator' do
      expect(asset_clone.creator).to eq(asset.creator)
    end

    it 'should maintain the categories' do
      expect(asset_clone.categories).to eq(asset.categories)
    end

    it 'should maintain the warranty status' do
      expect(asset_clone.warranty_status).to eq(asset.warranty_status)
    end

    it 'should maintain the health status' do
      expect(asset_clone.health_status).to eq(asset.health_status)
    end

    it 'should unassign the asset' do
      expect(asset_clone.user).to be_nil
      expect(asset_clone.assignment_status).to eq(Status::Assignment::Unassigned.status_name)
    end
  end

  context '::by_status' do
    let(:healthy_asset) { Asset.create(attr) }
    let(:dead_asset) { Asset.create(attr.merge(health_status: Status::Health::Dead.status_name)) }

    it 'should return the correct assets' do
      assets = Asset.by_status('health_status','Healthy')
      expect(assets).to include(healthy_asset)
      expect(assets).to_not include(dead_asset)
    end

    it 'should fail gracefully if handed rubbish column' do
      assets = Asset.by_status('rubbish_status','rubbish')
      expect(assets).to be_empty
    end
  end

  context '::from_asset_profile' do
    let(:asset_profile) { AssetProfile.create(name: 'Name', company: company) }

    it 'should return a clone of the asset profile' do
      asset = Asset.from_asset_profile(asset_profile, user)
      expect(asset).to_not be_persisted
      expect(asset.name).to eq(asset_profile.name)
      expect(asset.creator).to eq(user)
      expect(asset.asset_profile).to eq(asset_profile)
      expect(asset.company).to eq(company)
    end
  end

  context 'asset status changes' do
    let(:modifier) { company.users.create(email: 'davey.crockett@msn.com') }

    before do
      asset.modifying_user = modifier
      asset.save
    end

    it 'should have the correct changer' do
      asset.update(assignment_status: Status::Assignment::AssignmentInProgress.status_name)
      asset.asset_status_changes.each do |status_change|
        expect(status_change.user).to eq(modifier)
      end
    end

    it 'should respond to asset_status_changes' do
      expect(asset).to respond_to :asset_status_changes
    end

    context 'assignment change' do
      let(:new_assignment_status) { Status::Assignment::AssignmentInProgress.status_name }

      it 'should create a new asset status change' do
        expect do
          asset.update(assignment_status: new_assignment_status)
        end.to change(AssignmentStatusChange, :count).by(1)
      end
    end

    context 'health change' do
      let(:new_health_status) { Status::Health::Dead.status_name }

      it 'should create a new asset status change' do
        expect do
          asset.update(health_status: new_health_status)
        end.to change(HealthStatusChange, :count).by(1)
      end
    end

    context 'warranty change' do
      let(:new_warranty_status) { Status::Warranty::OutOfWarranty.status_name }

      it 'should create a new asset status change' do
        expect do
          asset.update(warranty_status: new_warranty_status)
        end.to change(WarrantyStatusChange, :count).by(1)
      end
    end

    context 'user change' do
      let(:new_user) { company.users.create(email: 'keith.richards@rollcall.com') }

      it 'should create a new user status change' do
        expect do
          asset.update(user: new_user)
        end.to change(UserStatusChange, :count).by(1)
      end
    end
  end

  context 'scopes' do
    let(:other_company) { Company.create(name: 'EvilWorks') }
    let(:other_company_asset) do
      other_company.assets.create(
        attr.merge(
          warranty_expiration_date: Date.tomorrow,
          inspection_date: Date.tomorrow
        ))
    end
    let(:expiring_asset1) do
      company.assets.create(
        attr.merge(
          warranty_expiration_date: 1.weeks.from_now,
          inspection_date: 1.weeks.from_now
        ))
    end
    let(:not_expiring_asset1) do
      company.assets.create(
        attr.merge(
          warranty_expiration_date: 1.months.from_now,
          inspection_date: 1.months.from_now
        ))
    end

    before do
      asset.save
      expiring_asset1.save
      not_expiring_asset1.save
      other_company_asset.save
    end

    context '::warranty_expiring' do
      it 'should return assets about to expire' do
        expiring_warranties = Asset.warranty_expiring(company)

        expect(expiring_warranties).to include(asset)
        expect(expiring_warranties).to include(expiring_asset1)
        expect(expiring_warranties).to_not include(not_expiring_asset1)
      end

      it 'should only return assets for this company' do
        expiring_warranties = Asset.warranty_expiring(company)

        expect(expiring_warranties).to_not include(other_company_asset)
      end
    end

    context '::inspection_due' do
      it 'should return assets needing inspection' do
        impending_inspections = Asset.inspection_due(company)

        expect(impending_inspections).to include(expiring_asset1)
        expect(impending_inspections).to_not include(not_expiring_asset1)
      end

      it 'should only return assets for this company' do
        impending_inspections = Asset.warranty_expiring(company)

        expect(impending_inspections).to_not include(other_company_asset)
      end
    end
  end
end
