require 'spec_helper'

RSpec.describe AssetRequestItem do
	let(:company) { Company.new(name: 'Design Agency') }
	let(:author) { User.create(email: 'author@rollcall.com', company: company) }
	let(:asset_profile) { AssetProfile.new(name: 'Profile', company: company) }
	let(:asset_request) { 
		AssetRequest.new( company: company, requester: author, 
			due_date: Date.tomorrow, asset_profiles: [asset_profile])
	}
	let(:attr) {
		{
			asset_profile: asset_profile,
			asset_request: asset_request
		}
	}
	let(:asset_request_item) { AssetRequestItem.new(attr) }


	it 'should respond to asset_request' do
		expect(asset_request_item).to respond_to(:asset_request)
	end

	it 'should respond to asset_profile' do
		expect(asset_request_item).to respond_to(:asset_profile)
	end

	it 'should respond to asset' do
		expect(asset_request_item).to respond_to(:asset)
	end

	it 'should be invalid without an asset_request' do
		asset_request_item.asset_request = nil
		expect(asset_request_item).to be_invalid
	end

	it 'should be invalid without an asset_profile' do
		asset_request_item.asset_profile = nil
		expect(asset_request_item).to be_invalid
	end

	it 'should be invalid if an asset is not of the same asset profile' do
		incorrect_profile = AssetProfile.new(company: company, name: 'Incorrect')
		asset_request_item.asset = Asset.new(asset_profile: incorrect_profile)
		expect(asset_request_item).to be_invalid
	end

	context 'complete?' do
    it 'should respond to status' do
      expect(asset_request_item).to respond_to(:complete?)
    end

    context 'incomplete' do
    	before do
    		asset_request_item.asset = nil
    	end

      it 'should be incomplete when an asset is not assigned' do
      	expect(asset_request_item).to_not be_complete
      end
    end

    context 'complete' do
    	before do
    		asset_request_item.asset = Asset.new
    	end

      it 'should be complete when when an asset is assigned' do
      	expect(asset_request_item).to be_complete
      end
    end
  end
end
