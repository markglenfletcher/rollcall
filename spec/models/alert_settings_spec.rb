require 'spec_helper'

RSpec.describe AlertSettings do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:alert_settings) { AlertSettings.new(company: company) }
  let(:user) { User.create(email: 'a@b.com') }

  it 'should respond to company' do
    expect(alert_settings).to respond_to(:company)
  end

  it 'should respond to users' do
    expect(alert_settings).to respond_to(:users)
  end

  it 'should be invalid without a company' do
    alert_settings.company = nil
    expect(alert_settings).to_not be_valid
  end

  it 'should be fine with having no users' do
    alert_settings.users = []
    expect(alert_settings).to be_valid
  end

  it 'should not allow non-company users to be added' do
    alert_settings.save
    alert_settings.users << user
    expect(alert_settings).to_not be_valid
  end

  it 'should be destroyed when a company is' do
    alert_settings.save
    expect do 
      company.destroy
    end.to change(AlertSettings, :count).by(-1)
  end
end
