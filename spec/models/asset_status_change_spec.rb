require 'spec_helper'

RSpec.describe AssetStatusChange do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) { company.users.create(email: 'a@b.com') }
  let(:category) { company.categories.new(name: 'Other Company Category', user: user) }
  let(:asset) do
    company.assets.new(
      name: 'Hardware Asset',
      user: user,
      creator: user,
      assignment_status: Status::Assignment::Assigned.status_name,
      health_status: Status::Health::Healthy.status_name,
      warranty_status: Status::Warranty::UnderWarranty.status_name,
      warranty_expiration_date: Date.tomorrow,
      categories: [
        category
      ]
    )
  end
  let(:attr) do
    {
      asset: asset,
      changed_from: 'str1',
      changed_to: 'str2',
      changed_date: Date.today,
      user: user
    }
  end
  let(:asset_status_change) { AssetStatusChange.new(attr) }

  it 'should be invalid without an asset' do
    asset_status_change.asset = nil
    expect(asset_status_change).to be_invalid
  end

  it 'should be invalid without a user' do
    asset_status_change.user = nil
    expect(asset_status_change).to be_invalid
  end

  it 'should be invalid without a changed_from' do
    asset_status_change.changed_from = nil
    expect(asset_status_change).to be_invalid
  end

  it 'should be invalid without a changed_to' do
    asset_status_change.changed_to = nil
    expect(asset_status_change).to be_invalid
  end

  it 'should be invalid without a changed_date' do
    asset_status_change.changed_date = nil
    expect(asset_status_change).to be_invalid
  end

  context '#changed_to_status' do
    it 'should return the correct value' do
      expect(asset_status_change.changed_to_status).to eq(asset_status_change.changed_to)
    end
  end

  context '#changed_from_status' do
    it 'should return the correct value' do
      expect(asset_status_change.changed_from_status).to eq(asset_status_change.changed_from)
    end
  end

  it 'should be invalid with a changed_date in the future' do
    asset_status_change.changed_date = Date.tomorrow
    expect(asset_status_change).to be_invalid
    expect(asset_status_change.errors.messages[:changed_date]).to include('cannot be future dated.')
  end

  it 'should be invalid if changed_from and changed_to are identical' do
    asset_status_change.changed_to = asset_status_change.changed_from
    expect(asset_status_change).to be_invalid
    expect(asset_status_change.errors.messages[:changed_from]).to include('cannot be identical to changed to.')
  end

  it 'should have the correct status class to reference' do
    expect(AssignmentStatusChange.status_class).to eq(Status::Assignment)
    expect(HealthStatusChange.status_class).to eq(Status::Health)
    expect(WarrantyStatusChange.status_class).to eq(Status::Warranty)
  end

  context 'UserStatusChange' do
    let(:new_user) { company.users.create(email: 'keith.richards@rollcall.com') }
    let(:user_status_change) { UserStatusChange.new(attr.merge(changed_from: user.id, changed_to: new_user.id)) }

    context '#changed_to_status' do
      it 'should return the correct user when assigned' do
        expect(user_status_change.changed_to_status).to eq(new_user)
      end

      it 'should return No User when unassigned' do
        user_status_change.changed_to = 0
        expect(user_status_change.changed_to_status).to eq(nil)
      end
    end

    context '#changed_from_status' do
      it 'should return the correct user when assigned' do
        expect(user_status_change.changed_from_status).to eq(user)
      end

      it 'should return No User when unassigned' do
        user_status_change.changed_from = 0
        expect(user_status_change.changed_from_status).to eq(nil)
      end
    end
  end
end
