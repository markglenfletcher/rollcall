require 'spec_helper'

RSpec.describe SignUp do
	let(:attr) do
		{
			company: Company.new(name: 'Acme'),
			inviter: User.new(email: 'inviter@rollcall.com'),
			user: User.new(email: 'new_user@rollcall.com')
		}
	end

	let(:signup) { SignUp.new(attr) }

	it 'should respond to company' do
		expect(signup).to respond_to(:company)
	end

	it 'should respond to user' do
		expect(signup).to respond_to(:user)
	end

	it 'should respond to inviter' do
		expect(signup).to respond_to(:inviter)
	end

	it 'should be invalid without a company' do
		signup.company = nil
		expect(signup).to be_invalid
	end

	it 'should be invalid without a user' do
		signup.user = nil
		expect(signup).to be_invalid
	end

	it 'should be invalid without an inviter' do
		signup.inviter = nil
		expect(signup).to be_invalid
	end

	it 'should generate a signup code upon save' do
		expect(signup.signup_code).to be_nil
		signup.save
		expect(signup.signup_code).to_not be_nil
	end
end

RSpec.describe SignUp::SignupCodeGeneration do
	it 'should create a unique string' do
		codes = 5.times.map { SignUp::SignupCodeGeneration.generate_signup_code }
		expect(codes.uniq).to eq(codes)
	end
end
