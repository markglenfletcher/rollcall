require 'spec_helper'

RSpec.describe Comment do
  let(:user) do
    User.create(email: 'a@b.com') do |u| 
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:role) { Role.create(name: 'My Role') }
  let(:comment) { Comment.new(text: 'My Comment', user_id: user, 
    commentable_id: role)
  }

  it 'should be invalid without a comment' do
    comment.text = ''
    expect(comment).to_not be_valid
  end

  it 'should be invalid without a user' do
    comment.user = nil
    expect(comment).to_not be_valid
  end

  it 'should be invalid without a commentable' do
    comment.commentable = nil
    expect(comment).to_not be_valid
  end

  it 'should respond to comments' do
    expect(comment).to respond_to(:comments)
  end
end
