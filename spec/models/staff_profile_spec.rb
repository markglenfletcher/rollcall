require 'spec_helper'

RSpec.describe StaffProfile do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:staff_profile) { StaffProfile.new(name: 'IT Staff Profile', company: company) }
  let(:other_company) { Company.create(name: 'EvilWorks') }

  it 'should respond to company' do
    expect(staff_profile).to respond_to(:company)
  end

  it 'should respond to roles' do
    expect(staff_profile).to respond_to(:roles)
  end

  it 'should respond to asset profiles' do
    expect(staff_profile).to respond_to(:asset_profiles)
  end
  
  it 'should respond to users' do
    expect(staff_profile).to respond_to(:users)
  end

  it 'should be invalid without a company' do
    staff_profile.company = nil
    expect(staff_profile).to_not be_valid
  end

  it 'should be invalid without a name' do
    staff_profile.name = ''
    expect(staff_profile).to_not be_valid
  end

  context 'company scoping' do
    let (:other_company_role) { other_company.roles.create(name: 'Evil IT Admin') }

    it 'should be invalid if role is not in the same company' do
      staff_profile.roles << other_company_role
      expect(staff_profile).to_not be_valid
    end
  end
end
