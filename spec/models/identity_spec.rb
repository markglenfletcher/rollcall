require 'spec_helper'

RSpec.describe Identity do
  let(:identity) { Identity.new(provider: 'provider', uid: 'uid') }

  it 'should respond to user' do
    expect(identity).to respond_to(:user)
  end

  it 'should be invalid without a provider' do
    identity.provider = nil
    expect(identity).to be_invalid
  end

  it 'should be invalid without a provider' do
    identity.uid = nil
    expect(identity).to be_invalid
  end

  context '::create_with_omniauth!' do
    let(:auth) do
      {
        'provider' => 'provider',
        'uid' => 'uid',
        'info' => {
          'email' => 'a@b.com',
          'first_name' => 'yuri',
          'last_name' => 'gellar',
          'image' => 'http://image.com/me.jpg'
        }
      }
    end

    it 'should create the expected identity' do
      identity = Identity.create_with_omniauth!(auth)
      expect(identity.new_record?).to eql(false)
      expect(identity.persisted?).to eql(true)
      expect(identity.email).to eql(auth['info']['email'])
      expect(identity.first_name).to eql(auth['info']['first_name'])
      expect(identity.last_name).to eql(auth['info']['last_name'])
      expect(identity.image_url).to eql(auth['info']['image'])
      expect(identity.provider).to eql(auth['provider'])
      expect(identity.uid).to eql(auth['uid'])
    end

    it 'should return nil if identity could not be created' do
      false_auth = auth.dup
      false_auth['uid'] = nil
      expect(Identity.create_with_omniauth!(false_auth)).to be_nil
    end
  end
end
