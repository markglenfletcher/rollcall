require 'spec_helper'

RSpec.describe AssetRequest do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) { company.users.create(email: 'author@rollcall.com') }
  let(:asset_request_item1) do
    AssetRequestItem.new(asset_profile: AssetProfile.create(name: 'Macbook', company: company))
  end 
  let(:asset_request_item2) do
    AssetRequestItem.new(asset_profile: AssetProfile.create(name: 'Secondary Display', company: company))
  end

  let(:attr) do
    {
      requester: user,
      user: user,
      manager: user,
      due_date: Date.tomorrow,
      asset_request_items: [
        asset_request_item1,
        asset_request_item2
      ],
      company: company
    }
  end

  let(:asset_request) do
    AssetRequest.new(attr)
  end

  it 'should respond to asset_request_items' do
    expect(asset_request).to respond_to(:asset_request_items)
  end

  it 'should respond to asset_profiles' do
    expect(asset_request).to respond_to(:asset_profiles)
  end

  it 'should respond to assets' do
    expect(asset_request).to respond_to(:assets)
  end

  it 'should respond to company' do
    expect(asset_request).to respond_to(:company)
  end

  it 'should respond to assignee' do
    expect(asset_request).to respond_to(:user)
    expect(asset_request).to respond_to(:assignee)
  end

  it 'should respond to managger' do
    expect(asset_request).to respond_to(:manager)
  end

  it 'should respond to due_date' do
    expect(asset_request).to respond_to(:due_date)
  end

  it 'should respond to asset_profiles' do
    expect(asset_request).to respond_to(:asset_profiles)
  end

  it 'should be invalid without a company' do
    asset_request.company = nil
    expect(asset_request).to_not be_valid
  end

  it 'should be invalid without a due date' do
    asset_request.due_date = nil
    expect(asset_request).to_not be_valid
  end

  it 'should be invalid if due date is today' do
    asset_request.due_date = Date.today
    expect(asset_request).to_not be_valid
  end

  it 'should be invalid if due date is in the past' do
    asset_request.due_date = Date.yesterday
    expect(asset_request).to_not be_valid
  end

  it 'should be invalid without a requester' do
    asset_request.requester = nil
    expect(asset_request).to_not be_valid
  end

  it 'should be invalid without asset_request_items' do
    asset_request.asset_request_items = []
    expect(asset_request).to_not be_valid
  end

  it 'should enable the creation of a new user' do
    user_attrs = {
      email: 'new_user@rollcall.com',
      role: Role.create(company: company, name: 'Bin man'),
      company: company
    }

    nested_attr = attr.merge(user_attributes: user_attrs)

    expect do
      AssetRequest.create(nested_attr)
    end.to change(User, :count).by(1)
  end

  it 'should enable the creation of a new asset profile' do
    asset_profile_attrs = {
      name: 'Magic Mouse',
      company: company
    }

    nested_attr = attr.merge(asset_profiles_attributes: [asset_profile_attrs])

    expect do
      AssetRequest.create(nested_attr)
    end.to change(AssetProfile, :count).by(1)
  end

  context '#before_tomorrow?' do
    it 'should return true if due_date in the past' do
      asset_request.due_date = Date.yesterday
      expect(asset_request.before_tomorrow?).to eq true
    end

    it 'should return true if due_date is today' do
      asset_request.due_date = Date.today
      expect(asset_request.before_tomorrow?).to eq true
    end

    it 'should return false if due_date in the future' do
      asset_request.due_date = Date.tomorrow
      expect(asset_request.before_tomorrow?).to eq false
    end

    it 'should return false if due_date is nil' do
      asset_request.due_date = nil
      expect(asset_request.before_tomorrow?).to eq true
    end
  end

  context 'status' do
    it 'should respond to complete' do
      expect(asset_request).to respond_to(:complete?)
    end

    context 'incomplete' do
      before do
        asset_request.asset_request_items = [mock_model(AssetRequestItem, complete?: false)]
      end

      it 'should be incomplete when any associated asset_request_items are incomplete' do
        expect(asset_request).to_not be_complete
      end
    end

    context 'complete' do
      before do
        asset_request.asset_request_items = [mock_model(AssetRequestItem, complete?: true)]
      end

      it 'should be complete when all associated asset_request_items are complete' do
        expect(asset_request).to be_complete
      end
    end
  end

  context 'company scoping' do
    let (:other_company) { Company.create(name: 'EvilWorks') }
    let (:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }

    it 'should be invalid if assigned to a user not in the same company' do
      asset_request.user = other_company_user
      expect(asset_request).to_not be_valid
    end

    it 'should be invalid if requester is a user not in the same company' do
      asset_request.requester = other_company_user
      expect(asset_request).to_not be_valid
    end

    it 'should be invalid if manager not in the same company' do
      asset_request.manager = other_company_user
      expect(asset_request).to_not be_valid
    end
  end

  context '::due' do
    let(:other_company) { Company.create(name: 'EvilWorks') }
    let(:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }
    let(:other_company_asset_profile) do
      other_company.asset_profiles.create(name: 'Evil MBP')
    end
    let(:other_company_asset_request_item) do
      AssetRequestItem.create(asset_profile: other_company_asset_profile)
    end
    let(:other_company_asset_request) do
      other_company.asset_requests.create(
        attr.merge(
          requester: other_company_user,
          assignee: other_company_user,
          manager: other_company_user,
          asset_request_items: [other_company_asset_request_item]
        ))
    end

    before do
      asset_request.save
      other_company_asset_request.save
    end

    it 'should include asset requests that due' do
      due_requests = AssetRequest.due(company)

      expect(due_requests).to include(asset_request)
    end

    it 'should not include asset requests for other companies' do
      due_requests = AssetRequest.due(company)

      expect(due_requests).to_not include(other_company_asset_request)
    end
  end

  context 'alerts' do
    let(:mock_alerter) { double('alerter') }

    it 'should call send alerts when asset request created' do
      expect(Alerters::AssetRequests::RequestCreatedAlerter).to receive(:new).and_return(mock_alerter)
      expect(mock_alerter).to receive(:alert).once
      AssetRequest.create(attr)
    end
  end
end
