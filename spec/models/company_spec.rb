require 'spec_helper'

RSpec.describe Company do
  let(:user) do 
    User.create(email: 'a@b.com') do |u|
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png')
    end
  end
  let(:company) { Company.new( name: 'Design Agency', 
      url: 'http://designagency.com',
      image_url: 'http://designagency.com/logo.png',
      description: 'Description of the Media Agency',
      owner: user)
  }

  context 'validations' do
    it 'should be invalid without a company name' do
      company.name = ''
      expect(company).to_not be_valid
    end
  end

  context 'associations' do
    it 'should respond_to users' do
      expect(company).to respond_to(:users)
    end

    it 'should respond_to categories' do
      expect(company).to respond_to(:categories)
    end

    it 'should respond_to roles' do
      expect(company).to respond_to(:roles)
    end

    it 'should respond_to assets' do
      expect(company).to respond_to(:assets)
    end

    it 'should respond_to asset_profiles' do
      expect(company).to respond_to(:asset_profiles)
    end

    it 'should respond_to staff_profiles' do
      expect(company).to respond_to(:staff_profiles)
    end

    it 'should respond_to asset_requests' do
      expect(company).to respond_to(:asset_requests)
    end

    it 'should respond_to sign_ups' do
      expect(company).to respond_to(:sign_ups)
    end

    it 'should respond to alert_settings' do
      expect(company).to respond_to(:alert_settings)
    end
  end

  it 'should create an alert settings when created' do
    expect do
      company.save
    end.to change(AlertSettings, :count).by(1)
    expect(company.alert_settings).to_not be_nil
  end

  context '#alertees' do
    before do
      company.alert_settings = AlertSettings.new(users: [user])
    end

    it 'should delegate to alert_settings' do
      expect(company.alertees).to eq([user])
    end
  end
end
