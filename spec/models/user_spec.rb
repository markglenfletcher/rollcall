require 'spec_helper'

RSpec.describe User do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:identity) {
    Identity.create(
      provider: 'provider',
      uid: 'uid', 
      email: 'a@b.com',
      first_name: 'Jeff',
      last_name: 'Bridges',
      image_url: 'http://img.url/1.jpg'
    )
  }
  let(:attr) {
    {
      company: company,
      email: 'a@b.com',
      identities: 
      [
        identity
      ]
    }
  }
  let(:user) { User.create(attr) }

  it 'should respond to company' do
    expect(user).to respond_to(:company)
  end

  it 'should respond to role' do
    expect(user).to respond_to(:role)
  end

  it 'should respond to signup' do
    expect(user).to respond_to(:sign_up)
  end

  it 'should respond to invitations' do
    expect(user).to respond_to(:invitations)
  end

  it 'should respond to assets' do
    expect(user).to respond_to(:assets)
  end

  it 'should respond to comments' do
    expect(user).to respond_to(:comments)
  end

  it 'should respond to manager_asset_requests' do
    expect(user).to respond_to(:manager_asset_requests)
  end

  it 'should respond to created_asset_requests' do
    expect(user).to respond_to(:created_asset_requests)
  end

  it 'should respond to asset_requests' do
    expect(user).to respond_to(:asset_requests)
  end

  it 'should respond to alert_settings' do
    expect(user).to respond_to(:alert_settings)
  end

  it 'primary identity should return the correct identity' do
    expect(user.primary_identity).to eql identity
  end

  it 'should have the correct email' do
    expect(user.email).to eql identity.email
  end

  it 'should be invalid without email' do
    user.email = nil
    expect(user).to_not be_valid
  end

  it 'should have the correct first_name' do
    expect(user.first_name).to eql identity.first_name
  end

  it 'should have the correct last_name' do
    expect(user.last_name).to eql identity.last_name
  end

  it 'should have the correct full name' do
    expect(user.full_name).to eql "#{identity.first_name} #{identity.last_name}"
  end

  it 'should have the correct image url' do
    expect(user.image_url).to eql identity.image_url
  end

  it 'should be invalid if the email is not unique' do
    user.touch
    user2 = User.new(attr)
    expect(user2).to be_invalid
    expect(user2.errors.messages[:email]).to include('has already been taken')
  end

  context '#role_name' do
    let(:role) { Role.create(company: company, name: 'IT Staff') }

    context 'user has a role' do
      let(:role_user) { User.new(attr.merge(role: role)) }

      it 'should return role' do
        expect(role_user.role_name).to eql(role.name)
      end
    end

    context 'user has no role' do
      let(:no_role_user) { User.new(attr.merge(role: nil)) }

      it 'should return no role' do
        expect(no_role_user.role_name).to eql('No role')
      end
    end
  end

  context '::create_with_omniauth!' do
    let(:auth) {
      {
        'provider' => 'provider',
        'uid' => 'uid',
        'info' => {
          'email' => 'a@c.com',
          'first_name' => 'yuri',
          'last_name' => 'gellar',
          'image' => 'http://image.com/me.jpg'
        }
      }
    }

    context 'user doesnt yet exist' do
      it 'should create a new user with an identity' do
        new_user = User.create_with_omniauth!(auth)
        expect(new_user.identities.count).to_not eql(0)
      end
    end

    context 'user does exist' do
      let(:existing_user) { User.create(company: company, email: auth['info']['email']) }

      it 'should find an existing user and create an identity' do
        expect {
          User.create_with_omniauth!(auth)
        }.to change(existing_user.reload.identities, :count).by(1)
      end
    end
  end

  context '::find_by_provider_and_uid' do
    it 'should find the correct user' do
      user.identities.create(provider: 'my_provider', uid: '123')
      expect(User.find_by_provider_and_uid('my_provider', '123')).to eql(user)
    end

    it 'should return nil if no identity found' do
      expect(User.find_by_provider_and_uid('no_exist_provider', '123')).to eql(nil)
    end
  end

  context 'company scoping' do
    let (:other_company) { Company.create(name: 'EvilWorks') }
    let (:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }
    let (:other_company_category) { other_company.categories.create(name: 'Other Company Category', user: other_company_user)}
    let (:other_company_asset_profile) { other_company.asset_profiles.create(name: 'Evil Asset')}
    let (:other_company_role) { other_company.roles.create(name: 'Evil IT Admin') }
    let (:other_company_asset) { 
      other_company.assets.create(
        name: 'Hardware Asset',
        user: other_company_user,
        creator: other_company_user,
        assignment_status: Status::Assignment::Assigned.status_name,
        health_status: Status::Health::Healthy.status_name,
        warranty_status: Status::Warranty::UnderWarranty.status_name,
        warranty_expiration_date: Date.tomorrow,
        categories: [
          other_company_category
        ]
      )
    }
    let (:other_company_asset_request) { 
      other_company.asset_requests.create(
        requester: other_company_user,
        due_date: Date.tomorrow,
        asset_profiles: [
          other_company_asset_profile
        ]
      )
    }

    it 'should be invalid if role not in the same company' do
      new_user = User.new(attr.merge(role: other_company_role))
      expect(new_user).to_not be_valid
    end

    it 'should be invalid if assets not in the same company' do
      new_user = User.new(attr.merge(assets: [other_company_asset]))
      expect(new_user).to_not be_valid
    end

    it 'should be invalid if assigned to asset request not in the same company' do
      new_user = User.new(attr.merge(asset_requests: [other_company_asset_request]))
      expect(new_user).to_not be_valid
    end

    it 'should be invalid if managing asset request not in the same company' do
      new_user = User.new(attr.merge(manager_asset_requests: [other_company_asset_request]))
      expect(new_user).to_not be_valid
    end

    it 'should be invalid if creator of asset request not in the same company' do
      new_user = User.new(attr.merge(created_asset_requests: [other_company_asset_request]))
      expect(new_user).to_not be_valid
    end
  end

  context 'destroy' do
    it 'should also destroy all its identities' do
      user.touch
      expect do
        user.destroy
      end.to change(Identity, :count).by(-1)
    end
  end
end
