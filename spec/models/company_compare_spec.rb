require 'spec_helper'

RSpec.describe CompanyCompare do
	class CompanyComparer
		attr_accessor :company
		include CompanyCompare
	end

	let(:company) { Object.new }
	let(:other_company) { Object.new }
	let(:company_scoped) {
		CompanyComparer.new.tap do |cc|
			cc.company = company
		end
	}

	context '#belongs_to_company?' do
    it 'should return true if belongs to company' do
      expect(company_scoped.belongs_to_company?(company)).to eq(true)
    end

    it 'should return false if doesnt belong to company' do
      expect(company_scoped.belongs_to_company?(other_company)).to eq(false)
    end
  end
end
