require 'spec_helper'

RSpec.describe Category do
  let(:company) { Company.create(name: 'Design Agency') }
  let(:user) do
    User.create(company: company, email: 'a@b.com') do |u| 
      u.identities.build(provider: 'provider', uid: 'uid',
        first_name: 'first', last_name: 'last',
        email: 'a@b.com', image_url: 'http://img.com/i.png') 
    end 
  end
  let(:category) { Category.new(name: 'Hardware', company: company, user: user) }

  it 'should respond to company' do
    expect(category).to respond_to(:company)
  end    

  it 'should respond to assets' do
    expect(category).to respond_to(:assets)
  end

  it 'should respond to user' do
    expect(category).to respond_to(:user)
  end

  it 'should respond to comments' do
    expect(category).to respond_to(:comments)
  end

  it 'should be invalid without a name' do
    category.name = nil
    expect(category).to be_invalid
  end

  it 'should be invalid without a user' do
    category.user = nil
    expect(category).to be_invalid
  end

  it 'should be invalid without a company' do
    category.company = nil
    expect(category).to be_invalid
  end

  context 'company scoping' do
    let (:other_company) { Company.create(name: 'EvilWorks') }
    let (:other_company_user) { other_company.users.create(email: 'other_user@evilworks.com') }
    let (:other_company_category) { other_company.categories.create(name: 'Other Company Category', user: other_company_user)}
    let (:other_company_asset) { 
      other_company.assets.create(
        name: 'Hardware Asset',
        user: other_company_user,
        creator: other_company_user,
        assignment_status: Status::Assignment::Assigned.status_name,
        health_status: Status::Health::Healthy.status_name,
        warranty_status: Status::Warranty::UnderWarranty.status_name,
        warranty_expiration_date: Date.tomorrow,
        categories: [
          other_company_category
        ]
      )
    }

    it 'should be invalid if user not in the same company' do
      category.user = other_company_user
      expect(category).to_not be_valid
    end

    it 'should be invalid if assets not in the same company' do
      category.assets << other_company_asset
      expect(category).to_not be_valid
    end
  end
end
