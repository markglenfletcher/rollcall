company = Company.create(name: 'Acme')

non_tech_staff_profile = StaffProfile.create(
  company: company,
  name: 'Non technical Staff Profile')
support_role = Role.create(
  company: company,
  name:'Support', 
  staff_profile: non_tech_staff_profile)
sales_role = Role.create(
  company: company,
  name:'Sales', 
  staff_profile: non_tech_staff_profile)
exec_role = Role.create(
  company: company,
  name:'Execs', 
  staff_profile: non_tech_staff_profile)

keith = User.create(
  company: company,
  email: 'kr@richards.com', 
  role:support_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Keith', 
    last_name: 'Richards', 
    email: 'kr@richards.com', 
    image_url: nil) 
}
suzanne = User.create(
  company: company,
  email: 'suzy@herbster.com',
  role:support_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', first_name: 'Suzanne', 
    last_name: 'Herbies', 
    email: 'suzy@herbster.com', 
    image_url: nil) }

karen = User.create(
  company: company,
  email: 'km@matthews.com', 
  role:sales_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Karen', 
    last_name: 'Matthews', 
    email: 'km@matthews.com', 
    image_url: nil) }
fiona = User.create(
  company: company,
  email: 'fckuoots@cootofmyway.com', 
  role:sales_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Fiona', 
    last_name: 'Coots', 
    email: 'fckuoots@cootofmyway.com', 
    image_url: nil) }

john = User.create(
  company: company,
  email: 'johnnymotson@b.com',
  role:exec_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'John', 
    last_name: 'Motson', 
    email: 'johnnymotson@b.com', 
    image_url: nil) }

tech_staff_profile = StaffProfile.create(
  company: company,
  name: 'Technical Staff Profile')

engineer_role = Role.create(
  company: company,
  name:'Engineering', 
  staff_profile: tech_staff_profile)
it_role = Role.create(
  company: company,
  name:'IT', 
  staff_profile: tech_staff_profile)

rick = User.create(
  company: company,
  email: 'rick@walkers.com', 
  role:engineer_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Rick', 
    last_name: 'Grimes', 
    email: 'rick@walkers.com', 
    image_url: nil) }
trevor = User.create(
  company: company,
  email: 'trev@macca.com',
  role:engineer_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Trevor', 
    last_name: 'McDonald', 
    email: 'trev@macca.com', 
    image_url: nil) }

george = User.create(
  company: company,
  email: 'bigg@little.com', 
  role:it_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'George', 
    last_name: 'Little', 
    email: 'bigg@little.com', 
    image_url: nil) }
cantye = User.create(
  company: company,
  email: 'c@blang.com', 
  role:it_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Cantye', 
    last_name: 'East', 
    email: 'c@blang.com', 
    image_url: nil) }

desktop_hardware_cat = Category.create(
  company: company,
  name: 'Desktop Hardware', 
  user: george)
laptop_hardware_cat = Category.create(
  company: company,
  name: 'Laptop Hardware', 
  user: george)
monitor_cat = Category.create(
  company: company,
  name: 'Monitor', 
  user: george)

tech_software_cat = Category.create(
  company: company,
  name: 'Technical Software', 
  user: george)
office_software_cat = Category.create(
  company: company,
  name: 'Office Software', 
  user: george)
apple_category = Category.create(
  company: company,
  name: 'Apple',
  user:george)

macbook_profile = AssetProfile.create(
  company: company,name: 'Macbook')
keiths_macbook = macbook_profile.assets.create(
  company: company,
  name: 'Macbook', 
  user: keith, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 1.year.from_now, 
  inspection_date: 6.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
suzannes_macbook = macbook_profile.assets.create(
  company: company,
  name: 'Macbook', 
  user: suzanne, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::DueAnUpgrade.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 2.years.from_now, 
  purchase_date: '11/11/2011', 
  inspection_date: 2.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

karens_macbook = macbook_profile.assets.create(
  company: company,
  name: 'Macbook', 
  user: karen, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::NoWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: nil,
  purchase_date: '11/11/2011', 
  inspection_date: 10.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
fionas_macbook = macbook_profile.assets.create(
  company: company,
  name: 'Macbook', 
  user: fiona, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 1.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

johns_macbook = macbook_profile.assets.create(
  company: company,
  name: 'Macbook', 
  user: john, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: Date.tomorrow,
  purchase_date: '11/11/2011', 
  inspection_date: 2.years.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

imac_profile = AssetProfile.create(
  company: company,name: 'iMac')
ricks_imac = imac_profile.assets.create(
  company: company,
  name: 'iMac 27', 
  user: rick, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 3.weeks.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
trevors_imac = imac_profile.assets.create(
  company: company,
  name: 'iMac 27', 
  user: trevor, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Healthy.status_name,
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 7.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

georges_imac = imac_profile.assets.create(
  company: company,
  name: 'iMac 27', 
  user: george, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::NoWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: nil,
  purchase_date: '11/11/2011', 
  inspection_date: 3.years.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
cantyes_macbook = macbook_profile.assets.create(
  company: company,
  name: 'Macbook', 
  user: cantye, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 1.week.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

monitor_profile = AssetProfile.create(
  company: company,name: 'Monitor')
ricks_monitor = monitor_profile.assets.create(
  company: company,
  name: '27 inch monitor', 
  user: rick, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Dead.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 2.months.from_now,
  purchase_date: '11/11/2011', 
  inspection_date: 14.weeks.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
trevors_imac = monitor_profile.assets.create(
  company: company,
  name: '21 inch monitor', 
  user: trevor, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::OutForRepair.status_name, 
  warranty_status: Status::Warranty::NoWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date:nil,
  purchase_date: '11/11/2011', 
  inspection_date: 5.days.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

johns_monitor = monitor_profile.assets.create(
  company: company,
  name: '30 inch Apple Cinema Display', 
  user: john, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 3.years.from_now,
  purchase_date: '11/11/2011', 
  inspection_date: 12.weeks.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
cantyes_monitor = monitor_profile.assets.create(
  company: company,
  name: '27 inch Apple Cinema Display', 
  user: cantye, 
  creator: george, 
  categories: [apple_category,monitor_cat],
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::DueAnUpgrade.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 7.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

unassigned_monitor = monitor_profile.assets.create(
  company: company,
  name: '21 inch monitor', 
  user: nil, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::AssignmentInProgress.status_name, 
  health_status: Status::Health::NeedsRepair.status_name, 
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2013',
  purchase_date: '11/11/2011', 
  inspection_date: 8.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assignment in progress')

unassigned_acd = monitor_profile.assets.create(
  company: company,
  name: '27 inch Apple Cinema Display', 
  user: nil, 
  creator: george, 
  categories: [apple_category,monitor_cat],
  assignment_status: Status::Assignment::Unassigned.status_name, 
  health_status: Status::Health::Dead.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 1.year.from_now,
  purchase_date: '11/11/2011', 
  inspection_date: 9.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset unassigned')

unassigned_imac = imac_profile.assets.create(
  company: company,
  name: 'iMac 27', 
  user: john, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Reserved.status_name,
  health_status: Status::Health::DueAnUpgrade.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 11.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')


company2 = Company.create(name: 'Warner Bros')

non_tech_staff_profile = StaffProfile.create(
  company: company2,
  name: 'Non technical Staff Profile')
support_role = Role.create(
  company: company2,
  name:'Support', 
  staff_profile: non_tech_staff_profile)
sales_role = Role.create(
  company: company2,
  name:'Sales', 
  staff_profile: non_tech_staff_profile)
exec_role = Role.create(
  company: company2,
  name:'Execs', 
  staff_profile: non_tech_staff_profile)

keith = User.create(
  company: company2,
  email: 'kr@richards.co.uk', 
  role:support_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Keitho', 
    last_name: 'Richardso', 
    email: 'kr@richards.co.uk', 
    image_url: nil) 
}
suzanne = User.create(
  company: company2,
  email: 'suzy@herbster.co.uk',
  role:support_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', first_name: 'Suzanne', 
    last_name: 'Herbies', 
    email: 'suzy@herbster.co.uk', 
    image_url: nil) }

karen = User.create(
  company: company2,
  email: 'km@matthews.co.uk', 
  role:sales_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Karen', 
    last_name: 'Matthews', 
    email: 'km@matthews.co.uk', 
    image_url: nil) }
fiona = User.create(
  company: company2,
  email: 'fckuoots@cootofmyway.co.uk', 
  role:sales_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Fiona', 
    last_name: 'Coots', 
    email: 'fckuoots@cootofmyway.co.uk', 
    image_url: nil) }

john = User.create(
  company: company2,
  email: 'johnnymotson@b.co.uk',
  role:exec_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'John', 
    last_name: 'Motson', 
    email: 'johnnymotson@b.co.uk', 
    image_url: nil) }

tech_staff_profile = StaffProfile.create(
  company: company2,
  name: 'Technical Staff Profile')

engineer_role = Role.create(
  company: company2,
  name:'Engineering', 
  staff_profile: tech_staff_profile)
it_role = Role.create(
  company: company2,
  name:'IT', 
  staff_profile: tech_staff_profile)

rick = User.create(
  company: company2,
  email: 'rick@walkers.co.uk', 
  role:engineer_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Rick', 
    last_name: 'Grimes', 
    email: 'rick@walkers.co.uk', 
    image_url: nil) }
trevor = User.create(
  company: company2,
  email: 'trev@macca.co.uk',
  role:engineer_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Trevor', 
    last_name: 'McDonald', 
    email: 'trev@macca.co.uk', 
    image_url: nil) }

george = User.create(
  company: company2,
  email: 'bigg@little.co.uk', 
  role:it_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'George', 
    last_name: 'Little', 
    email: 'bigg@little.co.uk', 
    image_url: nil) }
cantye = User.create(
  company: company2,
  email: 'c@blang.co.uk', 
  role:it_role) { |u| u.identities.build(
    provider: 'provider', 
    uid: 'uid', 
    first_name: 'Cantye', 
    last_name: 'East', 
    email: 'c@blang.co.uk', 
    image_url: nil) }

desktop_hardware_cat = Category.create(
  company: company2,
  name: 'Desktop Hardware', 
  user: george)
laptop_hardware_cat = Category.create(
  company: company2,
  name: 'Laptop Hardware', 
  user: george)
monitor_cat = Category.create(
  company: company2,
  name: 'Monitor', 
  user: george)

tech_software_cat = Category.create(
  company: company2,
  name: 'Technical Software', 
  user: george)
office_software_cat = Category.create(
  company: company2,
  name: 'Office Software', 
  user: george)
apple_category = Category.create(
  company: company2,
  name: 'Apple',
  user:george)

macbook_profile = AssetProfile.create(
  company: company2,name: 'Macbook')
keiths_macbook = macbook_profile.assets.create(
  company: company2,
  name: 'Macbook', 
  user: keith, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 1.year.from_now, 
  inspection_date: 6.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
suzannes_macbook = macbook_profile.assets.create(
  company: company2,
  name: 'Macbook', 
  user: suzanne, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::DueAnUpgrade.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 2.years.from_now, 
  purchase_date: '11/11/2011', 
  inspection_date: 2.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

karens_macbook = macbook_profile.assets.create(
  company: company2,
  name: 'Macbook', 
  user: karen, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::NoWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: nil,
  purchase_date: '11/11/2011', 
  inspection_date: 10.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
fionas_macbook = macbook_profile.assets.create(
  company: company2,
  name: 'Macbook', 
  user: fiona, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 1.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

johns_macbook = macbook_profile.assets.create(
  company: company2,
  name: 'Macbook', 
  user: john, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: Date.tomorrow,
  purchase_date: '11/11/2011', 
  inspection_date: 2.years.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

imac_profile = AssetProfile.create(
  company: company2,name: 'iMac')
ricks_imac = imac_profile.assets.create(
  company: company2,
  name: 'iMac 27', 
  user: rick, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 3.weeks.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
trevors_imac = imac_profile.assets.create(
  company: company2,
  name: 'iMac 27', 
  user: trevor, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Healthy.status_name,
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 7.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

georges_imac = imac_profile.assets.create(
  company: company2,
  name: 'iMac 27', 
  user: george, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::NoWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: nil,
  purchase_date: '11/11/2011', 
  inspection_date: 3.years.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
cantyes_macbook = macbook_profile.assets.create(
  company: company2,
  name: 'Macbook', 
  user: cantye, 
  creator: george, 
  categories: [apple_category,laptop_hardware_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 1.week.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

monitor_profile = AssetProfile.create(
  company: company2,name: 'Monitor')
ricks_monitor = monitor_profile.assets.create(
  company: company2,
  name: '27 inch monitor', 
  user: rick, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::Assigned.status_name,
  health_status: Status::Health::Dead.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 2.months.from_now,
  purchase_date: '11/11/2011', 
  inspection_date: 14.weeks.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
trevors_imac = monitor_profile.assets.create(
  company: company2,
  name: '21 inch monitor', 
  user: trevor, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::OutForRepair.status_name, 
  warranty_status: Status::Warranty::NoWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date:nil,
  purchase_date: '11/11/2011', 
  inspection_date: 5.days.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

johns_monitor = monitor_profile.assets.create(
  company: company2,
  name: '30 inch Apple Cinema Display', 
  user: john, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::Healthy.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 3.years.from_now,
  purchase_date: '11/11/2011', 
  inspection_date: 12.weeks.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
cantyes_monitor = monitor_profile.assets.create(
  company: company2,
  name: '27 inch Apple Cinema Display', 
  user: cantye, 
  creator: george, 
  categories: [apple_category,monitor_cat],
  assignment_status: Status::Assignment::Assigned.status_name, 
  health_status: Status::Health::DueAnUpgrade.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 7.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')

unassigned_monitor = monitor_profile.assets.create(
  company: company2,
  name: '21 inch monitor', 
  user: nil, 
  creator: george, 
  categories: [apple_category,monitor_cat], 
  assignment_status: Status::Assignment::AssignmentInProgress.status_name, 
  health_status: Status::Health::NeedsRepair.status_name, 
  warranty_status: Status::Warranty::OutOfWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2013',
  purchase_date: '11/11/2011', 
  inspection_date: 8.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assignment in progress')

unassigned_acd = monitor_profile.assets.create(
  company: company2,
  name: '27 inch Apple Cinema Display', 
  user: nil, 
  creator: george, 
  categories: [apple_category,monitor_cat],
  assignment_status: Status::Assignment::Unassigned.status_name, 
  health_status: Status::Health::Dead.status_name, 
  warranty_status: Status::Warranty::UnderWarranty.status_name, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: 1.year.from_now,
  purchase_date: '11/11/2011', 
  inspection_date: 9.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset unassigned')

unassigned_imac = imac_profile.assets.create(
  company: company2,
  name: 'iMac 27', 
  user: john, 
  creator: george, 
  categories: [apple_category,desktop_hardware_cat], 
  assignment_status: Status::Assignment::Reserved.status_name,
  health_status: Status::Health::DueAnUpgrade.status_name, 
  warranty_status: nil, 
  warranty_type: 'AppleCare', 
  warranty_expiration_date: '11/11/2011',
  purchase_date: '11/11/2011', 
  inspection_date: 11.months.from_now, 
  serial_number: '12345678', 
  description: 'Asset assigned to a colleague')
