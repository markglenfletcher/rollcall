# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 19) do

  create_table "alert_settings", force: :cascade do |t|
    t.integer  "company_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "asset_profiles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id", limit: 4
  end

  create_table "asset_request_items", force: :cascade do |t|
    t.integer  "asset_request_id", limit: 4
    t.integer  "asset_profile_id", limit: 4
    t.integer  "asset_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "asset_requests", force: :cascade do |t|
    t.date     "due_date"
    t.integer  "requester_id", limit: 4
    t.integer  "user_id",      limit: 4
    t.integer  "manager_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",   limit: 4
  end

  create_table "asset_status_changes", force: :cascade do |t|
    t.string   "changed_from", limit: 255
    t.string   "changed_to",   limit: 255
    t.date     "changed_date"
    t.integer  "asset_id",     limit: 4
    t.integer  "user_id",      limit: 4
    t.string   "type",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "assets", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.integer  "user_id",                  limit: 4
    t.integer  "creator_id",               limit: 4
    t.integer  "asset_profile_id",         limit: 4
    t.string   "assignment_status",        limit: 255
    t.string   "health_status",            limit: 255
    t.string   "warranty_status",          limit: 255
    t.string   "warranty_type",            limit: 255
    t.date     "warranty_expiration_date"
    t.date     "purchase_date"
    t.date     "inspection_date"
    t.string   "serial_number",            limit: 255
    t.text     "description",              limit: 65535
    t.string   "image_url",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",               limit: 4
  end

  create_table "assets_categories", force: :cascade do |t|
    t.integer "asset_id",    limit: 4
    t.integer "category_id", limit: 4
  end

  add_index "assets_categories", ["asset_id"], name: "index_assets_categories_on_asset_id", using: :btree
  add_index "assets_categories", ["category_id"], name: "index_assets_categories_on_category_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id", limit: 4
  end

  create_table "comments", force: :cascade do |t|
    t.text     "text",             limit: 65535
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.integer  "user_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "url",         limit: 255
    t.string   "image_url",   limit: 255
    t.text     "description", limit: 65535
    t.integer  "owner_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "identities", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.string   "image_url",  limit: 255
    t.string   "provider",   limit: 255
    t.string   "uid",        limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.integer  "staff_profile_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",       limit: 4
  end

  create_table "sign_ups", force: :cascade do |t|
    t.integer  "company_id",  limit: 4
    t.integer  "inviter_id",  limit: 4
    t.integer  "user_id",     limit: 4
    t.string   "signup_code", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "staff_profiles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id", limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.integer  "role_id",           limit: 4
    t.string   "email",             limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_id",        limit: 4
    t.integer  "alert_settings_id", limit: 4
  end

end
