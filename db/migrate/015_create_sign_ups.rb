class CreateSignUps < ActiveRecord::Migration
  def self.up
    create_table :sign_ups do |t|
      t.integer :company_id
      t.integer :inviter_id
      t.integer :user_id
      t.string :signup_code
      t.timestamps
    end
  end

  def self.down
    drop_table :sign_ups
  end
end
