class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.string :name
      t.integer :user_id
      t.timestamps
    end

    create_table :assets_categories do |t|
      t.belongs_to :asset, index: true
      t.belongs_to :category, index: true
    end
  end

  def self.down
    drop_table :categories
    drop_table :assets_categories
  end
end
