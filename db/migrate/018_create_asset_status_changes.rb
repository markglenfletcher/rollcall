class CreateAssetStatusChanges < ActiveRecord::Migration
  def self.up
    create_table :asset_status_changes do |t|
      t.string :changed_from
      t.string :changed_to
      t.date :changed_date
      t.integer :asset_id
      t.integer :user_id
      t.string :type
      t.timestamps
    end
  end

  def self.down
    drop_table :asset_status_changes
  end
end
