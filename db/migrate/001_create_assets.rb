class CreateAssets < ActiveRecord::Migration
  def self.up
    create_table :assets do |t|
      t.string :name
      t.integer :user_id
      t.integer :creator_id

      t.integer :asset_profile_id

      t.string :assignment_status
      t.string :health_status
      t.string :warranty_status

      t.string :warranty_type
      t.date :warranty_expiration_date
      t.date :purchase_date
      t.date :inspection_date
      t.string :serial_number
      t.text :description
      t.string :image_url
      
      t.timestamps
    end
  end

  def self.down
    drop_table :assets
  end
end
