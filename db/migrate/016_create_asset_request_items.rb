class CreateAssetRequestItems < ActiveRecord::Migration
  def self.up
    create_table :asset_request_items do |t|
      t.integer :asset_request_id
      t.integer :asset_profile_id
      t.integer :asset_id
      
      t.timestamps
    end
  end

  def self.down
    drop_table :asset_request_items
  end
end
