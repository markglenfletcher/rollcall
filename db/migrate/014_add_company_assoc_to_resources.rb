class AddCompanyAssocToResources < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.integer :company_id
    end
    change_table :categories do |t|
      t.integer :company_id
    end
    change_table :roles do |t|
      t.integer :company_id
    end
    change_table :assets do |t|
      t.integer :company_id
    end
    change_table :asset_profiles do |t|
      t.integer :company_id
    end
    change_table :staff_profiles do |t|
      t.integer :company_id
    end
    change_table :asset_requests do |t|
      t.integer :company_id
    end
  end

  def self.down
    change_table :users do |t|
      t.remove :company_id
    end
    change_table :categories do |t|
      t.remove :company_id
    end
    change_table :roles do |t|
      t.remove :company_id
    end
    change_table :assets do |t|
      t.remove :company_id
    end
    change_table :asset_profiles do |t|
      t.remove :company_id
    end
    change_table :staff_profiles do |t|
      t.remove :company_id
    end
    change_table :asset_requests do |t|
      t.remove :company_id
    end
  end
end
