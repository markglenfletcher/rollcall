class CreateStaffProfiles < ActiveRecord::Migration
  def self.up
    create_table :staff_profiles do |t|
      t.string :name
      t.timestamps
    end
  end

  def self.down
    drop_table :staff_profiles
  end
end
