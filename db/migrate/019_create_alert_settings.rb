class CreateAlertSettings < ActiveRecord::Migration
  def self.up
    create_table :alert_settings do |t|
      t.integer :company_id
      t.timestamps
    end

    change_table(:users) do |t|
      t.column :alert_settings_id, :integer
    end
  end

  def self.down
    drop_table :alert_settings

    change_table(:users) do |t|
      t.remove :alert_settings_id
    end
  end
end
