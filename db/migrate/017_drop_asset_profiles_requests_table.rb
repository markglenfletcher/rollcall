class DropAssetProfilesRequestsTable < ActiveRecord::Migration
  def self.up
  	drop_table :asset_profiles_requests
  end

  def self.down
  	create_table :asset_profiles_requests do |t|
      t.belongs_to :asset_profile, index: true
      t.belongs_to :asset_request, index: true
    end
  end
end
