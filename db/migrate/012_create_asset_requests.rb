class CreateAssetRequests < ActiveRecord::Migration
  def self.up
    create_table :asset_requests do |t|
      t.date :due_date
      t.integer :requester_id
      t.integer :user_id
      t.integer :manager_id
      t.timestamps
    end

    create_table :asset_profiles_requests do |t|
      t.belongs_to :asset_profile, index: true
      t.belongs_to :asset_request, index: true
    end
  end

  def self.down
    drop_table :asset_requests
    drop_table :asset_profiles_requests
  end
end
