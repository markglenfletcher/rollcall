class CreateIdentities < ActiveRecord::Migration
  def self.up
    create_table :identities do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :image_url
      t.string :provider
      t.string :uid

      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :identities
  end
end

