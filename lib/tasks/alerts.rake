namespace :rollcall do
  namespace :alerts do
    desc "Send notification emails for assets with expiring warranties for all companies"
    task :expiring_warranties => :environment do
      Alerters::Assets::ExpiringWarrantiesAlerter.alert
    end

    desc "Send notification emails for assets with impending inspections for all companies"
    task :inspection_due => :environment do
      Alerters::Assets::InspectionDueAlerter.alert
    end

    desc "Send notification emails for asset requests that are due for all companies"
    task :requests_due => :environment do
      Alerters::AssetRequests::RequestsDueAlerter.alert
    end
  end
end
