module Status
  module Health
    class OutForRepair < AssetStatus
      def self.status_class
        'btn-default'
      end
    end
  end
end
