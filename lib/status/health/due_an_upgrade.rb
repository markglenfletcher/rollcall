module Status
  module Health
    class DueAnUpgrade < AssetStatus
      def self.status_class
        'btn-info'
      end
    end
  end
end
