module Status
  module Health
    class Dead < AssetStatus
      def self.status_class
        'btn-danger'
      end
    end
  end
end
