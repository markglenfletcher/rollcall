module Status
  module Health
    class Healthy < AssetStatus
      def self.status_class
        'btn-success'
      end
    end
  end
end
