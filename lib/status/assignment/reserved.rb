module Status
  module Assignment
    class Reserved < AssetStatus
      def self.status_class
        'btn-default'
      end

      def validate(asset)
        [].tap do |errors|
          errors << 'cannot be set to reserved without a user' if asset.user.nil?
        end
      end
    end
  end
end
