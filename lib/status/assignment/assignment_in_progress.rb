module Status
  module Assignment
    class AssignmentInProgress < AssetStatus
      def self.status_class
        'btn-info'
      end
    end
  end
end
