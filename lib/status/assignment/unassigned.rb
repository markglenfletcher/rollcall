module Status
  module Assignment
    class Unassigned < AssetStatus
      def self.status_class
        'btn-success'
      end

      def validate(asset)
        [].tap do |errors|
          errors << 'cannot be set to unassigned with a user' if asset.user
        end
      end
    end
  end
end
