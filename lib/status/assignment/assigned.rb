module Status
  module Assignment
    class Assigned < AssetStatus
      def self.status_class
        'btn-danger'
      end

      def validate(asset)
        [].tap do |errors|
          errors << 'cannot be set to assigned without a user' if asset.user.nil?
        end
      end
    end
  end
end
