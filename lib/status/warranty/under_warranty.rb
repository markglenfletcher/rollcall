module Status
  module Warranty
    class UnderWarranty < AssetStatus
      def self.status_class
        'btn-success'
      end

      def validate(asset)
        [].tap do |errors|
          errors << 'cannot be under warranty if no warranty expiration date has been set!' unless asset.warranty_expiration_date
          errors << 'cannot be under warranty if warranty expiration date has passed!' if asset.warranty_expiration_date && asset.warranty_expiration_date < Date.today
        end
      end
    end
  end
end
