module Status
  module Warranty
    class NoWarranty < AssetStatus
      def self.status_class
        'btn-danger'
      end

      def validate(asset)
        [].tap do |errors|
          errors << 'cannot have no warranty if warranty expiration date has been set!' if asset.warranty_expiration_date
        end
      end
    end
  end
end
