module Status
  class AssetStatus
    def self.status_name
      name.demodulize.titleize
    end

    def self.status_key
      name.demodulize.underscore
    end

    def self.status_class
      'btn-primary'
    end

    def status_name
      self.class.status_name
    end

    def status_key
      self.class.status_key
    end

    def status_class
      self.class.status_class
    end

    def validate(asset)
      []
    end
  end
end
