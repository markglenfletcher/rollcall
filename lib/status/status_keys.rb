module Status
  module StatusKeys
    def [](key)
      klasses.select { |klass| klass.status_key == key.to_s || klass.status_name == key.to_s }.first
    end

    def pairs
      klasses.map do |klass|
        [klass.status_name, klass.status_key]
      end
    end

    def keys
      klasses.map(&:status_key)
    end

    def names
      klasses.map(&:status_name)
    end

    private

    def klasses
      constants.map { |klass| const_get(klass) }
    end
  end
end
