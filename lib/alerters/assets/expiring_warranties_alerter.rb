module Alerters
  module Assets
    class ExpiringWarrantiesAlerter < Alerter
      def alertables
        Asset.warranty_expiring(@company)
      end
      
      def send_alert_email(user)
        Rollcall::App.deliver(:asset_alert, :expiring_warranties, user, alertables)
      end
    end
  end
end
