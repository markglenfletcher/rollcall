module Alerters
  module Assets
    class InspectionDueAlerter < Alerter
      def alertables
        Asset.inspection_due(@company)
      end

      def send_alert_email(user)
        Rollcall::App.deliver(:asset_alert, :inspection_due, user, alertables)
      end
    end
  end
end
