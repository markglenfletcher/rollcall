module Alerters
  module AssetRequests
    class RequestCreatedAlerter < Alerter
      def initialize(company, asset_request)
        @asset_request = asset_request
        super(company)
      end

      def alertables
        @asset_request
      end

      def send_alert_email(user)
        Rollcall::App.deliver(:asset_request_alert, :request_created, user, alertables)
      end

      def self.alert
        raise NotImplementedError
      end
    end
  end
end
