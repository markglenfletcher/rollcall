module Alerters
  module AssetRequests
    class RequestsDueAlerter < Alerter
      def alertables
        AssetRequest.due(@company)
      end

      def send_alert_email(user)
        Rollcall::App.deliver(:asset_request_alert, :requests_due, user, alertables)
      end
    end
  end
end
