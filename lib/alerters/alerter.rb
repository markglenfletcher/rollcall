module Alerters
  class Alerter
    def initialize(company)
      @company = company
    end

    def alert
      send_alerts if should_alert?
    end

    def alertees
      @company.alertees
    end

    def send_alerts
      alertees.each { |alertee| send_alert_email(alertee) }
    end

    def should_alert?
      alertables.present?
    end

    def alertables
      []
    end

    def self.alert
      Company.find_each { |company| new(company).alert }
    end
  end
end
